"""
Functions for data generation

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

"""

import numpy as np
from sklearn.utils import shuffle
from collections import Iterable


def f_peak_1d(x):
    r"""The "pyramid" function in 1 dimension.

    =============================== ===================
    x                               y
    =============================== ===================
    [-:math:`\infty`, -1]           0
    (-1, 1)                         1 - :math:`|x|`
    [1, :math:`\infty`]             0
    =============================== ===================

    Args:
        x (float): input

    Returns:
        float
    """
    if abs(x) < 1.0:
        return max(0.0, 1.0 - abs(x))
    else:
        return 0.0


def data_peak_1d(n, rng=2.0, sigma=0.0, dtype=np.float32):
    """Generate a dataset (x, y) given by ``f_pyramid_1d()``.

    Args:
        n (int): number of rows
        rng (float): x domain = (-rng, rng). rng >= 1.0.
        sigma (float): add random error
        dtype (type): data type

    Returns:
        tuple:
            - **x** (*numpy.array*): of shape (n_docs, 1)
            - **y** (*numpy.array*): of shape (n_docs,)
    """
    # check
    if rng < 1.0:
        raise ValueError(f"rng={rng} < 1.0 is not reasonable!")

    x = np.random.uniform(-rng, rng, n).astype(dtype)
    y = np.array(list(map(f_peak_1d, x)), dtype=dtype)
    if sigma > 0:  # add random term
        y += np.random.normal(0, sigma, n)
    return x.reshape((n, 1)), y.reshape((n, 1))


def f_peak_nd(x):
    r"""N-dimensional "pyramid" function.

    ============================================== ========================
    :math:`x_i, i \in \{1,2,...,N\}`               y
    ============================================== ========================
    :math:`x_i \in [\infty, -1], \exists i`        0
    :math:`x_i \in (-1, 1), \forall i`             :math:`\min(1-|x_i|)`
    :math:`x_i \in [1, \infty], \exists i`         0
    ============================================== ========================

    Args:
        x (Iterable[float]): input coordinates

    Returns:
        float
    """
    ans = 1.0
    for x_i in x:
        # if out of bound, return 0 immediately
        if abs(x_i) >= 1.0:
            return 0.0
        # the value the s
        ans = min(ans, 1.0 - abs(x_i))
    return ans


def data_peak_nd(n, ndim, rng=2.0, sigma=0.0, dtype=np.float32):
    """Generate a dataset (x, y) given by ``f_pyramid_nd()``.

    Args:
        n (int): number of rows
        ndim (int): number of input dimension
        rng (float): x domain = (-rng, rng). rng >= 1.0.
        sigma (float): add random error
        dtype (type): data type

    Returns:
        tuple:
            - **x** (*numpy.array*): shape=(n_docs, 1)
            - **y** (*numpy.array*): shape=(n_docs,)
    """
    # check
    if rng < 1.0:
        raise ValueError(f"rng={rng} < 1.0 is not reasonable!")

    x = np.random.uniform(-rng, rng, (n, ndim)).astype(dtype)
    y = np.array(list(map(f_peak_nd, x)), dtype=dtype)
    if sigma > 0:  # add random term
        y += np.random.normal(0, sigma, n)
    return x, y.reshape((n, 1))


def f_bar_1d_scalar(x, width=0.3333, rng=1.0):
    """The "bar" function in 1 dimension.

    ============ ===================
    x             y
    ============ ===================
    [-1, -w]      0
    [-w, w]       1
    [w, 1]        0
    ============ ===================

    Args:
        x (float): x
        width (float): the bar region is abs(x) <= width
        rng (float): x domain = (-rng, rng)

    Returns:
        float
    """
    if abs(x) <= width:
        return 1
    elif (abs(x) >= width) and (abs(x) <= width):
        return 1
    else:
        raise ValueError(f"{-width} <= {x} <= {width} is violated!")


def f_bar_1d():
    pass


def f_zigzag(x):
    """The zigzag function (linearized sine).

    Args:
        x (float): a real number

    Returns:
        float
    """
    a = (x + 0.5) % 2
    return 1.0 - 2.0 * abs(a - 1)


def data_zigzag_1d(n, rng=3.0, sigma=0.0, dtype=np.float32):
    """The zigzag function (linearized sine).

    Args:
        n (int): number of rows
        rng (float): x domain = (-rng, rng), rng >= 0.5
        sigma (float): add random error
        dtype (type): data type

    Returns:
        tuple:
            - **x** (*numpy.array*): of shape (n_docs, 1)
            - **y** (*numpy.array*): of shape (n_docs,)
    """

    # check
    if rng < 0.5:
        raise ValueError(f"rng={rng} < 0.5 is not reasonable!")

    x = np.random.uniform(-rng, rng, n).astype(dtype)
    y = np.array(list(map(f_zigzag, x)), dtype=dtype)
    if sigma > 0:  # add random term
        y += np.random.normal(0, sigma, n)

    return x.reshape((n, 1)), y.reshape((n, 1))


def f_step_1d_scalar():
    pass


def f_step_1d():
    pass


def f_natsign_depr(n, sigma=0, size=1.0):
    r"""(deprecated) Generate data for a "natural sign" (♮, U+266E) 2-D classification problem.

    Args:
        n (int): sample size(total #samples = 4n)
        sigma (float): add random error (unused)
        size (float): half-width of the box-size (unused)

    Returns:
        tuple:
            - **(x,y)** (*np.array*): (x, y) coordinate
            - **f(x,y)** (*np.array*): f(x, y) :math:`\in` {'L', 'R'}
    """

    ans_xy = np.zeros((4 * n, 2), dtype=np.float32)
    ans_f = np.zeros(4 * n, dtype=object)

    # top-left arm (1)
    t = np.random.uniform(0, 1, n)
    ans_xy[:n, 0] = -1.55 + 1.55 * t
    ans_xy[:n, 1] = -0.5 + 1.55 * t
    ans_f[:n] = 'L'

    # bottom-left bottom arm (1)
    t = np.random.uniform(0, 1, n)
    ans_xy[n:2*n, 0] = -1.55 + 1.95 * t
    ans_xy[n:2*n, 1] = -0.5
    ans_f[n:2*n] = 'L'

    # bottom-right bottom arm (0)
    t = np.random.uniform(0, 1, n)
    ans_xy[2*n:3*n, 0] = 1.55 * t
    ans_xy[2*n:3*n, 1] = -1.05 + 1.55 * t
    ans_f[2*n:3*n] = 'R'

    # top-right arm (0)
    t = np.random.uniform(0, 1, n)
    ans_xy[3*n:, 0] = -0.4 + 1.95 * t
    ans_xy[3*n:, 1] = 0.5
    ans_f[3*n:] = 'R'

    return ans_xy, ans_f


def data_natsign(n=2000, rnd=False, x0=1.0, y0=0.3, x1=1.3, r=1, gap=0.05, sigma=0, verbose=False):
    r"""Generate data for the NatSign 2-D classification problem.
    NatSign = "natural sign" (♮, U+266E).

    Args:
        n (int): sample size(total #samples = n)
        rnd (bool): random sampling or uniform sampling
        x0 (float): x-coordinate of the top-right elbow tip
        y0 (float): y-coordinate of the top-right elbow tip
        x1 (float): x-coordinate of the top-right tail
        r (float): slope of the tail
        gap (float): horizontal separation between classes
        sigma (float): add random noise (unused)
        verbose (bool): print diagnostic message

    Returns:
        tuple:
            - **(x,y)** (*numpy.array*)
            - **f(x,y)** (*numpy.array*): f(x, y) :math:`\in` {0 (left), 1 (right)}

    Note:
        - "Horizon" = part a
        - (Slash except Tail) = part b
        - "Tail" = part c
        - "Elbow" = part a + part b
        - "Slash" = part b + part c
    """

    # derived parameters
    y1 = r * (x1 + 1) - y0  # top-right tail
    x2 = 2 * y0 / r - 1  # slash-horizon intersection
    x3 = x2 + gap  # make a gap from x2

    # calculate segment length and n_points
    len_a = x0 - x3
    len_bc = ((x1 + x0) ** 2 + (y1 + y0) ** 2) ** 0.5
    n_a = int(n / 2 * len_a / (len_a + len_bc))
    n_bc = int(n / 2 - n_a)
    assert 2 * (n_a + n_bc) == n

    if verbose:

        # calculate tail-to-elbow ratio
        len_b = len_bc * (x2 + x0) / (x1 + x0)
        len_c = len_bc - len_b
        print(f"Tail-to-elbow ratio: {len_c / (len_a + len_b):.2f}")

        # count dots
        n_b = int(n_bc * len_b / len_bc)
        n_c = n_bc - n_b
        print(f"n_pts={2 * (n_a + n_bc)}, n_horizon={n_a}, n_slash={n_bc}, n_b=(ca.){n_b}, n_tail=(ca.){n_c}")

    # generate x
    if rnd:
        l1x = np.random.uniform(-x0, -x3, n_a).astype(np.float32)
        l2x = np.random.uniform(-x0, x1, n_bc).astype(np.float32)
    else:
        l1x = np.linspace(-x0, -x3, n_a, dtype=np.float32)
        l2x = np.linspace(-x0, x1, n_bc, dtype=np.float32)

    # derive y
    l1y = np.full(n_a, -y0, dtype=np.float32)
    c1 = np.ones(n_a, dtype=np.float32)  # class=1
    l2y = l2x * r + (r - y0)
    c2 = np.ones(n_bc, dtype=np.float32)

    # derive the mirror region
    l3x = -l1x
    l3y = -l1y
    c3 = np.zeros(n_a, dtype=np.float32)

    l4x = -l2x
    l4y = -l2y
    c4 = np.zeros(n_bc, dtype=np.float32)

    # combine data
    x = np.concatenate([l1x, l2x, l3x, l4x]).reshape((n, 1))
    y = np.concatenate([l1y, l2y, l3y, l4y]).reshape((n, 1))
    xy = np.hstack([x, y])
    f_xy = np.concatenate([c1, c2, c3, c4]).reshape((n, 1))
    assert xy.shape == (n, 2)

    xy, f_xy = shuffle(xy, f_xy)
    return xy, f_xy


def munge(x, n, s, p):
    """The MUNGE algo in `Buciluǎ et al. (2006). Model Compression. <https://doi.org/10.1145/1150402.1150464>`_

    Args:
        x (np.array): main dataset (2d)
        n (int): size multiplier
        s (float): local variance parameter
        p (float): probability parameter

    Returns:
        numpy.array: augmented x data
    """

    n_rows = x.shape[0]
    n_cols = x.shape[1]
    ans = np.zeros((n_rows * n, n_cols), dtype=np.float32)

    for i in range(n):
        x_tmp = x.copy()
        for row in range(n_rows):
            # find the closest example
            arr_dist = np.linalg.norm(x_tmp - x_tmp[row, :].reshape((1, n_cols)), axis=1)
            row_closest = arr_dist.argsort()[1]  # the first element is always 0
            # swap attributes with probability p
            for col in range(n_cols):
                if np.random.rand() < p:
                    a = x_tmp[row, col]
                    b = x_tmp[row_closest, col]
                    sd = abs(a - b) / s
                    x_tmp[row, col] = np.random.normal(b, sd)
                    x_tmp[row_closest, col] = np.random.normal(a, sd)
        ans[i * n_rows:(i + 1) * n_rows, :] = x_tmp
    else:
        return ans
