"""
Custom keras callback functions for this thesis.

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

Attention:
    Increment of neurons is NOT implemented in this file.
    ``NeuronMaskingCallback`` is abandoned.
"""

import os
import sys
from pathlib import Path
import numpy as np
from tensorflow.keras.layers import BatchNormalization, Dense
from tensorflow.keras.callbacks import Callback

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())
# append only if not exist
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

from lib.keras_model import get_dense_layer_names, get_layer_shape, get_layer_n_links


class LinkMaskingCallback(Callback):
    """Abstract base class for masking out links or neurons after a batch
    or an epoch. Originally designed to be used in conjunction with
    ``FixedMsTrainer`` because the ls_masks can change at the beginning of
    each stage.

    Model Requirements:
        - Dense layers must be named as ``dense_i, i=1,2,...n``.
        - Batch normalization layers must be named as ``bn_i``
        - Activation layers must be named as ``activation_i``

    Usage:
        Usually this should be put at the back of the keras ``callbacks=``
        list. DO NOT put in front of the ``BestModelCallback``!

    Note:
        Because the amount and ordering of other layers (e.g. activation,
        batch normalization, etc) are uncertain, the only way to access
        the layers I can think of is to access by names.
    """

    def __init__(self,
                 ls_masks=None,
                 skip_output_layer=True,
                 apply_on_batch=True,
                 batch_interval=1):

        super(LinkMaskingCallback, self).__init__()

        #: list[numpy.array]: list of mask objects for each dense layer.
        #: Can be reset by the partner multistage callback via ``reset_masks()``.
        self.ls_masks = ls_masks  # set initial value (optional)

        #: bool: do not apply mask on the output layer
        self.skip_output_layer = skip_output_layer

        #: bool: apply on batch end (true) or on epoch end (false)
        self.apply_on_batch = apply_on_batch

        #: int: apply on batch interval (default=1, suggested=4 or 8)
        self.apply_batch_interval = batch_interval

        # ==================================
        # Internal Variables
        # ==================================

        #: list[numpy.array]: the reverted mask
        self.ls_rev_masks = []

        #: list[str]: names of dense layers (of no use for now).
        #: Didn't list layer instances directly because the model may be rebuilt during training!
        self.ls_dense_layer_names = []

        #: int: ``= len(self.ls_dense_layer_names)``
        self.n_dense_layers = 0

        #: list[int]: number of links in each dense layer
        self.ls_n_links = []

        #: list[tuple[int]]: (input_shape, output_shape) for each dense layer
        self.ls_shapes = []

        #: list[int]: number of ones in the masks
        self.ls_mask_ones = []

        #: list[bool]: is layer i full (all-1)
        self.ls_mask_is_full = []

    def on_train_begin(self, logs=None):
        self.get_layer_info()
        self.derive_mask_properties()

    def on_epoch_end(self, epoch, logs=None):
        # ensure the mask is applied once at last
        if self.apply_batch_interval > 1:
            self.apply_masks()

    def on_batch_end(self, batch, logs=None):
        if self.apply_on_batch:
            # apply every interval
            if (batch + 1) % self.apply_batch_interval == 0:
                self.apply_masks()

    def get_layer_info(self):
        """Get layer information."""
        self.ls_dense_layer_names = get_dense_layer_names(self.model, dense_i=True)
        self.n_dense_layers = len(self.ls_dense_layer_names)
        self.ls_n_links = [get_layer_n_links(self.model, f"dense_{i + 1}") for i in range(self.n_dense_layers)]
        self.ls_shapes = [get_layer_shape(self.model, f"dense_{i + 1}") for i in range(self.n_dense_layers)]

    def derive_mask_properties(self):

        # count 1's
        self.ls_mask_ones = [np.count_nonzero(mask) for mask in self.ls_masks]

        # determine full or not
        for i in range(self.n_dense_layers):
            self.ls_mask_is_full.append(self.ls_mask_ones[i] == self.ls_n_links[i])

        # compute reverted masks
        self.ls_rev_masks = [~mask for mask in self.ls_masks]

    def reset_masks(self, ls_masks):
        """Reset masks on a new training stage."""
        self.ls_masks = ls_masks
        self.derive_mask_properties()

    def apply_masks(self):
        """Apply masks."""

        for i in range(self.n_dense_layers):

            # skip an all-1 mask
            if self.ls_mask_is_full[i]:
                return

            # skip the output layer
            if (i == self.n_dense_layers - 1) and self.skip_output_layer:
                return

            # apply
            layer = self.model.get_layer(f"dense_{i + 1}")
            wt = layer.get_weights()
            rev_mask = self.ls_rev_masks[i]
            wt[0][rev_mask] = 0  # apply on weights only, not bias
            layer.set_weights(wt)


class NeuronMaskingCallback(Callback):
    """(Abandoned). An keras callback to ls_masks out the neurons located at
    the tail of each layer after a batch or epoch. "Mask out" means
    resetting weights and biases to 0.

    This class itself does not process the multi-stage training process.
    Therefore, it must be used in conjunction with ``NiByMaskingTrainer``
    in order to reset the ls_masks upon entering the next stage.

    Model Requirements:
        - Dense layers must be named as ``dense_i, i=1,2,...n``.
        - Batch normalization layers must be named as ``bn_i``
        - Activation layers must be named as ``activation_i``
        - If ``RetainBestModelInMemory`` is also used, it must be the first
          element of the ``callbacks=`` list.

    Note:
        Because the count and ordering of other layers (e.g. activation,
        batch normalization, etc) are uncertain, the only way I can think
        of for accessing the hidden layers is by name.
    """

    def __init__(self, active_neurons, apply_on_epoch=True):

        super().__init__()

        #: bool: apply on epoch begin (true) or batch begin (false)
        self.apply_on_epoch = apply_on_epoch

        #: list[int]|numpy.array[int]: number of neurons to NOT be masked out
        self.active_neurons = active_neurons

        #: int: number of hidden layers
        self.n_hidden_layers = len(active_neurons)

        #: bool: use BatchNormalization or not
        self.use_bn = False  # will reset on multistage begin

    def chk_bn(self):
        """Check if the model contains BatchNormalization layers.

        Returns:
            bool
        """
        for layer in self.model.layers:
            if isinstance(layer, BatchNormalization):
                return True
        else:
            return False

    def on_train_begin(self, logs=None):
        """Initialize and check"""
        # detect BatchNormalization layer
        self.use_bn = self.chk_bn()

    def on_epoch_begin(self, epoch, logs=None):
        """
        Note:
            The callback works on epoch/batch begin. It cannot work on epoch end
            because the history was not computed yet!
        """
        if self.apply_on_epoch:
            self.apply_masks()

    def on_batch_begin(self, batch, logs=None):
        """(See ``on_epoch_begin``)"""
        if not self.apply_on_epoch:
            self.apply_masks()

    def on_train_end(self, logs=None):
        # TODO: check if it works after restoring the best model!!
        self.apply_masks()

    def apply_masks(self):
        """Master masking function."""

        for layer_number in range(1, self.n_hidden_layers + 2):

            # 1. Dense layers
            self.apply_mask_on_a_layer("dense", layer_number)

            # 2. Activation layers (there is no activation layer that require masking for now....)
            # NOTE: PReLU won't be touched if the incoming and outgoing weights were masked
            # self.apply_mask_on_a_layer("activation", layer_number)

            # 3. batch normalization layers (doesn't matter)
            # if self.use_bn:
            #     self.apply_mask_on_a_layer("bn", layer_number)

        # The output dense layer
        #self.apply_mask_on_a_layer("dense", self.n_hidden_layers + 1)
        # Also the output activation layer, perhaps someday.
        # self.apply_mask_on_a_layer("activation", self.n_hidden_layers + 1)

    def apply_mask_on_a_layer(self, base_layer_name, layer_number):
        """Apply masks at the end of each batch.

        Args:
            base_layer_name (str): e.g. "bn"=batch normalization, "activation", "prelu"....
            layer_number (int): 1-based numbering
        """

        name = f"{base_layer_name}_{layer_number}"
        layer = self.model.get_layer(name)

        if isinstance(layer, Dense):

            weights = layer.get_weights()

            # 1. effect from masking this layer
            if layer_number <= self.n_hidden_layers:  # exclude the output layer
                n = self.active_neurons[layer_number - 1]
                weights[0][:, n:] = 0.0  # weights
                weights[1][n:] = 0.0  # bias

            # 2. effects from masking the previous layer
            if layer_number > 1:  # exclude the first layer
                n = self.active_neurons[layer_number - 2]
                weights[0][n:, :] = 0.0  # weights (don't touch bias)

            layer.set_weights(weights)

        elif isinstance(layer, BatchNormalization):
            # ones, zeros, zeros, ones
            weights = layer.get_weights()
            n = self.active_neurons[layer_number - 1]
            weights[0][n:] = 1.0
            weights[1][n:] = 0.0
            weights[2][n:] = 0.0
            weights[3][n:] = 1.0
            layer.set_weights(weights)

        # elif isinstance(layer, (Activation, Dropout, LeakyReLU, PReLU)):
        #     # No trainable parameters: Activation, Dropout, LeakyReLU
        #     # Parameters kept zero: PReLU (because both the input and output weights are zeroed)
        #     pass

        else:
            raise ValueError(f"Layer type not understood: {type(layer)}")

    def reset_masks(self, active_neurons):
        """Assume ``self.apply_on_epoch`` on."""

        # super().__init__()  # does not seem to be required

        self.active_neurons = active_neurons

        assert self.n_hidden_layers == len(self.active_neurons), "This callback is not designed to be reused on a different model!"
