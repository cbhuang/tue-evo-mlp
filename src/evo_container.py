"""
The main container class of the workflow of the proposed
evolutionary algorithms as well as neuron increment (NI).

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

TODO:
    - A better solution to deal with unseen labels...
    - Stratified sampling
    - Allow multi-dimensional input shape
    - Allow multiple metrics
    - Use KerasTuner to allow conditional hparams and automatic tuning!
"""

import os
from pathlib import Path
import sys
import json
import numpy as np
import time
import itertools
import random
import shutil
from datetime import datetime
import pandas as pd
import h5py
from copy import deepcopy

# mongodb
import pymongo
from bson.objectid import ObjectId

# tensorflow and keras
import tensorflow as tf
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping
from tensorflow.keras.datasets import mnist
from tensorflow.keras import initializers

# preprocessing / metrics / algebra
from sklearn.preprocessing import LabelEncoder, StandardScaler, MinMaxScaler, OneHotEncoder
from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix
from skimage.measure import block_reduce

# Visualization
import matplotlib

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())
# add base_dir to path if not added
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

# local libs
from lib.eng_util import print_loglike as printl, save_str_dict_as_json as sdajson, dump_sklearn_scaler_params, simple_dict_to_suffix, subset_dicts_by_dict, tuple_to_str_compact
from lib.math_util import r2_metric, count_through_list, median_and_empirical_ci
from lib.vis_template import plot_multi_y, plot_true_vs_pred, plot_confusion_matrix
from lib.base_data_container import BaseDataContainer, BaseAggrContainer
from lib.keras_model import sequential_mlp, build_optimizer
from lib.keras_callback import BestModelCallback
from lib.keras_multistage import FixedMsTrainer, RebuildSequentialMlpMsCallback, BestWeightsTransferMsCallback, ResetOptimizerMsCallback
from src.data_synthesis import data_peak_1d, data_zigzag_1d, f_peak_1d, f_zigzag, data_natsign
from src.evo_callback import LinkMaskingCallback
from src.evo_multistage import RleMsCallback, LeMsCallback, NeMsCallback, RneMsCallback


class EvoContainer(BaseDataContainer):
    """The main container class for evolutionary algorithms.

    Usage:

        .. code-block:: python

            # initialize
            from src.evo_container import EvoContainer
            o = EvoContainer()

            # Workflow
            o.s01_xxx_xx()
            o.sNN_xxx_xx()
            # ...
    """

    # ============================================
    # Inherited I/O Artifacts
    # ============================================

    _path_attrs = (
        "base_dir", "config_file", "params_file", "hptune_file",
        "config_dir", "data_dir", "raw_dir", "out_dir", "fig_dir",
        "logs_dir", "export_dir",
        "best_model_path", "best_model_plot_path",
        "history_plot_path", "history_path"
    )

    _no_pickle_attrs = (
        "model", "history", "mongo_driver", "db_obj", "coll_obj",
        "callbacks",
    )

    _no_pickle_types = (
        tf.keras.Model, tf.keras.callbacks.History, tf.keras.callbacks.Callback,
        tf.keras.optimizers.Optimizer,
        pymongo.MongoClient, pymongo.collection.Collection, pymongo.database.Database
    )

    # =======================
    # Constants
    # =======================

    #: list[str]: names of synthesized datasets that may be studied
    ls_synthesized_datasets = ["peak-1d", "zigzag-1d", "natsign"]
    #     , "peak-nd", "bar-1d", "bar-nd", "step-1d", "step-nd",
    #     "sin-1d", "sin-nd", "exp-1d", "exp-nd", "pow-1d", "pow-nd"

    #: list[str]: names of real datasets studied
    ls_real_datasets = ["mnist", "mnist-10", "mnist-7x7", "mnist-7x7-10"]

    #: list[str]: names of evolution methods studied
    ls_evo_methods = ["LE", "RLE", "NE", "RNE"]

    def __init__(self, params_file, config_file, hptune_file=None, **kwargs):
        """Initialize default attributes.

        Args:
            params_file (PathLike): parameter file
            config_file (PathLike): configuration file
            hptune_file (PathLike): hyperparam tuning file
            kwargs (dict): of no use for now

        Note: config_file and base_dir should be fixed
        """

        super(EvoContainer, self).__init__(**kwargs)

        # =======================
        # Fixed Paths
        # =======================

        #: Path: project base directory
        self.base_dir = base_dir

        #: Path: configuration file
        self.config_file = Path(config_file)
        assert self.config_file.exists()

        #: Path: parameter file
        self.param_file = Path(params_file)
        assert self.param_file.exists()

        #: Path: hyperparameter tuning list file
        self.hptune_file = Path(hptune_file) if (hptune_file is not None) else None
        if hptune_file is not None:
            assert self.hptune_file.exists()

        #: Path: configuration directory
        self.config_dir = self.base_dir / "config"
        self.config_dir.mkdir(parents=True, exist_ok=True)

        #: Path: data directory
        self.data_dir = self.base_dir / "data"
        self.data_dir.mkdir(parents=True, exist_ok=True)

        #: Path: raw data directory
        self.raw_dir = self.data_dir / "raw"
        self.raw_dir.mkdir(parents=True, exist_ok=True)

        #: Path: output and intermediate data directory
        self.out_dir = self.data_dir / "out"
        self.out_dir.mkdir(parents=True, exist_ok=True)

        #: Path: figure directory
        self.fig_dir = self.data_dir / "fig"
        self.fig_dir.mkdir(parents=True, exist_ok=True)

        #: Path: logs directory for Tensorboard
        self.logs_dir = self.data_dir / "logs"
        self.logs_dir.mkdir(parents=True, exist_ok=True)

        #: Path: export and dump directory
        self.export_dir = self.data_dir / "export"
        self.export_dir.mkdir(parents=True, exist_ok=True)

        # load config and parameters (right after the paths were set)
        c = self._load_json_into_dict(self.config_file)
        p = self._load_json_into_dict(self.param_file)

        # =======================
        # MongoDB
        # =======================

        #: bool: use mongodb or not
        self.use_mongodb = c["mongodb"]["use_mongodb"]

        #: str: hostname or ip of mongodb
        self.mongo_host = c["mongodb"]["mongo_host"]

        #: int: connection port of mongodb
        self.mongo_port = c["mongodb"]["mongo_port"]

        #: str: credential file (load username and pwd only upon driver initialization)
        self.mongo_credential_file = self.config_dir / c["credential_file"]

        #: str: default DB name
        self.mongo_db = c["mongodb"]["mongo_db"]

        #: str: default collection name
        self.mongo_collection = c["mongodb"]["mongo_collection"]

        #: str: debug collection name
        self.debug_collection = c["mongodb"]["debug_collection"]

        #: MongoClient: driver instance
        self.mongo_driver = None

        #: Database: mongodb working database
        self.db_obj = None

        #: Collection: mongodb working collection
        self.coll_obj = None

        # ============================
        # Reproducibility
        # ============================
        # NOTE: only works on the same machine with the same setup!

        #: int: randomization seed for reproducibility
        self.PYTHONHASHSEED = p["PYTHONHASHSEED"]
        #: int: for tensorflow-determinism
        self.TF_DETERMINISTIC_OPS = p["TF_DETERMINISTIC_OPS"]
        #: int: seed of ``numpy.random`` (-1=use ``time.time_ns()``)
        self.seed_np_random = self.get_seed(p["seed_np_random"])
        #: int: seed of ``random`` (-1=use ``time.time_ns()``)
        self.seed_python_random = self.get_seed(p["seed_python_random"])
        #: int: seed of ``tensorflow.random`` (-1=use ``time.time_ns()``)
        self.seed_tf_random = self.get_seed(p["seed_tf_random"])

        self._init_reproducibility()

        # ============================
        # Data Source
        # ============================

        #: str: source name (e.g. "peak-1d", "mnist-7x7-10", "natsign")
        self.data_source = p["data_source"].lower()

        #: bool: dataset is synthesized
        self.is_synthesized = self._derive_is_synthesized(self.data_source)

        #: str: F=fitting, C=classification
        self.problem_type = p["problem_type"].lower()

        #: int: input dimension
        self.input_dim = p["input_dim"]

        #: int: output dimension
        self.output_dim = p["output_dim"]

        #: int: total size of data (train+test)
        self.data_size = p["data_size"]

        #: dict: params to generate data
        self.data_params = p["data_params"]

        # ============================
        # Network Parameters
        # ============================

        #: list[int]: maximum number neurons in each layer
        self.ls_max_neurons = p["ls_max_neurons"]

        #: int: number of hidden layers = ``len(ls_max_neurons)``.
        self.n_hidden_layers = self._derive_n_hidden_layers()

        #: bool: apply batch normalization between dense layer and activation
        self.batch_normalization = p["batch_normalization"]

        #: float: drouput rate (0 = no dropout layer)
        self.dropout_rate = p["dropout_rate"]

        #: str: activation function of hidden layers
        self.hidden_activation = p["hidden_activation"]

        #: str: activation of the output layer
        self.output_activation = p["output_activation"]

        # =========================================
        # Evolution Parameters
        # =========================================

        #: str: NI=neuron increment, LE=link evolution,
        #:      NE=neuron evolution, NONE=no evolution
        self.evo_method = p["evo_method"]

        #: str: metric for evolution algorithm (like p-min, p-max)
        #:      as inspired by transfer learning papers
        self.neuron_evo_metric = p["neuron_evo_metric"]

        #: dict: the p of p-sum for neuron evolution
        self.neuron_evo_p = p["neuron_evo_p"]

        #: float: zeta in standard ENN
        self.zeta = p["zeta"]

        #: float: epsilon in standard ENN
        self.epsilon = p["epsilon"]

        #: array[int]: available neurons within each layer (column, 2nd dim)
        #:             for each stage (row, 1st dim)
        self.neuron_increment_stages = p["neuron_increment_stages"]

        #: int: times of neuron increments (=``len(neuron_increment)``)
        self.n_neuron_increments = self._derive_n_neuron_increments()

        #: int: rounds of training for each stage of neuron Increment
        self.n_evo_rounds = p["n_evo_rounds"]

        #: float: minimum change of magnitude (times ``self.std_evo_scale``)
        #: for a parameter in an evolution
        self.min_evo_scale = p["min_evo_scale"]

        #: float: stdev of evolution size (before ``min_evo_scale``)
        self.std_evo_scale = p["std_evo_scale"]

        #: bool: evolve output layer or not (default=NO)
        self.evo_output_layer = p["evo_output_layer"] if "evo_output_layer" in p else False  # backward compatibility

        #: int: When epsilon < 1, apply mask upon every N batches
        self.evo_mask_interval = p["evo_mask_interval"] if "evo_mask_interval" in p else 1  # backward compatibility

        # ============================
        # Training Parameters
        # ============================

        #: int: repeat n_docs runs for each hparam set
        self.n_repeat = p["n_repeat"]

        #: bool: use the same data for train and test. ``self.test_split`` is ignored.
        self.same_train_test = p["same_train_test"]
        #: float: test split
        self.test_split = p["test_split"]
        #: float: validation split in training
        self.validation_split = p["validation_split"]

        #: str: ``kernel_initializer`` in ``Dense()``
        self.weight_initializer = p["weight_initializer"].lower()
        #: float: scale factor for ``kernel_initializer``. Limited support (uniform and normal).
        self.weight_init_scale = p["weight_init_scale"]
        #: str: ``bias_initializer`` in ``Dense()``
        self.bias_initializer = p["bias_initializer"].lower()
        #: bool: scale factor for ``kernel_initializer``. Limited support (uniform and normal).
        self.bias_init_scale = p["bias_init_scale"]

        #: string: loss function in training. 0=EarlyStopping
        self.loss_function = p["loss_function"]

        #: int: Training epochs. ``epochs <= 0`` implies ``EarlyStopping``!
        self.signed_epochs = p["signed_epochs"]
        #: int: a large number of epochs when epochs=0 + EarlyStopping
        self.limit_epochs = p["limit_epochs"]
        #: int: epochs adjusted for ``epochs == 0``
        self.epochs = self._derive_epochs(self.signed_epochs, self.limit_epochs)
        #: bool: Apply EarlyStopping callback. Derived from the minus sign of ``epochs``!
        self.early_stopping = self._derive_early_stopping(self.signed_epochs)

        #: float: minimum improvement of loss for EarlyStopping and ReduceLRonPlateau
        self.min_loss_improvement = p["min_loss_improvement"]

        #: int: patience for callbacks
        self.patience = p["patience"]

        #: int: batch size of training
        self.batch_size = p["batch_size"]

        #: string: name of the model optimizer
        self.optimizer = p["optimizer"].lower()

        #: bool: ReduceLROnPlateau in training
        self.reduce_lr_on_plateau = p["reduce_lr_on_plateau"]
        #: float: initial learning rate
        self.init_lr = p["init_lr"]
        #: float: how many times does init_lr decrease
        self.min_lr_fraction = p["min_lr_fraction"]
        #: float: minimum learning rate for ReduceLRonPlateau (fixed as 1% of init_lr)
        self.min_lr = self._derive_min_lr()
        #: float: by this factor when LR is reduced (default 10)
        self.reduce_lr_factor = p["reduce_lr_factor"]

        # ============================
        # Resource
        # ============================

        #: int: begin time of the run in nanoseconds
        self.timestamp_exp_begin_ns = 0
        #: int: end of the run in nanoseconds
        self.timestamp_exp_end_ns = 0
        #: float: seconds spent on training (informal)
        self.exec_time = 0.0

        # ============================
        # Others for potential use
        # ============================

        #: dict: other parameter settings for future use
        self.other_params = p["other_params"]

        #: dict: other results for future use
        self.other_results = {}

        # ============================
        # Hyperparameter Tuning
        # ============================

        #: bool: perform hyperparameter tuning
        self.tune_hp = p["tune_hp"]
        #: dict: hyperparameter values to try if ``tune_hp`` is true
        self.hparams = {}
        #: int: run number in hparam tuning (0=not executed)
        self._hparam_run_nr = 0

        # ============================
        # Intermediate Data
        # ============================

        # ===== raw data =====
        #: np.array: raw x dataset
        self.x_raw = None
        #: np.array: raw y dataset
        self.y_raw = None
        #: np.array: raw x_train dataset
        self.x_train_raw = None
        #: np.array: raw y_train dataset
        self.y_train_raw = None
        #: np.array: raw x_test dataset
        self.x_test_raw = None
        #: np.array: raw y_test dataset
        self.y_test_raw = None

        # ===== scalers =====
        #: str: **n** = normalize (MinMaxScaler), **s** = standardize (StandardScaler),
        #:      **l** = LabelEncoder, **o** = OneHotEncoder, ""=None
        self.x_transformer = p["x_transformer"].lower()
        #: str: same as ``self.x_transformer``
        self.y_transformer = p["y_transformer"].lower()
        #: LabelEncoder|StandardScaler|MinMaxScaler|OneHotEncoder: transformer object of x, kept for inverse transform
        self.x_transformer_obj = self.construct_transformer(self.x_transformer)
        #: LabelEncoder|StandardScaler|MinMaxScaler|OneHotEncoder: transformer object of y, kept for inverse transform
        self.y_transformer_obj = self.construct_transformer(self.y_transformer)
        #: LabelEncoder|StandardScaler|MinMaxScaler|OneHotEncoder: transformer object of x, kept for inverse transform
        self.x_transformer_params = None
        #: LabelEncoder|StandardScaler|MinMaxScaler|OneHotEncoder: transformer object of y, kept for inverse transform
        self.y_transformer_params = None

        # ===== preprocessed data =====
        #: np.array: train X
        self.x_train = None
        #: np.array: test X
        self.x_test = None
        #: np.array: train y
        self.y_train = None
        #: np.array: test y
        self.y_test = None

        #: int: train data size
        self.n_train = self._derive_n_train()
        #: int: test data size
        self.n_test = self._derive_n_test()

        # ===== objects used in training =====

        #: tensorflow.keras.initializers.Initializer: ``kernel_initializer`` instance for ``Dense()``
        self.weight_initializer_obj = None
        #: tensorflow.keras.initializers.Initializer: ``bias_initializer``
        self.bias_initializer_obj = None

        #: tensorflow.keras.optimizers.Optimizer: the optimizer instance
        self.optimizer_obj = None

        #: list: list of metrics in model.compile().
        self.ls_metrics = None

        #: list: keras callback functions to be applied
        self.callbacks = None

        #: RetainBestModelInMemory: (obligatory) retain best model info
        self.best_callback_obj = None

        #: LinkMaskingCallback: link masking object for evolution algos with ``self.epsilon < 1``
        self.lm_callback_obj = None

        #: BaseMultiStageTrainer: train model on multiple stages
        self.multistage_trainer = None

        #: dict: (debug) can inspect current hparams if the program stops.
        self._hparams_now = None

        #: Model: main keras model instance
        self.model = None

        #: dict: self.model.history.history
        self.history = None

        # ============================
        # Result
        # ============================

        # ======== accuracy ===========
        #: str: accuracy metric to be reported
        self.acc_metric = p["acc_metric"].lower()
        #: float: train accuracy of the best model
        self.train_acc = 0.0
        #: float: validation accuracy of the best model
        self.val_acc = 0.0
        #: float: test accuracy of the best model
        self.test_acc = 0.0

        # ===== training / evolution =====
        #: int: best evolution stage (1-based)
        self.best_evo_stage = 0
        #: int: best epoch in ``best_evo_stage``
        self.best_epoch = 0
        #: int: best epoch since begin (incl. every stage)
        self.best_total_epoch = 0
        #: list[int]: list of actual epochs in each stage
        self.ls_epochs_each_stage = []
        #: int: #epochs trained
        self.total_epochs = 0

        # ===== output =====
        #: Path: path to the stored model coefficients given by ``model.get_layers()``
        self.best_model_path = None
        #: Path: path to the plot of the best model (true vs pred)
        self.best_model_plot_path = None
        #: Path: history plot
        self.history_plot_path = None
        #: Path: history file
        self.history_path = None

        # ============================
        # Visualization settings
        # ============================

        #: str: which backend to use
        self.plt_backend = c["visualization"]["plt_backend"]
        matplotlib.use(self.plt_backend)

        #: bool: show figure on screen
        self.show_fig = c["visualization"]["show_fig"]
        #: bool: save figures
        self.save_fig = c["visualization"]["save_fig"]
        #: int: figsize
        self.figsize = tuple(c["visualization"]["figsize"])
        #: int: figure dpi
        self.dpi = c["visualization"]["dpi"]

        # ============================
        # Suffixes
        # ============================

        #: str: main file suffix - generated dynamically for each run
        self.suffix = ""

        #: str: subfolder name of this experiment, marking debug or not
        self.suffix_exp = f"{self.suffix_ts}_debug" if c["debug"]["dbg"] else self.suffix_ts

        # ============================
        # Quality Control, Debug
        # ============================

        #: bool: run `chk_` functions at runtime to check data consistency
        self.chk = c["debug"]["chk"]

        #: bool: run debug code
        self.dbg = c["debug"]["dbg"]

        #: bool: delete (x|y)_raw after transform to save memory
        self.del_unsplit_raw = c["debug"]["del_unsplit_raw"]

        #: bool: delete (x|y)_(train|test)_raw and y_raw after transform to save memory
        self.del_split_raw = c["debug"]["del_split_raw"]

        # ============================
        # Misc. Initialization
        # ============================

        # load hparam info
        if self.hptune_file is not None:
            self.load_hparams(self.hptune_file)

        # init mongo driver after the debug flag was saved
        if self.use_mongodb:
            self._init_mongo_driver()

    # ========================================
    # Initialization
    # ========================================

    @staticmethod
    def _load_json_into_dict(json_file):
        """Load JSON format parameters.

        Args:
            json_file (PathLike): file path
        Returns:
            dict
        """
        with open(json_file) as f:
            dic = json.load(f)
        return dic

    @staticmethod
    def get_seed(seed):
        """Set a integer seed value. -1 = ``time.time_ns()``

        Args:
            seed (int): seed value. 0 = timestamp

        Returns:
            int: a nonnegative 32-bit integer
        """
        assert isinstance(seed, int), f"Seed is not a integer: {seed}"
        if seed == -1:
            return time.time_ns() % (2**32)  # 0 to 2^32-1
        elif seed >= 0:
            return seed
        else:
            raise ValueError(f"Seed = {seed} is not a non-negative integer!")

    def _init_reproducibility(self):
        """Initialize reproducibility parameters"""
        # unused yet
        os.environ["PYTHONHASHSEED"] = str(self.PYTHONHASHSEED)
        os.environ["TF_DETERMINISTIC_OPS"] = str(self.TF_DETERMINISTIC_OPS)
        # used
        np.random.seed(self.seed_np_random)
        random.seed(self.seed_python_random)
        tf.random.set_seed(self.seed_tf_random)

    def _reinit_reproducibility(self):
        """Re-seed"""
        # recalculate seeds
        self.seed_np_random = self.get_seed(-1)
        self.seed_python_random = self.get_seed(-1)
        self.seed_tf_random = self.get_seed(-1)
        # feed
        self._init_reproducibility()

    @staticmethod
    def construct_transformer(name):
        """Construct variable transformer.

        Args:
            name (str): n_docs, s, l, o

        Returns:
            LabelEncoder|StandardScaler|MinMaxScaler|OneHotEncoder: Scaler for numerical vars or Encoder for categorical vars
        """

        if name == "n":
            return MinMaxScaler()
        elif name == "s":
            return StandardScaler()
        elif name == "l":
            return LabelEncoder()
        elif name == "o":
            return OneHotEncoder()
        elif name == "":
            printl("Transformer = None! Make sure this is intended.", "WARNING")
            return None
        else:
            raise ValueError(f"Transformer name not understood: {name}")

    def _init_mongo_driver(self):
        """Initialize mongodb driver."""

        with open(self.mongo_credential_file) as f:
            dic = json.load(f)

        self.mongo_driver = pymongo.MongoClient(
            self.mongo_host, port=self.mongo_port,
            username=dic["mongo_user"],
            password=dic["mongo_pwd"],
            authSource=self.mongo_db
        )

        self.db_obj = self.mongo_driver[self.mongo_db]

        if self.dbg:
            self.coll_obj = self.db_obj[self.debug_collection]
        else:
            self.coll_obj = self.db_obj[self.mongo_collection]

    def reinit_derived_params(self):
        """Update all derived parameters. Run after parameter update."""
        self.is_synthesized = self._derive_is_synthesized(self.data_source)
        self.n_neuron_increments = self._derive_n_neuron_increments()
        self.n_hidden_layers = self._derive_n_hidden_layers()
        self.epochs = self._derive_epochs(self.signed_epochs, self.limit_epochs)
        self.early_stopping = self._derive_early_stopping(self.signed_epochs)
        self.min_lr = self._derive_min_lr()
        self.n_test = self._derive_n_test()
        self.n_train = self._derive_n_train()

    def _derive_is_synthesized(self, data_source):
        """Is a synthesized dataset?

        Args:
            data_source (str): custom data name

        Returns:
            bool
        """
        if data_source in self.ls_synthesized_datasets:
            return True
        elif data_source in self.ls_real_datasets:
            return False
        else:
            raise NotImplementedError(f"data_source not understood: {data_source}")

    @staticmethod
    def _derive_epochs(epochs, large_epoch):
        """If epoch=0 then substitute with a large number.

        Args:
            epochs (int): epochs
            large_epoch (int): substitute with what

        Returns:
            int
        """
        if epochs == 0:
            return large_epoch
        elif abs(epochs) > 0:
            return abs(epochs)
        else:
            raise ValueError(f"Bad epochs value: {epochs}")

    @staticmethod
    def _derive_early_stopping(epochs):
        """If epochs <= 0 then force true.

        Args:
            epochs (int): epochs

        Returns:
            bool
        """
        if epochs > 0:
            return False
        elif epochs <= 0:
            return True
        else:
            raise ValueError(f"Bad epochs value: {epochs}")

    def _derive_min_lr(self):
        """Fixed at 1% of ``self.init_lr`` to show its reasonable value."""
        return self.init_lr * self.min_lr_fraction

    def _derive_n_neuron_increments(self):
        return len(self.neuron_increment_stages)

    def _derive_n_hidden_layers(self):
        return len(self.ls_max_neurons)

    def _derive_n_test(self):
        return int(self.data_size * self.test_split)

    def _derive_n_train(self):
        return self.data_size - int(self.data_size * self.test_split)

    # ======================================================================
    # 0. Data Retrieval & Preprocessing
    # ======================================================================

    def s01_load_raw_data(self):
        """Load raw data and split into (x|y)_(train|test)_raw, 4 total.
        The exact preprocessing steps depends on data, so will be
        implemented separately.

        Produces:
            - **self.x_train_raw** (np.array[n_records, n_x_features])
            - **self.y_train_raw** (np.array[n_records, n_y_features])
            - **self.x_test_raw** (np.array[n_records, n_x_features])
            - **self.y_test_raw** (np.array[n_records, n_y_features])
        """

        if self.data_source == "peak-1d":
            self.generate_peak_1d()
        elif self.data_source == "zigzag-1d":
            self.generate_zigzag_1d()
        elif self.data_source == "mnist":
            self.load_mnist()
        elif self.data_source == "mnist-10":
            self.load_mnist(subset_fraction=0.1)
        elif self.data_source == "mnist-7x7":
            self.load_mnist(down_sample=4)
        elif self.data_source == "mnist-7x7-10":
            self.load_mnist(down_sample=4, subset_fraction=0.1)
        elif self.data_source == "natsign":
            self.generate_natsign()
        else:
            raise NotImplementedError(f"Dataset {self.data_source} not understood!")

    def generate_peak_1d(self):
        """Generate peak-1d dataset."""

        # obtain
        self.x_raw, self.y_raw = data_peak_1d(self.data_size, sigma=self.data_params["sigma"])

        # shuffle
        self.x_raw, self.y_raw = shuffle(self.x_raw, self.y_raw)

        # split train, test
        self.x_train_raw = self.x_raw[:self.n_train, :]
        self.y_train_raw = self.y_raw[:self.n_train, :]
        self.x_test_raw = self.x_raw[self.n_train:, :]
        self.y_test_raw = self.y_raw[self.n_train:, :]

    def generate_zigzag_1d(self):
        """Generate zigzag-1d dataset."""

        # obtain
        self.x_raw, self.y_raw = data_zigzag_1d(self.data_size, sigma=self.data_params["sigma"])

        # shuffle
        self.x_raw, self.y_raw = shuffle(self.x_raw, self.y_raw)

        # split train, test
        self.x_train_raw = self.x_raw[:self.n_train, :]
        self.y_train_raw = self.y_raw[:self.n_train, :]
        self.x_test_raw = self.x_raw[self.n_train:, :]
        self.y_test_raw = self.y_raw[self.n_train:, :]

    def load_mnist(self, down_sample=1, subset_fraction=1.0):
        """Load mnist, performing optional down-sampling and subsetting.

        Args:
            down_sample (int): An n-to-1 average for each image dimension
            subset_fraction (float): what fraction of data will be took
        """

        (x_train_raw, y_train_raw), (x_test_raw, y_test_raw) = mnist.load_data()

        w = int(np.ceil(28 / down_sample))  # new image width
        n_train = int(x_train_raw.shape[0] * subset_fraction)  # 60000 * frac
        n_test = int(x_test_raw.shape[0] * subset_fraction)  # 10000 * frac

        # shuffle
        x_train_raw, y_train_raw = shuffle(x_train_raw, y_train_raw)
        x_test_raw, y_test_raw = shuffle(x_test_raw, y_test_raw)

        # subset
        if subset_fraction < 1.0:
            x_train_raw = x_train_raw[:n_train, :, :]
            y_train_raw = y_train_raw[:n_train]
            x_test_raw = x_test_raw[:n_test, :, :]
            y_test_raw = y_test_raw[:n_test]

        # down-sampling
        if down_sample > 1:
            x_train_raw = block_reduce(x_train_raw, (1, down_sample, down_sample), func=np.mean)
            x_test_raw = block_reduce(x_test_raw, (1, down_sample, down_sample), func=np.mean)

        # flatten
        self.x_train_raw = x_train_raw.reshape((-1, w * w)).astype(np.float32)
        self.y_train_raw = y_train_raw.reshape((-1, 1))
        self.x_test_raw = x_test_raw.reshape((-1, w * w)).astype(np.float32)
        self.y_test_raw = y_test_raw.reshape((-1, 1))

    def generate_natsign(self):
        """Generate NatSign dataset"""

        # generate (shuffled)
        self.x_raw, self.y_raw = data_natsign(self.data_size, rnd=True, sigma=self.data_params["sigma"], verbose=True)

        # split train, test
        self.x_train_raw = self.x_raw[:self.n_train, :]
        self.y_train_raw = self.y_raw[:self.n_train, :]
        self.x_test_raw = self.x_raw[self.n_train:, :]
        self.y_test_raw = self.y_raw[self.n_train:, :]

    def s03_impute_missing(self):
        """Fill missing values."""
        pass

    def s05_transform(self):
        """Transform raw data."""

        # (re)initialize the transformers
        self.x_transformer_obj = self.construct_transformer(self.x_transformer)
        self.y_transformer_obj = self.construct_transformer(self.y_transformer)

        # transform data
        if self.data_source in ("mnist", "mnist-10", "mnist-7x7", "mnist-7x7-10"):

            self.x_train = self.transform_x(self.x_train_raw).astype(np.float32)
            self.x_test = self.transform_x(self.x_test_raw).astype(np.float32)

            self.y_transformer_obj, self.y_train, self.y_test = self._transform_raw_1d2d(
                self.y_transformer, self.y_transformer_obj, self.y_train_raw, self.y_test_raw
            )

        elif self.data_source in ("peak-1d", "zigzag-1d", "natsign"):  # 1D and 2D synthetic functions
            self.x_transformer_obj, self.x_train, self.x_test = self._transform_raw_1d2d(
                self.x_transformer, self.x_transformer_obj, self.x_train_raw, self.x_test_raw
            )
            self.y_transformer_obj, self.y_train, self.y_test = self._transform_raw_1d2d(
                self.y_transformer, self.y_transformer_obj, self.y_train_raw, self.y_test_raw
            )

        else:
            raise NotImplementedError(f"data source not understood: {self.data_source}")

        # save transformer info (dataset-specific methods will be excluded)
        self.save_transformer_params("x")
        self.save_transformer_params("y")

        # conserve memory
        if self.del_unsplit_raw:
            self.x_raw = None
            self.y_raw = None

        if self.del_split_raw:
            self.x_train_raw = None
            self.x_test_raw = None
            self.y_train_raw = None
            self.y_test_raw = None

    @staticmethod
    def _transform_raw_1d2d(transformer, transformer_obj, train_raw, test_raw):
        """Apply ``.fit()`` and ``.transform()`` on train and test data
        according to scale method ``self.(x|y)_transformer``

        Args:
            transformer (str): see ``self.x_transform`` and ``self.y_transform``
            transformer_obj (LabelEncoder|StandardScaler|MinMaxScaler|OneHotEncode):
            train_raw (numpy.array): raw train data
            test_raw (numpy.array): raw test data

        Returns:
            LabelEncoder|StandardScaler|MinMaxScaler|OneHotEncoder: the transformer object
            numpy.array: transformed train data
            numpy.array: transformed test data
        """

        printl("_transform_raw_1d2d() begins...")

        # a. classification problems: fit by train + test to avoid unseen labels
        if transformer in ("o", "l"):

            # 1) train is 1D -> combine by hstack
            if train_raw.ndim == 1:
                transformer_obj.fit(np.hstack([train_raw, test_raw]))
                train = transformer_obj.transform(train_raw)
                test = transformer_obj.transform(test_raw)

            # 2) train.shape = (-1, 1) -> transform to 1D, fit, and transform back to (-1, 1)
            elif (train_raw.ndim == 2) and (train_raw.shape[1] == 1):
                transformer_obj.fit(np.vstack([train_raw, test_raw]).reshape(-1))
                train = transformer_obj.transform(train_raw.reshape(-1))
                test = transformer_obj.transform(test_raw.reshape(-1)).reshape((-1, 1))

            # does not allow real 2D or more dimesions
            else:
                raise NotImplementedError(f"Method={transformer}, Data dimension={train_raw.shape}")

        # b. function-fitting problem: fit by train only
        elif transformer in ("s", "n"):

            if train_raw.ndim in (1, 2):
                train = transformer_obj.fit_transform(train_raw).astype(np.float32)
                test = transformer_obj.transform(test_raw).astype(np.float32)

            else:
                raise NotImplementedError(f"Method={transformer}, Data dimension={train_raw.shape}")

        # c. default (1-D functions)
        else:
            train = train_raw
            test = test_raw

        printl("_transform_raw_1d2d() done!")

        return transformer_obj, train, test

    def save_transformer_params(self, which):
        """Save transformer info into ``self.other_results`` for future recovery.
        Dataset-specific transformation methods will be ignored automatically.

        Args:
            which (str): x or y
        """
        if which not in ("x", "y"):
            raise ValueError(f"Bad 'which': {which}")

        tr_obj = getattr(self, f"{which}_transformer_obj")

        if tr_obj is not None:  # ignore data-specific transformation methods
            dump = dump_sklearn_scaler_params(tr_obj, json_format=True)
            setattr(self, f"{which}_transformer_params", dump)

    def transform_x(self, x):

        # must reshape 1D array to pass into a transformer
        if x.ndim == 1:
            x = x.reshape(-1, 1)

        if self.data_source in ("mnist", "mnist-10", "mnist-7x7", "mnist-7x7-10"):
            return (x - 127.5) / 127.5

        elif self.data_source in ("peak-1d", "zigzag-1d", "natsign"):  # synthetic functionss
            return self.x_transformer_obj.transform(x)

        else:
            raise NotImplementedError(f"transform_x(): data source not understood: {self.data_source}")

    def transform_y(self, y):

        # must reshape 1D array to pass into a transformer
        if y.ndim == 1:
            y = y.reshape(-1, 1)

        if self.data_source in ("mnist", "mnist-10", "mnist-7x7", "mnist-7x7-10", "peak-1d", "zigzag-1d", "natsign"):
            return self.y_transformer_obj.transform(y)

        else:
            raise NotImplementedError(f"transform_y(): data source not understood: {self.data_source}")

    def inverse_transform_x(self, x):

        # must reshape 1D array to pass into a transformer
        if x.ndim == 1:
            x = x.reshape(-1, 1)

        if self.data_source in ("mnist", "mnist-10", "mnist-7x7", "mnist-7x7-10"):
            return x * 127.5 + 127.5

        elif self.data_source in ("peak-1d", "zigzag-1d", "natsign"):  # 1-D synthetic functions
            return self.x_transformer_obj.inverse_transform(x)

        else:
            raise NotImplementedError(f"inverse_transform_x(): data source not understood: {self.data_source}")

    def inverse_transform_y(self, y):

        # must reshape 1D array to pass into a transformer
        if y.ndim == 1:
            y = y.reshape(-1, 1)

        # classification problem: must take argmax
        if self.problem_type == "c":
            return self.y_transformer_obj.inverse_transform(y.argmax(axis=1))
        # function-fitting problem: transform directly
        elif self.problem_type == "f":
            return self.y_transformer_obj.inverse_transform(y)
        else:
            raise NotImplementedError(f"inverse_transform_y(): data source not understood: {self.data_source}")

    def s07_init_output_dirs(self):
        """Create output directories."""
        (self.out_dir / self.suffix_exp).mkdir(parents=True, exist_ok=True)
        (self.fig_dir / self.suffix_exp).mkdir(parents=True, exist_ok=True)

    def s08_save_train_test_data(self):
        """(Optional step) Save raw and transformed data for future inspections."""

        with h5py.File(str(self.out_dir / self.suffix_exp / "data.h5"), "w") as f:
            f.create_dataset("x_train_raw", data=self.x_train_raw)
            f.create_dataset("y_train_raw", data=self.y_train_raw)
            f.create_dataset("x_test_raw", data=self.x_test_raw)
            f.create_dataset("y_test_raw", data=self.y_test_raw)
            f.create_dataset("x_train", data=self.x_train)
            f.create_dataset("y_train", data=self.y_train)
            f.create_dataset("x_test", data=self.x_test)
            f.create_dataset("y_test", data=self.y_test)

        printl(f"Raw and transformed data copied to: {self.out_dir / self.suffix_exp}", "INFO")

    def s09_save_config_and_params(self):
        """Backup config and params file."""

        # config
        dest_dir = self.out_dir / self.suffix_exp / "config"
        dest_dir.mkdir(exist_ok=True)
        shutil.copy(self.config_file, dest_dir / "master.json")

        # params
        dest_dir = self.out_dir / self.suffix_exp / "params"
        dest_dir.mkdir(exist_ok=True)
        shutil.copy(self.param_file, dest_dir / "params.json")

        # hptune
        if self.tune_hp:
            shutil.copy(self.hptune_file, dest_dir / "hptune.json")

        printl(f"Config & params copied to: {self.out_dir / self.suffix_exp}", "INFO")

    # ======================================================================
    # 1. Model Building and Training
    # ======================================================================

    def s10_loop_hparams(self):
        """Hyperparameter tuning loop. Supports grid search only."""

        # load list of hyperparameters
        self.load_hparams(self.hptune_file)

        n_comb = self._get_n_hparam_combinations()

        # instantiate generator object
        generator = self._hparams_generator()

        # iterate through a set of hyperparams
        for i, hparams_now in enumerate(generator):
            printl(f"***Run {i+1}/{n_comb} of hparam tuning***: hparams={hparams_now}")
            self._apply_hparams_now(hparams_now)
            self.s11_loop_n_repeat()
        else:
            printl(f"All {n_comb} combinations of hparams have been tested!", "INFO")

    def load_hparams(self, hp_tuning_file):
        """load hparams. See ``self.loop_hparams``."""
        with open(hp_tuning_file) as f:
            self.hparams = json.load(f)

    def _hparams_generator(self):
        """Yield the next set of hyperparameter.

        Returns:
            dict: hyperparameters
        """
        # list of hparam names and values
        ls_keys = list(self.hparams.keys())
        ls_ls_vals = list(self.hparams.values())

        # generate hparams
        hparams_out = {}
        for it in itertools.product(*ls_ls_vals):  # iterate through combinations
            for i, val in enumerate(it):  # through hyperparameters
                hparams_out[ls_keys[i]] = val
            yield hparams_out

    def _get_n_hparam_combinations(self):
        """Get number of combinations of hparams.

        Returns:
            int
        """
        ans = 1
        for _, v in self.hparams.items():
            ans *= len(v)
        return ans

    def _apply_hparams_now(self, hparams_now):
        """Overwrite hyperparameters with hparam_now

        Args:
            hparams_now (dict): generated by ``_hparams_generator()``
        """
        # for debug when stop
        self._hparams_now = hparams_now

        # reset assigned parameters
        for k, v in hparams_now.items():
            setattr(self, k, v)

        # reset relevant entities
        self.reinit_derived_params()
        self._reinit_reproducibility()

    def s11_loop_n_repeat(self):
        """Repeat ``self.n_repeat`` times for a specific hyperparameter and
        parameter set.
        """
        for i in range(self.n_repeat):
            # Note: run i is unimportant because each independent run
            # should have equal chance of reaching the best result.
            printl(f"***Run {i+1}/{self.n_repeat} of repeated experiments***")
            self._s13_init_outputs()
            self._s14_init_model_components()
            self._s15_build_model()
            self._s17_train()
            self._s19_evaluation_info()
            self._s21_plot_train_history()
            self._s22_plot_prediction()
            self._s23_save_best_model()
            self._s24_save_history()
            self._s28_save_mongo()

    def _s13_init_outputs(self):
        """Reset output items."""

        # ======== accuracy ===========
        self.val_acc = -float('inf')

        # ===== training / evolution =====
        self.best_evo_stage = 0
        self.best_epoch = 0
        self.best_total_epoch = 0
        self.total_epochs = 0
        self.ls_epochs_each_stage = []

        # ===== output =====
        self.best_model_path = None
        self.best_model_plot_path = None
        self.history_plot_path = None
        self.history_path = None

    def _s14_init_model_components(self):
        """Initialize model components"""
        self._s14_1_set_metrics()
        self._s14_2_init_callbacks()
        self._s14_3_init_optimizer_and_lr()
        self._s14_4_set_weight_bias_initializers()

    def _s14_1_set_metrics(self):
        """Set metric"""
        if self.acc_metric.lower() == "r2_metric":
            self.ls_metrics = [r2_metric]  # this is a function
        else:
            self.ls_metrics = [self.acc_metric]  # this is a string

    def _s14_2_init_callbacks(self):
        """Initialize callback objects."""

        # reinit callbacks
        self.callbacks = []

        # 1. best callback (enforced at the first place)
        self.best_callback_obj = BestModelCallback()
        self.callbacks.append(self.best_callback_obj)

        # 2. ls_mask callback for evolutionary algorithms with epsilon < 1
        if (self.epsilon < 1) and (self.evo_method in self.ls_evo_methods):
            self.lm_callback_obj = LinkMaskingCallback(
                ls_masks=None,
                skip_output_layer=~self.evo_output_layer,
                apply_on_batch=True,
                batch_interval=self.evo_mask_interval
            )
            self.callbacks.append(self.lm_callback_obj)
        else:
            # must kill if not needed
            self.lm_callback_obj = None

        # 3. ReduceLRonPlateau
        if self.reduce_lr_on_plateau:
            self.callbacks.append(ReduceLROnPlateau(
                monitor="val_loss", factor=self.reduce_lr_factor, patience=self.patience,
                verbose=1, mode="auto", min_delta=self.min_loss_improvement, min_lr=self.min_lr
            ))

        # 4. EarlyStopping.
        # NOTE:
        #     1. restore_best_weights is not reliable! (keras Issue #12511)
        #     2. EarlyStopping should restart counting after ReduceLRonPlateau is applied.
        #        -> The patience in EarlyStopping = 2*patience in ReduceLRonPlateau!
        if self.early_stopping:
            self.callbacks.append(EarlyStopping(
                monitor='val_loss', min_delta=self.min_loss_improvement, patience=2*self.patience,
                verbose=1, mode='auto'
            ))

    def _s14_3_init_optimizer_and_lr(self):
        """Re-instantiate the optimizer and the learning rate.

        Note:
            Must do this even if ``ReduceLRonPlateau`` is not called,
            because the optimizer has internal states!
        """
        self.optimizer_obj = build_optimizer(self.optimizer, {"learning_rate": self.init_lr})

    def _s14_4_set_weight_bias_initializers(self):
        """
        Set weight and bias initializers.

        - Weights: always N(0,1)
        - Biases: N(0,1) if standardized; Uniform[0,1) if normalized.
        """
        self.weight_initializer_obj = self.get_tf_initializer(self.weight_initializer, self.weight_init_scale)
        self.bias_initializer_obj = self.get_tf_initializer(self.bias_initializer, self.bias_init_scale)

    @staticmethod
    def get_tf_initializer(initializer, scale):
        """Return an tensorflow initializer instance.

        Args:
            initializer (str): s=standardized=N(0, scale), n_docs=normalized=Uniform[0, scale),
            scale (float): scale factor

        Returns:
            tensorflow.keras.initializers.Initializer
        """

        if initializer == "glorot_normal":
            return initializers.GlorotNormal()
        elif initializer == "glorot_uniform":
            return initializers.GlorotUniform()
        elif initializer == "zeros":
            return initializers.Zeros()
        elif initializer == "random_normal":
            return initializers.RandomNormal(stddev=scale)
        elif initializer == "random_uniform":
            return initializers.RandomUniform(minval=-scale, maxval=scale)
        elif initializer == "truncated_normal":
            return initializers.TruncatedNormal(stddev=scale)
        else:
            raise NotImplementedError(f"Initializer string not understood: {initializer}")

    def _s15_build_model(self):
        """Build and compile keras model."""

        self.model = sequential_mlp(
            self.input_dim, self.output_dim, self.ls_max_neurons, self.hidden_activation,
            output_activation=self.output_activation, loss=self.loss_function,
            optimizer=self.optimizer_obj,
            batch_normalization=self.batch_normalization, dropout_rate=self.dropout_rate,
            kernel_initializer=self.weight_initializer_obj,
            bias_initializer=self.bias_initializer_obj,
            metrics=self.ls_metrics, do_compile=True,
            **self.other_params
        )

    def _s17_train(self):
        """Train the model."""

        # mark beginning time of this experiment
        self.timestamp_exp_begin_ns = time.time_ns()
        self.suffix = f"data={self.data_source}_evo={self.evo_method}_{self.timestamp_exp_begin_ns}"

        if self.evo_method == "NONE":
            self._s17_a_train_plain()
        elif self.evo_method == "NI":
            self._s17_b_train_ni()
        elif self.evo_method in self.ls_evo_methods:
            self._s17_c_train_evo()
        else:
            raise ValueError(f"evo_method not understood: {self.evo_method}")

        # mark training time
        self.timestamp_exp_end_ns = time.time_ns()
        self.exec_time = (self.timestamp_exp_end_ns - self.timestamp_exp_begin_ns) / 1e9
        printl(f"exec_time = {self.exec_time:.1f}s")

    def _s17_a_train_plain(self):
        """No evolution"""

        history = self.model.fit(
            x=self.x_train, y=self.y_train, batch_size=self.batch_size, epochs=self.epochs,
            validation_split=self.validation_split, verbose=1, callbacks=self.callbacks
        )

        # get history
        self.history = history.history

    def _s17_b_train_ni(self):
        """Neuron-increment (NI) evolutionary scheme.
        Loop through multiple NI stages with the help of ``FixedMsTrainer`` class.
        """

        # params for the model builder
        build_params = {
            "input_shape": (self.input_dim,),  # TODO: allow 2D
            "out_dim": self.output_dim,
            "ls_neurons": self.neuron_increment_stages,
            "hidden_activation": self.hidden_activation,
            "output_activation": self.output_activation,
            "loss": self.loss_function,
            "optimizer": self.optimizer,  # need not instantiate
            "batch_normalization": self.batch_normalization,
            "dropout_rate": self.dropout_rate,
            "kernel_initializer": self.weight_initializer_obj,
            "bias_initializer": self.bias_initializer_obj,
            "metrics": self.ls_metrics,
            "do_compile": True
        }

        optimizer_params = {"learning_rate": self.init_lr}

        # multistage callbacks
        multistage_callbacks = [
            RebuildSequentialMlpMsCallback(build_params=build_params,
                                           optimizer_params=optimizer_params),
            BestWeightsTransferMsCallback()
        ]

        # fit params
        fit_params = self.get_fit_params()

        self.multistage_trainer = FixedMsTrainer(
            self.x_train, self.y_train, self.n_neuron_increments,
            model=self.model,
            metric=self.acc_metric,  # string, not object!
            fit_params=fit_params,
            multistage_callbacks=multistage_callbacks
        )

        self.multistage_trainer.train()

        # need to get model reference back because model was rebuilt
        self.model = self.multistage_trainer.model

        # get history from multistage trainer
        self.history = self.multistage_trainer.history_container.history

    def _s17_c_train_evo(self):
        """Generic process for evolutionary training (NI doesn't count)."""

        # ========== multistage callbacks ==========
        # (1) Restore best weights at the beginning (to prevent the results from getting worse)
        bwt_ms_callback = BestWeightsTransferMsCallback()

        # (2) evolution callback
        evo_ms_callback = self.build_evo_ms_callback()

        # (3) reset learning rate callback (resets the optimizer)
        reset_lr_ms_callback = self.build_reset_optimizer_callback()

        ls_ms_callbacks = [bwt_ms_callback, evo_ms_callback, reset_lr_ms_callback]

        # ========== trainer ==========
        # model.fit() parameters
        fit_params = self.get_fit_params()

        self.multistage_trainer = FixedMsTrainer(
            self.x_train, self.y_train, self.n_evo_rounds,
            model=self.model,
            metric=self.acc_metric,  # string, not object!
            fit_params=fit_params,
            ms_callbacks=ls_ms_callbacks
        )

        # go
        self.multistage_trainer.train()

        # Need not reset the model because it was not rebuilt (although it is recompiled).

        # get history from the built-in history container within the multistage_trainer
        self.history = self.multistage_trainer.history_container.history

    def get_fit_params(self):
        """An internal method of ``train()``)."""
        return {
            "validation_split": self.validation_split,
            "batch_size": self.batch_size,
            "epochs": self.epochs,
            "callbacks": self.callbacks
        }

    def build_reset_optimizer_callback(self):
        """An internal method of ``train()``)."""
        return ResetOptimizerMsCallback(
            {"learning_rate": self.init_lr},
            {"optimizer": self.optimizer_obj, "loss": self.loss_function, "metrics": self.ls_metrics}
        )

    def build_evo_ms_callback(self):
        """Build the main evolution multistage callback

        Returns:
            EvoMsCallback
        """

        if self.evo_method == "LE":
            return LeMsCallback(
                self.epsilon, self.zeta, self.min_evo_scale, self.std_evo_scale,
                evo_output_layer=self.evo_output_layer,
                mask_callback=self.lm_callback_obj
            )
        elif self.evo_method == "RLE":
            return RleMsCallback(
                self.epsilon, self.zeta, self.min_evo_scale, self.std_evo_scale,
                evo_output_layer=self.evo_output_layer,
                mask_callback=self.lm_callback_obj
            )
        elif self.evo_method == "NE":
            return NeMsCallback(
                self.epsilon, self.zeta, self.min_evo_scale, self.std_evo_scale,
                neuron_evo_metric=self.neuron_evo_metric, p=self.neuron_evo_p,  # neuron-based
                evo_output_layer=self.evo_output_layer,
                mask_callback=self.lm_callback_obj
            )
        elif self.evo_method == "RNE":
            return RneMsCallback(
                self.epsilon, self.zeta, self.min_evo_scale, self.std_evo_scale,
                evo_output_layer=self.evo_output_layer,
                mask_callback=self.lm_callback_obj
            )
        else:
            raise ValueError(f"Evolution method not understood: {self.evo_method}")

    def _s19_evaluation_info(self):
        """Collect evaluationary information"""

        if self.evo_method == "NONE":
            self._s19_a_eval_plain()
        elif self.evo_method in (self.ls_evo_methods + ["NI"]):
            self._s19_b_multistage_generic()
        else:
            raise ValueError(f"evo_method not understood: {self.evo_method}")

        printl(f"best_epoch: {self.best_epoch}, ls_epochs_each_stage: {self.ls_epochs_each_stage}")
        printl(f"Best acc: train={self.train_acc:.4f}, val={self.val_acc:.4f}, test={self.test_acc:.4f}")

    def _s19_a_eval_plain(self):
        # epochs
        self.best_epoch = self.best_callback_obj.best_epoch
        self.total_epochs = self.best_callback_obj.epoch_now

        # accuracy
        self.train_acc = self.history[self.acc_metric][self.best_epoch - 1]
        self.val_acc = self.history[f"val_{self.acc_metric}"][self.best_total_epoch - 1]
        # model.evaluate returns (loss, metric)  # TODO: consider multiple metrics
        _, self.test_acc = self.model.evaluate(self.x_test, self.y_test)

    def _s19_b_multistage_generic(self):

        # epochs
        self.ls_epochs_each_stage = self.multistage_trainer.history_container.ls_epochs
        self.best_epoch = self.multistage_trainer.best_model_container.best_epoch
        self.best_evo_stage = self.multistage_trainer.best_model_container.best_stage
        self.best_total_epoch = count_through_list(self.ls_epochs_each_stage,
                                                   self.best_evo_stage, self.best_epoch)
        self.total_epochs = sum(self.ls_epochs_each_stage)

        # accuracy
        self.train_acc = self.history[self.acc_metric][self.best_total_epoch - 1]
        self.val_acc = self.history[f"val_{self.acc_metric}"][self.best_total_epoch - 1]
        # model.evaluate returns (loss, metric)(only 1 metric for now)
        _, self.test_acc = self.model.evaluate(self.x_test, self.y_test)

    # ======================================================================
    # 2. Save Results
    # ======================================================================

    def _s21_plot_train_history(self):
        """Plot acc_metric history."""
        # TODO: include all stages of evolution

        x = np.linspace(1, self.total_epochs, self.total_epochs)
        y1 = self.history[self.acc_metric]
        y2 = self.history[f"val_{self.acc_metric}"]
        labels = ["Train", "Validation"]
        xlabel = "epoch"
        ylabel = f"val_{self.acc_metric}"
        title = f"Train History: {self.acc_metric}"

        self.history_plot_path = self.fig_dir / self.suffix_exp / f"history_{self.suffix}.png"
        save_path = self.history_plot_path if self.save_fig else ""

        plot_multi_y(x, [y1, y2], labels, xlabel, ylabel, title,
                     plot_format="-",
                     show_fig=self.show_fig, figsize=self.figsize, dpi=self.dpi,
                     save_path=save_path)

    def _s22_plot_prediction(self):
        """Plot y_true vs y_pred"""
        if self.problem_type == "f":
            self._plot_yt_yp()
        elif self.problem_type == "c":
            self._plot_confusion_matrix()
        else:
            raise ValueError(f"problem_type not understood: {self.problem_type}")

    def _plot_yt_yp(self):
        """Plot y_true vs y_pred. TODO: fix when dim(x) > 1d."""
        x = self.x_transformer_obj.inverse_transform(self.x_test)
        y_true = self.y_transformer_obj.inverse_transform(self.y_test)
        y_pred = self.y_transformer_obj.inverse_transform(self.model.predict(self.x_test))
        xlabel = "x"
        ylabel = "y"
        title = "$Y_{true}$ vs $Y_{pred}$"

        self.best_model_plot_path = self.fig_dir / self.suffix_exp / f"yt_yp_{self.suffix}.png"
        save_path = self.best_model_plot_path if self.save_fig else None

        plot_true_vs_pred(x, [y_true, y_pred], xlabel, ylabel, title,
                          show_fig=self.show_fig, figsize=self.figsize,
                          dpi=self.dpi, save_path=save_path)

    def _plot_confusion_matrix(self):
        """Plot confusion matrix for classification problem"""

        y_pred = self.model.predict(self.x_test).argmax(axis=1)
        cm = confusion_matrix(self.y_test, y_pred)

        self.best_model_plot_path = self.fig_dir / self.suffix_exp / f"cm_{self.suffix}.png"
        save_path = self.best_model_plot_path if self.save_fig else None

        plot_confusion_matrix(cm, title="Confusion Matrix", show_fig=self.show_fig,
                              save_path=save_path)

    def _s23_save_best_model(self):
        """Save the weights of the best model."""
        self.best_model_path = self.out_dir / self.suffix_exp / f"weights_{self.suffix}.h5"
        self.model.save_weights(str(self.best_model_path))

    def _s24_save_history(self):
        """Save training history to a text file."""
        self.history_path = self.out_dir / self.suffix_exp / f"history_{self.suffix}.json"
        # save json which has no quotes as its contents.
        sdajson(self.history, self.history_path)

    def _s28_save_mongo(self):
        """Save results into MongoDB"""
        dic = {
            "tune_hp": self.tune_hp,
            "timestamp": self.timestamp,
            "timestamp_readable": self.suffix_ts,
            "timestamp_exp_begin_ns": self.timestamp_exp_begin_ns,
            "timestamp_exp_end_ns": self.timestamp_exp_end_ns,
            "exec_time": self.exec_time,
            "PYTHONHASHSEED": self.PYTHONHASHSEED,
            "TF_DETERMINISTIC_OPS": self.TF_DETERMINISTIC_OPS,
            "seed_np_random": self.seed_np_random,
            "seed_python_random": self.seed_python_random,
            "seed_tf_random": self.seed_tf_random,
            "data_source": self.data_source,
            "is_synthesized": self.is_synthesized,
            "problem_type": self.problem_type,
            "input_dim": self.input_dim,
            "output_dim": self.output_dim,
            "data_size": self.data_size,
            "data_params": self.data_params,
            "x_transformer": self.x_transformer,
            "y_transformer": self.y_transformer,
            "x_transformer_params": self.x_transformer_params,
            "y_transformer_params": self.y_transformer_params,
            "n_hidden_layers": self.n_hidden_layers,
            "ls_max_neurons": self.ls_max_neurons,
            "batch_normalization": self.batch_normalization,
            "dropout_rate": self.dropout_rate,
            "hidden_activation": self.hidden_activation,
            "output_activation": self.output_activation,
            "n_repeat": self.n_repeat,
            "same_train_test": self.same_train_test,
            "test_split": self.test_split,
            "validation_split": self.validation_split,
            "weight_initializer": self.weight_initializer,
            "weight_init_scale": self.weight_init_scale,
            "bias_initializer": self.bias_initializer,
            "bias_init_scale": self.bias_init_scale,
            "loss_function": self.loss_function,
            "signed_epochs": self.signed_epochs,
            "epochs": self.epochs,
            "batch_size": self.batch_size,
            "optimizer": self.optimizer,
            "reduce_lr_on_plateau": self.reduce_lr_on_plateau,
            "init_lr": self.init_lr,
            "min_lr_fraction": self.min_lr_fraction,
            "min_lr": self.min_lr,
            "reduce_lr_factor": self.reduce_lr_factor,
            "early_stopping": self.early_stopping,
            "min_loss_improvement": self.min_loss_improvement,
            "patience": self.patience,
            "evo_method": self.evo_method,
            "zeta": self.zeta,
            "epsilon": self.epsilon,
            "neuron_increment_stages": self.neuron_increment_stages,
            "n_neuron_increments": self.n_neuron_increments,
            "n_evo_rounds": self.n_evo_rounds,
            "min_evo_scale": self.min_evo_scale,
            "std_evo_scale": self.std_evo_scale,
            "neuron_evo_metric": self.neuron_evo_metric,
            "neuron_evo_p": self.neuron_evo_p,
            "evo_output_layer": self.evo_output_layer,
            "evo_mask_interval": self.evo_mask_interval,
            "other_params": self.other_params,
            "other_results": self.other_results,
            "acc_metric": self.acc_metric,
            "train_acc": self.train_acc,
            "val_acc": self.val_acc,
            "test_acc": self.test_acc,
            "best_evo_stage": self.best_evo_stage,
            "best_epoch": self.best_epoch,
            "best_total_epoch": self.best_total_epoch,
            "total_epochs": self.total_epochs,
            "ls_epochs_each_stage": self.ls_epochs_each_stage,
            "best_model_path": str(self.best_model_path.relative_to(self.base_dir)),
            "best_model_plot_path": str(self.best_model_plot_path.relative_to(self.base_dir)),
            "history_plot_path": str(self.history_plot_path.relative_to(self.base_dir)),
            "history_path": str(self.history_path.relative_to(self.base_dir))
        }
        self.coll_obj.insert_one(dic)
        printl(f"Saved into MongoDB: No. {self.timestamp_ns}", "INFO")

    def get_n_stages(self):
        """Get number of stages in a multistage training run."""

        if self.evo_method == "NI":
            return self.n_neuron_increments

        elif self.evo_method in ("LE", "RLE", "NE", "RNE"):
            return self.n_evo_rounds

        elif self.evo_method == "NONE":
            return 1

        else:
            ValueError(f"Evo_method not understood: {self.evo_method}")


class EvoAggr(BaseAggrContainer):
    """Calculate aggregate statistics from hyperparam tuning results."""

    # ===============================
    # I/O Artifacts
    # ===============================
    # EvoAggr: won't save anything

    # ===============================
    # Statistics To Be Reported
    # ===============================
    # EvoAggr: inherits everything

    #: list[str]: names of history components
    ls_history_components = ["loss", "val_loss", "metric", "val_metric", "lr"]

    def __init__(self, dc, suffix_aggr, **kwargs):
        """Initialize an EvoAggr instance.

        Args:
            dc (EvoContainer): the restored data container to be analyzed
            suffix_aggr (str): main suffix for the aggregation results
        """
        # EvoAggr-specific check
        assert isinstance(dc, EvoContainer)

        super(EvoAggr, self).__init__(dc, **kwargs)
        self.reset_dc()

        # =================================
        # Suffixes and paths
        # =================================

        #: str: main suffix for the outputs of this aggregation
        self.suffix_aggr = suffix_aggr

        # int: timestamp for this aggregation instance
        self.timestamp_aggr = int(datetime.now().timestamp())
        # int: readable timestamp suffix to identify different runs
        self.suffix_ts_aggr = self.timestamp_to_suffix(self.timestamp_aggr)

        #: Path: aggregate stats subfolder
        self.out_dir = self.dc.out_dir / self.suffix_aggr
        self.out_dir.mkdir(parents=True, exist_ok=True)

        #: Path: aggregate stats subfolder
        self.fig_dir = self.dc.fig_dir / self.suffix_aggr
        self.fig_dir.mkdir(parents=True, exist_ok=True)

        #: Path: history h5 path
        self.hist_h5_path = self.out_dir / "history.h5"

        # =================================
        # Retrieved Data
        # =================================

        #: list[dict]: raw data of retrieved documents
        self.ls_docs = None
        #: int: number of retrieved docs in ``self.ls_docs``
        self.n_docs = -1

        # =================================
        # Result Datasets
        # =================================

        #: DataFrame: dataframe converted from ``self.dic``
        self.df = None

        #: pandas.DataFrame: main accuracy stats
        self.df_acc_overall = None

        #: pandas.DataFrame: accuracy stats by hparam values and accuracy groups
        self.df_acc_by_hp = None

        #: pandas.DataFrame: The table for choosing hparam values
        self.df_choose_hp = None

        # =================================
        # Predictions
        # =================================

        #: int: default number of data points for plotting
        self.n_pts = 1001

        #: numpy.array: x value for plotting (from ``np.linspace``)
        self.x_plot = None
        #: numpy.array: y_true for plotting
        self.yt_plot = None
        #: numpy.array: y_pred for plotting, all docs (costly!)
        self.yp_plot = None

        # ================================
        # History
        # ================================

        #: int: total stages of training (0=indefinite)
        self.n_stages = -1

        #: int: #epochs per stage (0=indefinite)
        self.n_epochs_per_stage = -1

        #: int: #epochs in all stages (0=indefinite)
        self.total_epochs = -1

        #: numpy.array: Multistage loss history. shape=(n_rec, total_epochs).
        self.arr_loss = None

        #: numpy.array: Multistage val_loss history. shape=(n_rec, total_epochs).
        self.arr_val_loss = None

        #: numpy.array: Multistage acc_metric history. shape=(n_rec, total_epochs).
        self.arr_metric = None

        #: numpy.array: Multistage val_acc_metric history. shape=(n_rec, total_epochs).
        self.arr_val_metric = None

        #: numpy.array: Multistage learning rate history. shape=(n_rec, total_epochs).
        self.arr_lr = None

    # =======================================
    # s3x. download and preprocess records
    # =======================================

    def reset_dc(self):
        """Reset data container after import"""
        self.dc.suffix_exp = self.dc.param_file.parent.parent.name

    def s32_get_all_docs(self, match, projection=None):
        """Retreive all docs to be analyzed.

        Args:
            match (dict): MongoDB-style match string
            projection (dict|None): MongoDB-style projection string
        """

        if projection is not None:
            generator = self.dc.coll_obj.find(match, projection)
        else:
            generator = self.dc.coll_obj.find(match)

        self.ls_docs = list(generator)
        self.n_docs = len(self.ls_docs)
        printl(f"{self.n_docs} records retrieved!", "INFO")

    def s35_to_pandas(self):
        self.df = pd.DataFrame.from_dict(self.ls_docs)

    def s36_add_group_tags(self):
        self.df["train_acc_gp"] = self.df["train_acc"].apply(self.gp_acc_metric)
        self.df["val_acc_gp"] = self.df["val_acc"].apply(self.gp_acc_metric)
        self.df["test_acc_gp"] = self.df["test_acc"].apply(self.gp_acc_metric)

    @staticmethod
    def gp_acc_metric(acc):
        """Assign group number to the accuracy metric.

        Args:
            acc (float): accuracy

        Returns:
            str: stringified label
        """
        # TODO: customize "good" value

        if acc >= 0.5:  # reasonable
            return "0"
        elif 0 < acc < 0.5:  # poor
            return "1"
        else:  # unacceptable
            return "2"

    # =======================================
    # s4x. Analyze Accuracy
    # =======================================

    def s40_acc_aggr_with_subset(self, subset=None, subset_suffix=None):
        """Redo s41~s45 but based on subset only.

        Args:
            subset (dict|None): mongodb-style subsetter
            subset_suffix (str|None): suffix for this dataset
        """
        if subset is not None:
            # if subset is given, it must give a name
            assert subset_suffix is not None
        else:
            # otherwise, no name should be given
            assert subset_suffix is None

        self.s41_calc_acc_overall(subset=subset, subset_suffix=subset_suffix)
        self.s42_acc_by_param_and_gp(subset=subset, subset_suffix=subset_suffix)
        self.s45_compare_acc_by_hparam(subset=subset, subset_suffix=subset_suffix)

    def s41_calc_acc_overall(self, subset=None, subset_suffix=None):
        """Batch overall average, as a basis of comparison.

        Args:
            subset (dict): subset ``self.df``
            subset_suffix (str): name suffix of the dataframe
        """

        # define output targets
        if subset is None:
            df_acc_overall = "df_acc_overall"
        else:
            df_acc_overall = f"df_acc_overall_{subset_suffix}"

        out_path = Path(self.out_dir / f"{df_acc_overall}.csv")

        # create schema
        self.init_df_acc_overall(subset=subset, subset_suffix=subset_suffix)

        # compute stat for train, val and test respectively
        for ds in ("train", "val", "test"):

            if subset is None:
                df = self.df
            else:
                df = self.simple_subset_by_dict(self.df, subset)

            self.compute_stats(getattr(self, df_acc_overall),  # self.df_acc_overall
                               (ds,),
                               df[f"{ds}_acc"].values,  # self.df or its subset
                               do_num=True)
        # save
        getattr(self, df_acc_overall).to_csv(out_path)

    def init_df_acc_overall(self, subset=None, subset_suffix=None):
        """Initialize the overall accuracy report.
        Split by train, val and test."""

        if subset is None:
            df_acc_overall = "df_acc_overall"
        else:
            df_acc_overall = f"df_acc_overall_{subset_suffix}"

        # put index columns
        setattr(self, df_acc_overall, pd.DataFrame({"ds": ["train", "val", "test"]}))

        # create stats columns
        self.create_stats_columns(getattr(self, df_acc_overall), do_num=True)

        # set index
        getattr(self, df_acc_overall).set_index("ds", inplace=True)

    @staticmethod
    def simple_subset_by_dict(df, subset):
        """Simple subsetter.

        Args:
            df (pandas.DataFrame): original subset
            subset (dict): simple selection

        Returns:
            pandas.DataFrame
        """
        n = len(df)
        mask = np.ones(n, dtype=bool)

        for k, v in subset.items():
            mask &= (df[k] == v)

        return df[mask]

    def s42_acc_by_param_and_gp(self, subset=None, subset_suffix=None):
        """Report stability & accuracy by each param and each group.

        Args:
            subset (dict): subset ``self.df``
            subset_suffix (str): name suffix of the dataframe
        """

        # define output targets
        if subset is None:
            df_acc_by_hp = "df_acc_by_hp"
        else:
            assert subset_suffix is not None  # must give a name
            df_acc_by_hp = f"df_acc_by_hp_{subset_suffix}"

        out_path = Path(self.out_dir / f"{df_acc_by_hp}.csv")

        # schema
        self.init_df_acc_by_hp(subset=subset, subset_suffix=subset_suffix)

        # for each hparam name and value
        for hp in self.dc.hparams:
            for hp_val in self.dc.hparams[hp]:

                # subset data
                if subset is None:
                    df = self.df
                else:
                    df = self.simple_subset_by_dict(self.df, subset)

                # subset data from that hparam value
                df_all = df.loc[df[hp] == hp_val]

                # for every data slice
                for ds in ("train", "val", "test"):

                    acc_name = f"{ds}_acc"
                    acc_gp_name = f"{ds}_acc_gp"

                    # compute stats by group
                    for gp in ("0", "1", "2"):
                        df = df_all[df_all[acc_gp_name].values == gp]  # subset data
                        self.compute_stats(getattr(self, df_acc_by_hp),
                                           (hp, hp_val, ds, gp),
                                           df[acc_name].values,
                                           do_num=True)
                    # gp = "0+1"
                    df = df_all[(df_all[acc_gp_name].values == "0") | (df_all[acc_gp_name].values == "1")]
                    self.compute_stats(getattr(self, df_acc_by_hp),
                                       (hp, hp_val, ds, "0+1"),
                                       df[acc_name].values,
                                       do_num=True)
                    # gp = "all"
                    df = df_all
                    self.compute_stats(getattr(self, df_acc_by_hp),
                                       (hp, hp_val, ds, "all"),
                                       df[acc_name].values,
                                       do_num=True)

        # save to file
        getattr(self, df_acc_by_hp).to_csv(out_path)

    def init_df_acc_by_hp(self, subset=None, subset_suffix=None):
        """Initialize ``self.df_acc_by_hp``.

        Note:
            - Index = hparams and subgroups = ``ls_hp x (train, val, test) x gp_acc_metric``
            - Columns = conti. dist. stats and/or discrete stats

        Args:
            subset (dict): subset ``self.df``
            subset_suffix (str): name suffix of the dataframe
        """

        # define output targets
        if subset is None:
            df_acc_by_hp = "df_acc_by_hp"
        else:
            assert subset_suffix is not None  # must give a name
            df_acc_by_hp = f"df_acc_by_hp_{subset_suffix}"

        # setup index columns
        ls_idx = [(hp, hp_val, name, gp)
                  for hp in self.dc.hparams.keys()
                  for hp_val in self.dc.hparams[hp]
                  for name in ("train", "val", "test")
                  for gp in ("0", "1", "2", "0+1", "all")]  # supgroups

        n_rows = len(ls_idx)
        setattr(self, df_acc_by_hp, pd.DataFrame(ls_idx, columns=["hp", "hp_val", "ds", "gp"]))

        # for numerical params, report numeric stats
        self.create_stats_columns(getattr(self, df_acc_by_hp), do_num=True)

        # set index
        getattr(self, df_acc_by_hp).set_index(["hp", "hp_val", "ds", "gp"], inplace=True)

        # performance....?
        getattr(self, df_acc_by_hp).sort_index(inplace=True)

    def s45_compare_acc_by_hparam(self, ds="test", gp="all",
                                  subset=None, subset_suffix=None,
                                  best_threshold=0.99):
        """Compare accuracy between different values of each hparam.

        Args:
            ds (str): which slice
            gp (str): which group of values
            subset (dict): subset ``self.df``
            subset_suffix (str): name suffix of the dataframe
            best_threshold (float): define ``acc >= best_threshold`` as the best result
        """

        # define output targets
        if subset is None:
            df_acc_overall = "df_acc_overall"
            df_acc_by_hp = "df_acc_by_hp"
            df_choose_hp = "df_choose_hp"
        else:
            assert subset_suffix is not None  # must give a name
            df_acc_overall = f"df_acc_overall_{subset_suffix}"
            df_acc_by_hp = f"df_acc_by_hp_{subset_suffix}"
            df_choose_hp = f"df_choose_hp_{subset_suffix}"

        out_path = Path(self.out_dir / f"{df_choose_hp}.csv")

        # create output array
        n = 1  # an extra 1 for the overall column
        for hp in self.dc.hparams.keys():
            n += len(self.dc.hparams[hp])

        # output series
        dic_arr = {
            "hp": np.empty(n, dtype=object),
            "hp_val": np.empty(n, dtype=object),
            "ds": np.empty(n, dtype=object),
            "gp": np.empty(n, dtype=object),
            "n": np.zeros(n, dtype=int),
            "min": np.full(n, np.nan, dtype=float),
            "q05": np.full(n, np.nan, dtype=float),
            "median": np.full(n, np.nan, dtype=float),
            "q95": np.full(n, np.nan, dtype=float),
            "q975": np.full(n, np.nan, dtype=float),
            "max": np.full(n, np.nan, dtype=float),
            "avg": np.full(n, np.nan, dtype=float),
            "std": np.full(n, np.nan, dtype=float),
            "n_best": np.zeros(n, dtype=int),  # must define "best"
            "best_rate": np.full(n, np.nan, dtype=float),
        }

        # 1. overall results
        dic_arr["hp"][0] = "(overall)"
        dic_arr["hp_val"][0] = "(overall)"
        dic_arr["ds"][0] = ds
        dic_arr["gp"][0] = gp
        dic_arr["n"][0] = getattr(self, df_acc_overall).loc["test", "n"]
        dic_arr["min"][0] = getattr(self, df_acc_overall).loc["test", "min"]
        dic_arr["q05"][0] = getattr(self, df_acc_overall).loc["test", "q05"]
        dic_arr["median"][0] = getattr(self, df_acc_overall).loc["test", "median"]
        dic_arr["q95"][0] = getattr(self, df_acc_overall).loc["test", "q95"]
        dic_arr["q975"][0] = getattr(self, df_acc_overall).loc["test", "q975"]
        dic_arr["max"][0] = getattr(self, df_acc_overall).loc["test", "max"]
        dic_arr["avg"][0] = getattr(self, df_acc_overall).loc["test", "avg"]
        dic_arr["std"][0] = getattr(self, df_acc_overall).loc["test", "std"]
        dic_arr["n_best"][0] = self.count_n_best(best_threshold, subset=subset, ds=ds)
        dic_arr["best_rate"][0] = dic_arr["n_best"][0] / dic_arr["n"][0]

        # 2. loop hparams
        i = 1
        for hp in self.dc.hparams.keys():

            if subset is None:
                subset_now = {}
            else:
                subset_now = subset.copy()

            for hp_val in self.dc.hparams[hp]:
                idx = (hp, hp_val, ds, gp)

                subset_now.update({hp: hp_val})

                dic_arr["hp"][i] = hp
                dic_arr["hp_val"][i] = hp_val
                dic_arr["ds"][i] = ds
                dic_arr["gp"][i] = gp
                dic_arr["n"][i] = getattr(self, df_acc_by_hp).loc[idx, "n"]
                dic_arr["min"][i] = getattr(self, df_acc_by_hp).loc[idx, "min"]
                dic_arr["q05"][i] = getattr(self, df_acc_by_hp).loc[idx, "q05"]
                dic_arr["median"][i] = getattr(self, df_acc_by_hp).loc[idx, "median"]
                dic_arr["q95"][i] = getattr(self, df_acc_by_hp).loc[idx, "q95"]
                dic_arr["q975"][i] = getattr(self, df_acc_by_hp).loc[idx, "q975"]
                dic_arr["max"][i] = getattr(self, df_acc_by_hp).loc[idx, "max"]
                dic_arr["avg"][i] = getattr(self, df_acc_by_hp).loc[idx, "avg"]
                dic_arr["std"][i] = getattr(self, df_acc_by_hp).loc[idx, "std"]
                dic_arr["n_best"][i] = self.count_n_best(best_threshold, subset=subset_now, ds=ds)
                dic_arr["best_rate"][i] = dic_arr["n_best"][i] / dic_arr["n"][i]
                i += 1

        # create dataframe
        setattr(self, df_choose_hp, pd.DataFrame(dic_arr))
        getattr(self, df_choose_hp).set_index(["hp", "hp_val", "ds", "gp"], inplace=True)
        # save
        getattr(self, df_choose_hp).to_csv(out_path)

    def count_n_best(self, best_threshold, subset=None, ds="test"):
        """Count how many records satisfies ``test_acc >= best_threshold``

        Args:
            best_threshold (float): a number in (0, 1)
            subset (dict): a dict subsetter
            ds (str): data slice = train / val / test

        Returns:
            int
        """
        # master dataset
        if subset is None:
            df = self.df
        else:
            df = self.simple_subset_by_dict(self.df, subset)

        # test dataset
        return np.count_nonzero(df[f"{ds}_acc"] >= best_threshold)

    # =============================================
    # s5x. Average Predictions
    # =============================================

    def s51_recover_data_and_transformers(self, chk=False):
        """Recover y_pred from all records

        Args:
            chk (bool): check if re-preprocessed data matched
                the saved data.
        """

        # reset the data container
        # 1. reload saved data
        self.load_saved_data()

        # 2. re-preprocess to recover the variable transformer
        if chk:
            x_test = deepcopy(self.dc.x_test)
            y_train = deepcopy(self.dc.y_train)

        self.dc.s05_transform()

        # check reproducibility
        if chk:
            assert np.all(x_test == self.dc.x_test)
            assert np.all(y_train == self.dc.y_train)

    def load_saved_data(self):
        """Load train/test raw/transformed x and y datasets in
        the data container.
        """

        with h5py.File(str(self.dc.out_dir / self.dc.suffix_exp / "data.h5"), "r") as f:
            self.dc.x_train_raw = f["x_train_raw"][:]
            self.dc.y_train_raw = f["y_train_raw"][:]
            self.dc.x_train = f["x_train"][:]
            self.dc.y_train = f["y_train"][:]
            self.dc.x_test_raw = f["x_test_raw"][:]
            self.dc.y_test_raw = f["y_test_raw"][:]
            self.dc.x_test = f["x_test"][:]
            self.dc.y_test = f["y_test"][:]

    def s52_set_x_yt_plot(self):
        """Set ``self.x_plot`` and ``self.yt_plot`` for plotting.
        For a synthesized "non-noisy" dataset, generate a uniformly spaced
        synthesized dataset for prettier plot.
        For real datasets, simply copy x_test_raw and y_test_raw.
        """
        is_noisy = (self.dc.data_params["sigma"] > 0)

        if self.dc.data_source == "peak-1d":
            # if noisy, load data and argsort
            if is_noisy:
                self.set_x_yt_argsort()
            else:  # else regenerate linspace
                self.x_plot = np.linspace(-2.0, 2.0, num=self.n_pts, dtype=np.float32)
                self.yt_plot = np.array(list(map(f_peak_1d, self.x_plot)), dtype=np.float32)

        elif self.dc.data_source == "zigzag-1d":
            if is_noisy:
                self.set_x_yt_argsort()
            else:
                self.x_plot = np.linspace(-3.0, 3.0, num=self.n_pts, dtype=np.float32)
                self.yt_plot = np.array(list(map(f_zigzag, self.x_plot)), dtype=np.float32)

        # classification problem: ordering doesn't matter
        elif self.dc.problem_type == "c":
            self.x_plot = self.dc.x_test_raw
            self.yt_plot = self.dc.y_test_raw
        else:
            raise NotImplementedError(f"Haven't considered data={self.dc.data_source}, problem type={self.dc.problem_type})!")

    def set_x_yt_argsort(self):
        """Argsort 1D array to avoid messy lines in plots."""
        ind = self.dc.x_test_raw.reshape(-1).argsort()
        self.x_plot = self.dc.x_test_raw.reshape(-1)[ind]
        self.yt_plot = self.dc.y_test_raw.reshape(-1)[ind]

    def s53_recover_yp_plot(self, load_yp_plot=False):
        """Recover yp by reconstruction or load reconstructed data.
        If the data doesn't exist or auto-load is not intended, rebuild.

        Args:
            load_yp_plot (bool): load predicted value automatically
        """
        # load existing data only when intended and the data exist
        if load_yp_plot and (self.out_dir / "x_yt_yp.h5").exists():
            self.load_yp_plot()
        else:
            self.rebuild_yp_plot()

    def rebuild_yp_plot(self):
        """Recover model coefficients"""

        # storage
        self.yp_plot = np.zeros((self.n_docs, len(self.yt_plot)), dtype=np.float32)

        for i, doc in enumerate(self.ls_docs):

            printl(f"Recovering record {i + 1}/{self.n_docs} ...")

            # recover best model
            self.recover_best_model(doc)

            # fit: transform -> predict -> inverse_transform
            x_tr = self.dc.transform_x(self.x_plot)
            y_pred_tr = self.dc.model.predict(x_tr)
            yp = self.dc.inverse_transform_y(y_pred_tr)

            # store
            self.yp_plot[i, :] = yp.reshape(1, -1)

        # save results for future analysis
        with h5py.File(str(self.out_dir / "x_yt_yp.h5"), "w") as f:
            f.create_dataset("x", data=self.x_plot, dtype=np.float32)
            f.create_dataset("yt", data=self.yt_plot, dtype=np.float32)
            f.create_dataset("yp", data=self.yp_plot, dtype=np.float32)

    def recover_best_model(self, doc):
        """Rebuild the best model for each document.

        Args:
            doc (dict): retreived document
        """

        # must recover hparams for each run!
        if self.dc.hparams:  # only if not empty
            self.recover_hparams(doc)

        # recover model skeleton
        self.dc._s13_init_outputs()
        self.dc._s14_init_model_components()
        self.dc._s15_build_model()

        # recover model coefficients
        best_model_path = self.dc.base_dir / doc["best_model_path"]
        self.dc.model.load_weights(str(best_model_path))

    def recover_hparams(self, doc):
        """recover hparams after each load

        Args:
            doc (dict): retreived document
        """
        # retrieve hparams
        for hp in self.dc.hparams.keys():
            setattr(self.dc, hp, doc[hp])

        # reinit derived parameters
        self.dc.reinit_derived_params()

    def load_yp_plot(self):
        """Load yp_plot from saved."""
        with h5py.File(str(self.out_dir / "x_yt_yp.h5"), "r") as f:
            self.x_plot = f["x"][:]
            self.yt_plot = f["yt"][:]
            self.yp_plot = f["yp"][:]

    def s54_plot_true_vs_pred_by_hp(self):
        """Plot prediction band or confusion matrix for every single hparam value."""

        if self.dc.hparams.items():  # not empty
            for k, ls_v in self.dc.hparams.items():
                for v in ls_v:
                    self.plot_by_hp_subset({k: v})

        # plot overall average anyway
        self.plot_by_hp_subset(None)

    def plot_by_hp_subset(self, subset):
        if self.dc.problem_type == "f":
            self.plot_yt_yp_by_hp_subset(subset)
        elif self.dc.problem_type == "c":
            self.plot_cm_by_hp_subset(subset)

    def plot_yt_yp_by_hp_subset(self, dic_subset=None):
        """plot avg prediction for a single data source.

        Args:
            dic_subset (dict): the mongodb-style subsetting query
        """

        # set suffix and path
        if dic_subset:
            file_suffix = simple_dict_to_suffix(dic_subset)
            fig_path = self.fig_dir / f"avg_yt_yp_{file_suffix}.png"
            h5_path = self.out_dir / f"avg_yt_yp_{file_suffix}.h5"
            # subset the data
            ls_idx = subset_dicts_by_dict(self.ls_docs, dic_subset, ret=1)
            arr = self.yp_plot[ls_idx, :]
        else:
            file_suffix = ""
            fig_path = self.fig_dir / "avg_yt_yp.png"
            h5_path = self.out_dir / "avg_yt_yp.h5"
            arr = self.yp_plot

        # accumulate
        # a symmetric 90% empirical prediction interval
        yp_l, yp, yp_u = median_and_empirical_ci(arr)
        yp_std = arr.std(axis=0)

        # plot lines + CI shade
        x = self.x_plot.reshape(-1)
        ls_y = [self.yt_plot, yp, yp_u, yp_l]
        ls_labels = ["$Y_{true}$", "$Y_{med}$", "$Y_{95}$", "$Y_{05}$"]
        xlabel = "x"
        ylabel = "y"
        title = "Empirical 90% C.I. of $Y_{pred}$"
        plot_format = ["-", "--", ":", ":"]  # "-", "--", ":", "-."

        plot_multi_y(x, ls_y, ls_labels, xlabel, ylabel, title,
                     plot_format=plot_format,
                     linewidth=[2, 2, 1, 1],
                     color=["#939090", "navy", "navy", "navy"],
                     fontsize_legend=12,
                     fill_between=(2, 3), fill_alpha=0.1, fill_color="#666666",
                     show_fig=False, save_path=fig_path)

        # save data
        with h5py.File(str(h5_path), "w") as f:
            f.create_dataset("x", data=self.x_plot, dtype=np.float32)
            f.create_dataset("yt", data=self.yt_plot, dtype=np.float32)
            f.create_dataset("yp_avg", data=yp, dtype=np.float32)
            f.create_dataset("yp_std", data=yp_std, dtype=np.float32)
            f.create_dataset("yp_l", data=yp_l, dtype=np.float32)
            f.create_dataset("yp_u", data=yp_u, dtype=np.float32)

    def plot_cm_by_hp_subset(self, dic_subset=None):
        """Plot avg confusion matrix for a single data subset.

        Args:
            dic_subset (dict): the mongodb-style subsetting query
        """

        # set suffix and path
        if dic_subset:
            file_suffix = simple_dict_to_suffix(dic_subset)
            fig_path = self.fig_dir / f"avg_cm_{file_suffix}.png"
            h5_path = self.out_dir / f"avg_cm_{file_suffix}.h5"
            # subset dataset
            ls_idx = subset_dicts_by_dict(self.ls_docs, dic_subset, ret=1)
            arr = self.yp_plot[ls_idx, :]  # labels
        else:
            file_suffix = ""
            fig_path = self.fig_dir / "avg_cm.png"
            h5_path = self.out_dir / "avg_cm.h5"
            arr = self.yp_plot

        n = len(arr)

        # accumulate
        yt = np.tile(self.yt_plot.reshape(-1), n)
        yp = arr.reshape(-1)

        cm = confusion_matrix(yt, yp)

        plot_confusion_matrix(cm, title="Confusion Matrix", save_path=fig_path)

        # save data
        with h5py.File(str(h5_path), "w") as f:
            f.create_dataset("yt_all", data=yt, dtype=np.uint8)
            f.create_dataset("yp_all", data=yp, dtype=np.uint8)

    # =================================================
    # History
    # =================================================

    def s60_plot_history(self, subset=None, load_h5=True):
        """Fully automated history plotter. Guards against variable epochs.

        Args:
            subset (dict|None): plot a subset of ``self.df``
            load_h5 (bool): reuse the saved history.h5 file if possible

        Todo:
            (future) support for variable #epochs.
        """

        # load history.h5 if the file exists and doesn't want to rebuild
        if self.hist_h5_path.exists() and load_h5:
            self.s61_load_history_h5()
        elif self.chk_fixed_epochs():
            self.s61_collect_history(subset=subset)
        else:
            printl(f"Unable to collect history together because #epochs is not constant!")
            return

        self.s62_plot_epoch_history()
        self.s63_plot_epoch_history_by_stage()
        self.s65_plot_stage_history()

    def s61_collect_history(self, subset=None):
        """Load (multistage) history from raw json files.

        Todo:
            (future) support for variable #epochs.
        """

        if subset is None:
            ls_docs = self.ls_docs
        else:
            ls_docs = subset_dicts_by_dict(self.ls_docs, subset)

        # size
        n = len(ls_docs)
        self.set_n_stages_and_epochs()

        # temp storage
        self.arr_loss = np.zeros((n, self.total_epochs))
        self.arr_val_loss = np.zeros((n, self.total_epochs))
        self.arr_metric = np.zeros((n, self.total_epochs))
        self.arr_val_metric = np.zeros((n, self.total_epochs))
        self.arr_lr = np.zeros((n, self.total_epochs))

        for i, doc in enumerate(ls_docs):

            with open(self.dc.base_dir / doc["history_path"]) as fp:
                dic = json.load(fp)

            self.arr_loss[i, :] = dic["loss"]
            self.arr_val_loss[i, :] = dic["val_loss"]
            self.arr_metric[i, :] = dic[f"{self.dc.acc_metric}"]
            self.arr_val_metric[i, :] = dic[f"val_{self.dc.acc_metric}"]
            self.arr_lr[i, :] = dic[f"lr"]

        # dump
        with h5py.File(str(self.hist_h5_path), "w") as f:
            f.create_dataset("loss", data=self.arr_loss, dtype=np.float32)
            f.create_dataset("val_loss", data=self.arr_val_loss, dtype=np.float32)
            f.create_dataset("metric", data=self.arr_metric, dtype=np.float32)
            f.create_dataset("val_metric", data=self.arr_val_metric, dtype=np.float32)
            f.create_dataset("lr", data=self.arr_lr, dtype=np.float32)

    def s61_load_history_h5(self, h5_path=None):
        """Load history from a saved h5 file.

        Args:
            h5_path (Path|str): override default ``self.hist_h5_path``
        """
        self.set_n_stages_and_epochs()

        if h5_path:
            fpath = h5_path
        else:
            fpath = self.hist_h5_path

        with h5py.File(str(fpath), "r") as f:
            self.arr_loss = f["loss"][:]
            self.arr_val_loss = f["val_loss"][:]
            self.arr_metric = f["metric"][:]
            self.arr_val_metric = f["val_metric"][:]
            self.arr_lr = f["lr"][:]

    def chk_fixed_epochs(self):
        """Check whether the number of epochs is fixed.

        This function looks into every document retrieved. This is
        somewhat inefficient but can process correctly if hp-tuning
        is run or ``self.ls_docs`` is subset.
        """

        # Check whether early_stopping's are all False. If not, return False
        if not np.all(~self.df["early_stopping"].values):
            return False

        # #epochs must be the same
        elif len(self.df["epochs"].unique()) > 1:
            return False

        else:
            return True

    def set_n_stages_and_epochs(self, i_doc=0):
        """Set ``self.n_stage`` and ``self.total_epochs``

        Args:
            i_doc (int): the index of the document from which #epochs is retrieved
        """
        self.n_stages = self.dc.get_n_stages()
        self.n_epochs_per_stage = self.dc.epochs
        self.total_epochs = self.ls_docs[i_doc]["epochs"] * self.n_stages
        assert self.total_epochs == self.n_epochs_per_stage * self.n_stages, f"total_epochs={self.total_epochs}, n_epochs_per_stage={self.n_epochs_per_stage}, n_stages={self.n_stages}"

    def s62_plot_epoch_history(self, ci=0.9):
        """Plot history components over every epoch (across all stages).

        Args:
            ci (float): confidence interval (default=0.9)
        """
        for which in self.ls_history_components:
            self.plot_epoch_history_component(which, ci)

    def plot_epoch_history_component(self, which, ci=0.9):
        """Plot a component of the epoch history.

        Args:
            which (str): history component
            ci (float): confidence interval
        """

        # select arr
        arr = self.select_history_component_data(which, by_stage=False)

        # validation loss
        line_l, line_avg, line_u = median_and_empirical_ci(arr, ci=ci, axis=0)

        x = np.array(range(1, self.total_epochs + 1), dtype=int)
        ls_y = [line_avg, line_u, line_l]
        ls_labels = ["med", "q95", "q05"]
        xlabel = "epoch"
        ylabel = self.generate_history_ylabel(which)
        title = self.generate_history_title(which, ci=ci)
        plot_format = ["-", ":", ":"]  # "-", "--", ":", "-."
        ylim = self.generate_history_ylim(which)
        fig_path = self.fig_dir / f"hist_{which}.png"

        plot_multi_y(x, ls_y, ls_labels, xlabel, ylabel, title,
                     plot_format=plot_format,
                     ylim=ylim,
                     linewidth=[2, 1, 1],
                     color=["#939090", "navy", "navy"],
                     fontsize_legend=12,
                     fill_between=(1, 2), fill_alpha=0.1, fill_color="#666666",
                     show_fig=False, save_path=fig_path)

    def select_history_component_data(self, which, by_stage):
        """Return a history component for plot. LR is expressed in log scale.

        Args:
            which (str): component name
            by_stage (bool): reshape by stage

        Returns:
            numpy.array
        """

        arr = getattr(self, f"arr_{which}")
        if which == "lr":  # log scale
            arr = np.log10(arr)

        if by_stage:
            arr = arr.reshape((-1, self.n_stages, self.n_epochs_per_stage))

        return arr

    def generate_history_ylabel(self, which):
        """Return ylabel for a history component

        Args:
            which (str): history component

        Returns:
            str: ylabel
        """

        if which in ("loss", "val_loss"):
            return which

        elif which in ("metric", "val_metric"):

            if self.dc.acc_metric == "r2_metric":
                return "$R^2$"
            elif self.dc.acc_metric == "acc":
                return "accuracy"
            else:
                printl(f"Metric not understood: {self.dc.acc_metric}")
                return self.dc.acc_metric

        elif which == "lr":
            return "$log_{10}$(LR)"

        else:
            raise ValueError(f"History component not understood: {which}")

    def generate_history_title(self, which, ci=None, paren_text=None):
        """Return title for a history component includ. CI range.

        Args:
            which (str): history component
            ci (int|float|None): confidence interval
            paren_text (str|None): append parenthesis text

        Returns:
            str: title
        """

        # set main item
        if which == "loss":
            txt = "Loss"

        elif which == "val_loss":
            txt = "Val_loss"

        elif which == "metric":
            if self.dc.acc_metric == "r2_metric":
                txt = "$R^2$"
            elif self.dc.acc_metric == "acc":
                txt = "Accuracy"
            else:
                printl(f"Metric not understood: {self.dc.acc_metric}")
                txt = self.dc.acc_metric

        elif which == "val_metric":
            if self.dc.acc_metric == "r2_metric":
                txt = "Val_$R^2$"
            elif self.dc.acc_metric == "acc":
                txt = "Val_accuracy"
            else:
                printl(f"Metric not understood: {self.dc.acc_metric}")
                txt = self.dc.acc_metric

        elif which == "lr":
            txt = "Learning Rate"

        else:
            raise ValueError(f"History component not understood: {which}")

        # select ci
        if ci is None:
            txt2 = f"{txt} History"
        elif 0 < ci < 1:
            if int(ci*100) == ci*100:
                txt2 = f"{int(ci*100)}% C.I. of {txt} History"
            else:
                txt2 = f"{ci*100}% C.I. of {txt} History"
        else:
            raise ValueError(f"Bad ci value: {ci}")

        # paren
        if paren_text is None:
            return txt2
        else:
            return f"{txt2} ({paren_text})"

    def generate_history_ylim(self, which):
        """Generate appropriate ylim for the given history component.

        Args:
            which (str): history component

        Returns:
            tuple|None: ylim
        """
        if which in ("loss", "val_loss"):
            return (0, 2)

        elif which in ("metric", "val_metric"):
            if self.dc.acc_metric in ("r2_metric", "acc"):
                return (0, 1)
            else:
                printl(f"Metric not understood: {self.dc.acc_metric}, returning (0, 1)")
                return (0, 1)

        elif which == "lr":
            return None

        else:
            raise ValueError(f"History component not understood: {which}")

    def s63_plot_epoch_history_by_stage(self, ls_stages=None):
        """Analyze epoch history within each stage."""

        # guard single stage batches
        if self.dc.evo_method == "NONE":
            printl(f"A single-staged batch has no epoch-stage history!")
            return

        for which in self.ls_history_components:
            self.plot_epoch_history_by_stage_component(which, ls_stages)

    def plot_epoch_history_by_stage_component(self, which, ls_stages=None):
        """Plot a component of the epoch history.

        Args:
            which (str): history component
            ls_stages (tuple|list|array|None): list or stages to be plotted (1-based index). None = auto.
        """

        # set stages to be plotted
        if ls_stages is None:
            # default: every stage
            ls_stages = self.get_stage_xticks()

        n_lines = len(ls_stages)

        # get data
        idx_stages = [i - 1 for i in ls_stages]  # 1-based to 0-based
        arr = self.select_history_component_data(which, by_stage=True)[:, idx_stages, :]  # subset by stages

        # plot data
        x = np.array(range(1, self.n_epochs_per_stage + 1), dtype=int)

        # med, 95th pctl, best
        ls_y_med = np.median(arr, axis=0)  # -> (#stages, #epochs per stage)
        if which in ("loss", "val_loss", "lr"):
            ls_y_95 = np.quantile(arr, 0.05, axis=0)
            ls_y_best = arr.min(axis=0)
        elif which in ("metric", "val_metric"):
            ls_y_95 = np.quantile(arr, 0.95, axis=0)
            ls_y_best = arr.max(axis=0)
        else:
            raise ValueError(f"Bad component: {which}")

        # plot setup
        ls_labels = [f"Stg{i}" for i in ls_stages]
        xlabel = "epoch"
        ylabel = self.generate_history_ylabel(which)
        plot_format = "-"  # "-", "--", ":", "-."
        ylim = self.generate_history_ylim(which)
        # generate colors
        cmap = matplotlib.cm.get_cmap("viridis")
        ls_colors = cmap(np.linspace(0, 1, n_lines))

        # plot each component
        for i in range(3):
            # customize title
            if i == 0:
                ls_y = ls_y_med
                title = self.generate_history_title(which, ci=None, paren_text="Median")
                fig_path = self.fig_dir / f"hist_{which}_med_stage={tuple_to_str_compact(ls_stages)}.png"
            elif i == 1:
                ls_y = ls_y_95
                title = self.generate_history_title(which, ci=None, paren_text="Top 5%")
                fig_path = self.fig_dir / f"hist_{which}_q95_stage={tuple_to_str_compact(ls_stages)}.png"
            else:
                ls_y = ls_y_best
                title = self.generate_history_title(which, ci=None, paren_text="Best")
                fig_path = self.fig_dir / f"hist_{which}_best_stage={tuple_to_str_compact(ls_stages)}.png"

            plot_multi_y(x, ls_y, ls_labels, xlabel, ylabel, title,
                         plot_format=plot_format,
                         ylim=ylim,
                         linewidth=1,
                         color=ls_colors,
                         fontsize_legend=10,
                         show_fig=False, save_path=fig_path)

    def s65_plot_stage_history(self):
        """Analyze training history in unit of stage."""

        # guard single stage batches
        if self.dc.evo_method == "NONE":
            printl(f"A single-staged batch has no stage history!")
            return

        for which in self.ls_history_components:
            self.plot_stage_history_which(which)

    def plot_stage_history_which(self, which):
        """Plot history by stage (best epoch in each stage).

        Args:
            which (str): a history component

        """

        arr_raw = self.select_history_component_data(which, by_stage=True)
        # aggregate by stage (best of epochs)
        arr = arr_raw.max(axis=2)
        # aggregate by record
        y_05, y_med, y_95 = median_and_empirical_ci(arr, ci=0.9, axis=0)

        # plot data
        x = np.array(range(1, self.n_stages + 1), dtype=int)
        ls_y = [y_med, y_95, y_05]
        ls_labels = ["med", "q95", "q05"]
        xlabel = "stage"
        ylabel = self.generate_history_ylabel(which)
        xticks = self.get_stage_xticks()
        title = self.generate_history_title(which, ci=0.9)
        plot_format = ["-", ":", ":"]  # "-", "--", ":", "-."
        xlim = [0, self.n_stages]
        ylim = self.generate_history_ylim(which)
        fig_path = self.fig_dir / f"hist_{which}_BoS.png"

        plot_multi_y(x, ls_y, ls_labels, xlabel, ylabel, title,
                     plot_format=plot_format,
                     xlim=xlim, ylim=ylim,
                     xticks=xticks,
                     linewidth=[2, 1, 1],
                     color=["#939090", "navy", "navy"],
                     fontsize_legend=12,
                     fill_between=(1, 2), fill_alpha=0.1, fill_color="#666666",
                     show_fig=False, save_path=fig_path)

    def get_stage_xticks(self):
        """Get appropriate xticks for stage aggregation based on ``self.n_stages``.

        Returns:
            numpy.array
        """
        if self.n_stages <= 10:
            return np.arange(1, 1 + self.n_stages, 1, dtype=int)
        elif self.n_stages >= 11:
            ls1 = [1, 2, 5]
            ls2 = [i for i in range(10, self.n_stages, 5)]
            ls3 = [self.n_stages]  # include the last stage
            ls = ls1 + ls2 + ls3
            return np.array(ls, dtype=int)

    # def s71_weight_dist(self):
    #     pass

    # def s72_bias_dist(self):
    #     pass

    # def s91_exec_time(self):
    #     pass


def locate_batch_infiles(batch_no):
    """Locate parameter files at default location.

    Args:
        batch_no (str): timestamp_readable

    Returns:
        tuple:
            - **params_file** (*Path*)
            - **config_file** (*Path*)
            - **hptune_file** (*Path*)
    """

    config_file = base_dir / "data" / "out" / batch_no / "config" / "master.json"
    assert config_file.exists()

    params_file = base_dir / "data" / "out" / batch_no / "params" / "params.json"
    assert params_file.exists()

    hptune_file = base_dir / "data" / "out" / batch_no / "params" / "hptune.json"
    if not hptune_file.exists():
        hptune_file = None

    return params_file, config_file, hptune_file


def merge_batches(base_batch, add_batch):
    """Merge 2 batches after a crash.

    Args:
        base_batch (str)
        add_batch (str): the batch to be moved
    """
    # constants
    base_out_dir = base_dir / "data" / "out" / base_batch
    base_fig_dir = base_dir / "data" / "fig" / base_batch
    add_out_dir = base_dir / "data" / "out" / add_batch

    # config, params, hptune
    base_pf, base_cf, base_hf = locate_batch_infiles(base_batch)
    o_base = EvoContainer(base_pf, base_cf, base_hf)

    add_pf, add_cf, add_hf = locate_batch_infiles(add_batch)
    o_add = EvoContainer(add_pf, add_cf, add_hf)

    # retreive docs
    ls_docs_base = list(o_base.coll_obj.find({"timestamp_readable": base_batch}))
    ls_docs_add = list(o_add.coll_obj.find({"timestamp_readable": add_batch}))

    # base timestamp info
    base_timestamp = ls_docs_base[0]["timestamp"]
    base_timestamp_readable = ls_docs_base[0]["timestamp_readable"]
    for doc in ls_docs_base:  # check base batch consistency
        assert base_timestamp == doc["timestamp"]
        assert base_timestamp_readable == doc["timestamp_readable"]

    # 1. move infiles
    apf_dest = base_out_dir / "params" / f"params(merged={base_timestamp_readable}).json"
    shutil.move(add_pf, apf_dest)

    acf_dest = base_out_dir / "config" / f"config(merged={base_timestamp_readable}).json"
    shutil.move(add_cf, acf_dest)

    if add_hf is not None:
        ahf_dest = base_out_dir / "params" / f"hptune(merged={base_timestamp_readable}).json"
        shutil.move(add_hf, ahf_dest)

    # 2. move data
    data_src = add_out_dir / f"data.h5"
    data_dest = base_out_dir / f"data(merged={base_timestamp_readable}).h5"
    shutil.move(data_src, data_dest)

    # 3. move model, history and plots
    for doc in ls_docs_add:

        # best model
        model_src = base_dir / doc["best_model_path"]
        model_dest = base_out_dir / model_src.name
        shutil.move(model_src, model_dest)

        # best model plot
        model_plot_src = base_dir / doc["best_model_plot_path"]
        model_plot_dest = base_fig_dir / model_plot_src.name
        shutil.move(model_plot_src, model_plot_dest)

        # history
        hist_src = base_dir / doc["history_path"]
        hist_dest = base_out_dir / hist_src.name
        shutil.move(hist_src, hist_dest)

        # history plots
        hist_plot_src = base_dir / doc["history_plot_path"]
        hist_plot_dest = base_fig_dir / hist_plot_src.name
        shutil.move(hist_plot_src, hist_plot_dest)

        # 4. update mongodb record
        o_add.coll_obj.update_one(
            {"_id": doc["_id"]},
            {"$set":
                {
                    "best_model_path": str(model_dest.relative_to(base_dir)),
                    "best_model_plot_path": str(model_plot_dest.relative_to(base_dir)),
                    "history_path": str(hist_dest.relative_to(base_dir)),
                    "history_plot_path": str(hist_plot_dest.relative_to(base_dir)),
                    "timestamp": base_timestamp,
                    "timestamp_readable": base_timestamp_readable
                }
            }
        )
