"""
Multistage callback functions for the proposed evolutionary algorithms.

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

"""

import os
import sys
from pathlib import Path
import numpy as np
from abc import abstractmethod

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())

if str(base_dir) not in sys.path:  # add base_dir to path if not added yet
    sys.path.append(str(base_dir))

from lib.eng_util import print_loglike as printl
from lib.math_util import random_mask, random_normal_with_threshold, mask_subset_k, idx_to_mask
from lib.keras_multistage import MultiStageCallback
from lib.keras_model import get_dense_layer_names, get_layer_n_links, get_layer_shape
from .evo_callback import LinkMaskingCallback


class EvoMsCallback(MultiStageCallback):
    """Abstract base class of evolutionary multistage callbacks.
    The user has to implement ``self.get_ls_mask_del()``.
    """

    def __init__(self, epsilon, zeta, min_evo_scale, std_evo_scale,
                 evo_output_layer=True, mask_callback=None, **kwargs):

        super(EvoMsCallback, self).__init__(**kwargs)

        #: float: density coefficient in standard SET
        self.epsilon = epsilon

        #: float: evolution ratio in standard SET
        self.zeta = zeta
        assert 0 < self.zeta <= 1

        #: float: minimum jump size for each evolution
        self.min_evo_scale = min_evo_scale
        assert self.min_evo_scale >= 0

        #: float: stdev of random jump size for each evolution
        self.std_evo_scale = std_evo_scale
        assert self.std_evo_scale >= 0

        #: bool: evolve the output layer or not
        self.evo_output_layer = evo_output_layer

        #: LinkMaskingCallback: perform link-masking on epoch/batch end (optional).
        #: This callback must exist and be passed into the model callback if ``self.epsilon == 1``.
        self.mask_callback = mask_callback
        if self.epsilon == 1:
            assert self.mask_callback is None
        else:
            assert isinstance(self.mask_callback, LinkMaskingCallback)

        # =========================
        # internal variables
        # =========================

        #: list[str]: names of dense layers. Didn't list layer instances
        #: because the model may be rebuilt during training!
        self.ls_dense_layer_names = []

        #: int: ``= len(self.ls_dense_layer_names)``
        self.n_dense_layers = 0

        #: list[int]: number of links in each dense layer
        self.ls_n_links = []

        #: list[tuple[int]]: (input_shape, output_shape) for each dense layer
        self.ls_shapes = []

        #: list[int]: number of opened links in each dense layer
        self.ls_n_active_links = []

        #: list[array]: mask object (boolean arrays)
        self.ls_masks = []  # 1=unmasked (active), 0=masked (inactive)

    def on_multistage_begin(self):
        self.set_network_size_and_density()

    def set_network_size_and_density(self):
        """Set internal variables related to network size and edge density."""

        # 1. get dense layers and counts
        self.ls_dense_layer_names = get_dense_layer_names(self.model, dense_i=True)
        self.n_dense_layers = len(self.ls_dense_layer_names)
        self.ls_n_links = [get_layer_n_links(self.model, f"dense_{i + 1}") for i in range(self.n_dense_layers)]
        self.ls_shapes = [get_layer_shape(self.model, f"dense_{i+1}") for i in range(self.n_dense_layers)]
        assert isinstance(self.ls_shapes[0], tuple)

        if self.epsilon == 1:
            self.ls_n_active_links = self.ls_n_links
        else:
            self.ls_n_active_links = [int(np.ceil(self.epsilon * n)) for n in self.ls_n_links]

        for i in range(self.n_dense_layers):

            # case full density: append an all-true mask and don't apply
            if self.epsilon == 1:
                self.ls_masks.append(np.ones(self.ls_shapes[i], dtype=bool))

            # non-full:
            else:
                # 2. initialize masks for each layer
                arr_shape = self.ls_shapes[i]
                k = self.ls_n_active_links[i]
                mask = random_mask(arr_shape, k)
                self.ls_masks.append(mask)

                # 3. apply masks for each layer
                layer = self.model.get_layer(f"dense_{i+1}")
                weights = layer.get_weights()
                weights[0][~mask] = 0.0  # kill elements having a "False" mask value
                layer.set_weights(weights)

    def on_stage_begin(self):

        # evolve links or neurons
        self.evolve()

        # reset mask for the partner masking callback
        if (self.epsilon < 1) and (self.mask_callback is not None):
            self.mask_callback.reset_masks(self.ls_masks)

    def evolve(self):
        """Perform random evolution with probability ``self.zeta``."""

        # choose to evolve output layer or not
        if self.evo_output_layer:
            n_evo_layers = self.n_dense_layers
        else:
            n_evo_layers = self.n_dense_layers - 1

        # 1. pick links/neurons to be deleted from the active neurons
        # NOTE: for neuron-based evolution, get_mask_del() depends on multiple layers
        #       hence cannot do get_mask_del(i)
        ls_mask_del = self.get_ls_mask_del()  # customizable
        ls_n_deleted = [np.count_nonzero(mask_del) for mask_del in ls_mask_del]
        ls_mask_keep = [self.ls_masks[i] ^ ls_mask_del[i] for i in range(n_evo_layers)]

        for i in range(n_evo_layers):

            layer = self.model.get_layer(f"dense_{i+1}")
            weights = layer.get_weights()
            mask = self.ls_masks[i]
            mask_del = ls_mask_del[i]
            mask_keep = ls_mask_keep[i]
            n_deleted = ls_n_deleted[i]

            # 2. pick n_deleted links/neurons to be evolved
            if self.epsilon == 1:
                mask_evo = mask_del
            else:
                # customizable
                mask_evo = self.get_mask_evo(mask_keep, n_deleted)

            # 3. calculate jump/evolution size
            size_evo = random_normal_with_threshold(n_deleted,
                                                    self.min_evo_scale * self.std_evo_scale,
                                                    self.std_evo_scale)

            # 4. apply deletion
            weights[0][mask_del] = 0.0

            # 5. apply evolution
            weights[0][mask_evo] += size_evo  # assign by broadcasting

            # 6. update ls_masks
            if self.epsilon < 1:
                self.ls_masks[i] = (mask ^ mask_del) | mask_evo  # a - b + c

            # apply new weights
            layer.set_weights(weights)

    @abstractmethod
    def get_ls_mask_del(self):
        """Pick the links to be removed for each layer.

        Returns:
            list[numpy.array]: masks of elements to be deleted for each layer
        """
        pass

    def get_mask_evo(self, mask_keep, k):
        r"""Pick k links to be evolved (i.e. added) from a mask.
        Need not consider the special case ``epsilon == 1`` independently.

        Args:
            mask_keep (numpy.array): a 0-1 mask (ls_masks), 1=active
            k (int): how many neurons were to be evolved

        Returns:
            numpy.array: the ls_masks of elements to be evolved
        """
        return self.get_mask_evo_default(mask_keep, k)

    @staticmethod
    def get_mask_evo_default(mask_keep, k):
        """Pick neurons to be evolved randomly (default method).
        ``k < n_avail`` is assumed.
        """

        # indices of available links
        idx_avail = np.where(~mask_keep)  # most likely a tuple of arrays (arr1, arr2)
        n_avail = len(idx_avail[0])
        #print(f"k={k}, n_avail={n_avail}")

        # get the indices to be picked
        idx_idx_picked = np.random.permutation(n_avail)[:k]  # get random k from n_avail
        idx_picked = [axis[idx_idx_picked] for axis in idx_avail]  # get the above k elements from each axis

        # apply
        return idx_to_mask(idx_picked, mask_keep.shape)

    def get_masks_from_neurons(self, ls_idx_neurons):
        """Convert neuron indices to a list of boolean mask. This is a common
        method for neuron evolution algorithms.

        Args:
            ls_idx_neurons (list[numpy.array]): list of index of picked neurons

        Returns:
            list[numpy.array]: list of link masks
        """

        ls_del_masks = []
        for i in range(self.n_dense_layers):

            # (1) 1st layer: mask out output neurons only
            if i == 0:
                mask = np.zeros(self.ls_shapes[0], dtype=bool)
                idx_outs = ls_idx_neurons[0]
                mask[:, idx_outs] = True
                ls_del_masks.append(mask)

            # (2) the last layer: mask out input neurons only, and only when evo_last_layer is True
            elif i == self.n_dense_layers - 1:
                if self.evo_output_layer:
                    mask = np.zeros(self.ls_shapes[self.n_dense_layers - 1], dtype=bool)
                    idx_ins = ls_idx_neurons[self.n_dense_layers - 2]
                    mask[idx_ins, :] = True
                    ls_del_masks.append(mask)
                else:
                    pass

            # (3) layers in between:
            else:
                mask = np.zeros(self.ls_shapes[i], dtype=bool)
                idx_ins = ls_idx_neurons[i - 1]
                idx_outs = ls_idx_neurons[i]
                mask[idx_ins, :] = True
                mask[:, idx_outs] = True
                ls_del_masks.append(mask)

        return ls_del_masks


class RleMsCallback(EvoMsCallback):
    """Random link evolution (RLE) multistage callback."""

    def __init__(self, epsilon, zeta, min_evo_scale, std_evo_scale,
                 evo_output_layer=True, mask_callback=None, **kwargs):

        # nothing new
        super(RleMsCallback, self).__init__(
            epsilon, zeta, min_evo_scale, std_evo_scale,
            evo_output_layer=evo_output_layer, mask_callback=mask_callback, **kwargs
        )

    def get_ls_mask_del(self):

        # whether to exclude the last layer
        n = self.n_dense_layers if self.evo_output_layer else self.n_dense_layers - 1

        ls_mask_del = []
        for i in range(n):

            mask = self.ls_masks[i]  # of course mask cannot be all 0

            # generate an array of the same shape of ls_masks with values between [0, 1)
            arr = np.random.random(mask.shape)

            # delete with probability zeta
            mask_del = (arr < self.zeta)

            if self.epsilon == 1:
                ans = mask_del
            else:
                ans = mask & mask_del

            # make sure at least one neuron is evolved
            if np.count_nonzero(ans) > 0:
                ls_mask_del.append(ans)
            else:
                # pick an arbitrary one from mask
                printl(f"RleMsCallback::get_mask_del(): Pick 1 random link to evolve (zeta={self.zeta} picked 0)...")
                arr_idx = np.flatnonzero(mask)
                idx = np.random.choice(arr_idx)  # a scalar
                ans = idx_to_mask([idx], mask.shape)  # pass an iterable in
                ls_mask_del.append(ans)

        return ls_mask_del

    def get_mask_evo(self, mask_keep, k):  # not necessarily static
        # pick links randomly
        return self.get_mask_evo_default(mask_keep, k)


class LeMsCallback(EvoMsCallback):
    """Link evolution (LE) multistage callback."""

    def __init__(self, epsilon, zeta, min_evo_scale, std_evo_scale,
                 evo_output_layer=True, mask_callback=None, **kwargs):

        # nothing new
        super(LeMsCallback, self).__init__(
            epsilon, zeta, min_evo_scale, std_evo_scale,
            evo_output_layer=evo_output_layer, mask_callback=mask_callback, **kwargs
        )

    def get_ls_mask_del(self):

        ls_mask_del = []
        n = self.n_dense_layers if self.evo_output_layer else self.n_dense_layers - 1
        for i in range(n):

            layer = self.model.get_layer(f"dense_{i+1}")
            wt = layer.get_weights()

            # 1. pick the smallest positive elements
            mask_pos = (wt[0] >= 0)
            n_pos = np.count_nonzero(mask_pos)
            if n_pos > 0:
                k_pos = int(np.ceil(self.zeta * n_pos))  # >= 1
                mask_pick_pos = mask_subset_k(wt[0], mask_pos, k_pos, mode="min")
            else:
                mask_pick_pos = None

            # 2. pick the greatest negative elements
            mask_neg = (wt[0] < 0)
            n_neg = np.count_nonzero(mask_neg)
            if n_neg > 0:
                k_neg = int(np.ceil(self.zeta * n_neg))
                mask_pick_neg = mask_subset_k(wt[0], mask_neg, k_neg, mode="max")
            else:
                mask_pick_neg = None

            # 3. merge masks
            if mask_pick_pos is None:
                ls_mask_del.append(mask_pick_neg)
            elif mask_pick_neg is None:
                ls_mask_del.append(mask_pick_pos)
            else:
                ls_mask_del.append(mask_pick_pos | mask_pick_neg)

        return ls_mask_del


class NeMsCallback(EvoMsCallback):
    """Neuron evolution (NE) multistage callback. Evolution metrics
    are implemented here.
    """

    def __init__(self, epsilon, zeta, min_evo_scale, std_evo_scale,
                 neuron_evo_metric="p-sum", p=2,
                 evo_output_layer=True, mask_callback=None, **kwargs):

        super(NeMsCallback, self).__init__(
            epsilon, zeta, min_evo_scale, std_evo_scale,
            evo_output_layer=evo_output_layer, mask_callback=mask_callback, **kwargs
        )

        #: str: metric of neuron evolution inspired by transfer learning (p-sum, p-max,...)
        self.neuron_evo_metric = neuron_evo_metric

        #: str: metric of neuron evolution inspired by transfer learning (p-sum, p-max,...)
        self.p = p

    def p_sum(self, i):
        """The p-sum metric for dense layer i.

        Args:
            i (int): a 0-based index of the current layer

        Returns:
            numpy.array: metric value of each neuron
        """

        if i == self.n_dense_layers - 1:
            raise ValueError("P-sum is not applicable to the output layer!")

        wb_in = self.model.get_layer(f"dense_{i+1}").get_weights()
        wb_out = self.model.get_layer(f"dense_{i+2}").get_weights()

        p_sum_in = abs(wb_in[0] ** self.p).sum(axis=0)  # sum over element-wise abs
        p_sum_out = abs(wb_out[0] ** self.p).sum(axis=1)
        return np.sqrt(p_sum_in * p_sum_out)  # Take the geometric mean to offset arbitrary scaling

    def abs_max(self, i):
        """The abs-max metric for dense layer i.

        Note:
            The value of p doesn't matter!

        Args:
            i (int): a 0-based index of the current layer

        Returns:
            numpy.array: metric value of each neuron
        """
        if i == self.n_dense_layers - 1:
            raise ValueError("abs_max is not applicable to the output layer!")

        wb_in = self.model.get_layer(f"dense_{i+1}").get_weights()
        wb_out = self.model.get_layer(f"dense_{i+2}").get_weights()

        p_max_in = abs(wb_in[0]).max(axis=0)  # sum over element-wise abs
        p_max_out = abs(wb_out[0]).max(axis=0)
        return np.sqrt(p_max_in * p_max_out)  # Take the geometric mean to offset arbitrary scaling

    def get_ls_mask_del(self):

        ls_idx_picked_neurons = []
        for i in range(self.n_dense_layers - 1):

            # 1. compute the evolution metric
            if self.neuron_evo_metric == "p-sum":
                evo_metric = self.p_sum(i)
            elif self.neuron_evo_metric == "abs-max":
                evo_metric = self.abs_max(i)
            else:
                raise ValueError(f"neuron_evo_metric not understood: {self.neuron_evo_metric}")

            # 2. pick the smallest zeta fraction of neurons
            n_neurons = self.ls_shapes[i][1]  # 0=in, 1=out
            n_picked = int(np.ceil(self.zeta * n_neurons))
            ls_idx_picked_neurons.append(np.argsort(evo_metric)[:n_picked])

        # 3. derive the links to be deleted
        return self.get_masks_from_neurons(ls_idx_picked_neurons)


class RneMsCallback(EvoMsCallback):
    """Random neuron evolution (RNE) multistage callback."""

    def __init__(self, epsilon, zeta, min_evo_scale, std_evo_scale,
                 evo_output_layer=True, mask_callback=None, **kwargs):

        # nothing new. Need no metrics.
        super(RneMsCallback, self).__init__(
            epsilon, zeta, min_evo_scale, std_evo_scale,
            evo_output_layer=evo_output_layer, mask_callback=mask_callback, **kwargs
        )

    def get_ls_mask_del(self):
        # NOTE: assume every neuron is used or usable.

        # 1. pick a fraction zeta of neurons randomly
        ls_idx_picked_neurons = []
        for i in range(self.n_dense_layers - 1):

            n_neurons = self.ls_shapes[i][1]  # 0=in, 1=out

            # generate an array of the same shape of ls_masks with values between [0, 1)
            arr = np.random.random(n_neurons)

            # delete with probability zeta
            idx_picked_neurons = np.argwhere(arr < self.zeta).reshape(-1)

            # A. append (regular case)
            if len(idx_picked_neurons) > 0:
                ls_idx_picked_neurons.append(idx_picked_neurons)
            # B. if no neurons were picked, assign a random neuron
            else:
                # pick an arbitrary one from mask
                printl(f"RneMsCallback::get_ls_mask_del(): Pick 1 random neuron to evolve (zeta={self.zeta} picked 0)...")
                idx = np.random.choice(n_neurons)  # a scalar
                ls_idx_picked_neurons.append(np.array([idx], dtype=int))

        # 3. derive the links to be deleted
        return self.get_masks_from_neurons(ls_idx_picked_neurons)
