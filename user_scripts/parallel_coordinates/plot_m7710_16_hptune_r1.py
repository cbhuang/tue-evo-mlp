"""
Parallel coordinate plot
"""

import os
from pathlib import Path
import sys
from datetime import datetime
import shutil
import numpy as np

# vis
import plotly.graph_objects as go
import plotly.io as pio
pio.renderers.default = "browser"

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())
# append only if not exist
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

# local libs
from lib.eng_util import print_loglike as printl, seconds_to_hms as hms, timestamp_to_suffix
from src.evo_container import EvoContainer, EvoAggr

t0_ts = datetime.now().timestamp()

# ==================================
# Utility Functions
# ==================================


def tkval(sr):
    """Convert Series to sorted unique array

    Args:
        sr (paddas.Series): input

    Returns:
        numpy.array
    """
    return np.sort(sr.unique())


def tk_interval(sr, interval):
    """Produce tick interval (inner side)

    Args:
        sr (paddas.Series): input data
        interval (float): interval

    Returns:
        np.array
    """
    nl = int(np.ceil(sr.min() / interval))
    nu = int(np.floor(sr.max() / interval))
    n = nu - nl + 1
    l = interval * nl
    u = interval * nu
    return np.linspace(l, u, n)


# ====================================================
# params
# ====================================================

# list of batches to be analyzed
ls_batch_no = ["20200701_221111"]

# output path
model_name_size = "m7710-16"
round = 1
fname = f"{model_name_size}-hptune-r{round}.png"
save_path = base_dir / "data" / "fig" / f"{ls_batch_no[0]}_aggr" / fname
save_path_2 = base_dir / "data" / "presentation" / "plot" / model_name_size / fname

# figure params
fig_width = 1250
fig_height = 1000
fontsize = 28

# ==== (rarely touched below) =====

# error logfile
err_log_file = base_dir / "data" / "logs" / f"run_stats_errlog_{timestamp_to_suffix(t0_ts)}.txt"

config_file = base_dir / "data" / "out" / ls_batch_no[0] / "config" / "master.json"
params_file = base_dir / "data" / "out" / ls_batch_no[0] / "params" / "params.json"
hptune_file = base_dir / "data" / "out" / ls_batch_no[0] / "params" / "hptune.json"
if not hptune_file.exists():
    hptune_file = None

# default suffix: suffix_aggr = f"{ls_batch_no[0]}_aggr"
suffix_aggr = f"{ls_batch_no[0]}_aggr"

# get_all_docs
match = {"timestamp_readable": {"$in": ls_batch_no}}  # must be given
# match = {"timestamp_readable": {"$in": ls_batch_no},
#          "std_evo_scale": {"$ne": 0.7}}
projection = None

# aggr_with_subset
subset = None
subset_suffix = None
# subset = {"batch_normalization": True}
# subset_suffix = "bn=true"

# attempt loading saved data for yp_plot
# load_yp_plot = False
load_yp_plot = True

# ====================================================
# Data Retrieval
# ====================================================

# restore dc first
dc = EvoContainer(params_file, config_file, hptune_file)
o = EvoAggr(dc, suffix_aggr=suffix_aggr)

# preprocess
o.s32_get_all_docs(match, projection=projection)
o.s35_to_pandas()
o.s36_add_group_tags()

#====================
# Plot
#====================

# column cleanup
ls_hparams = [key for key in o.dc.hparams]
ls_cols = ls_hparams + ["test_acc"]
df = o.df[ls_cols].copy()
# cast bn
df.loc[:, "batch_normalization"] = df["batch_normalization"].values.astype(int)
# cast init_lr (logscale)
lr_txt = [str(el) for el in tkval(df["init_lr"])]
df.loc[:, "init_lr"] = np.log10(df["init_lr"].values).astype(np.float64)
# cast batch_size (logscale)
batch_size_txt = [str(el) for el in tkval(df["batch_size"])]
df.loc[:, "batch_size"] = np.log10(df["batch_size"].values).astype(np.float64)

# required for feeding into plotly
df = df.astype(np.float64)

# named colorscales:
#   ['aggrnyl', 'agsunset', 'algae', 'amp', 'armyrose', 'balance',
#    'blackbody', 'bluered', 'blues', 'blugrn', 'bluyl', 'brbg',
#    'brwnyl', 'bugn', 'bupu', 'burg', 'burgyl', 'cividis', 'curl',
#    'darkmint', 'deep', 'delta', 'dense', 'earth', 'edge', 'electric',
#    'emrld', 'fall', 'geyser', 'gnbu', 'gray', 'greens', 'greys',
#    'haline', 'hot', 'hsv', 'ice', 'icefire', 'inferno', 'jet',
#    'magenta', 'magma', 'matter', 'mint', 'mrybm', 'mygbm', 'oranges',
#    'orrd', 'oryel', 'peach', 'phase', 'picnic', 'pinkyl', 'piyg',
#    'plasma', 'plotly3', 'portland', 'prgn', 'pubu', 'pubugn', 'puor',
#    'purd', 'purp', 'purples', 'purpor', 'rainbow', 'rdbu', 'rdgy',
#    'rdpu', 'rdylbu', 'rdylgn', 'redor', 'reds', 'solar', 'spectral',
#    'speed', 'sunset', 'sunsetdark', 'teal', 'tealgrn', 'tealrose',
#    'tempo', 'temps', 'thermal', 'tropic', 'turbid', 'twilight',
#    'viridis', 'ylgn', 'ylgnbu', 'ylorbr', 'ylorrd'].

fig = go.Figure(
    data=go.Parcoords(
        line=dict(
            color=df['test_acc'],
            colorscale='portland',  # portland tealrose
            showscale=True,
            colorbar=dict(tickfont=dict(size=fontsize))  # cmin, cmax?
        ),
        dimensions=[
            dict(label="BN", values=df['batch_normalization'],
                 tickvals=tkval(df['batch_normalization']),
                 ticktext=["False", "True"]),
            dict(label="dropout_rate", values=df['dropout_rate'], tickvals=tkval(df['dropout_rate'])),
            dict(label="init_lr", values=df['init_lr'], tickvals=tkval(df['init_lr']),
                 ticktext=lr_txt),
            dict(label="batch_size", values=df['batch_size'],
                 tickvals=tkval(df['batch_size']), ticktext=batch_size_txt),
            dict(label="test_acc", values=df['test_acc'],
                 tickvals=tk_interval(df['test_acc'], 0.1),
                 tickformat=".1f"),
        ],
        labelfont=dict(size=fontsize),
        tickfont=dict(size=fontsize),
        rangefont=dict(size=fontsize)
    ),
    layout=go.Layout(
        autosize=False,
        width=fig_width,
        height=fig_height
    )
)
#fig.show()
fig.write_image(str(save_path))

try:
    shutil.copy(save_path, save_path_2)
except OSError as e:
    print(str(e))

printl(f"Overall time: {hms(datetime.now().timestamp() - t0_ts, colons=False)}", "INFO")
