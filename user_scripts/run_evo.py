"""
Run src/plain_container
"""
import os
from pathlib import Path
import sys
from datetime import datetime

# add project base directory to path
if '__file__' in globals():
    print(f"{datetime.now().strftime('%T')} [DEBUG] This script is run as a file.")
    base_dir = Path(__file__).absolute().parent.parent
else:
    print(f"{datetime.now().strftime('%T')} [DEBUG] This script is run interactively.")
    base_dir = Path(os.getcwd())
print(f"{datetime.now().strftime('%T')} [DEBUG] project base: {base_dir}")
# append only if not exist
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

# local libs
from src.evo_container import EvoContainer
from lib.eng_util import print_loglike as printl
from lib.eng_util import seconds_to_hms as hms

# =====================================================================
# Parameters
# =====================================================================

ls_inputs = [  # [params_file, hptune_file]
    [base_dir / "data" / "raw" / "144_params.json", None],
    #[base_dir / "data" / "raw" / "212_params.json", base_dir / "data" / "raw" / "212_hptune.json"],
]

config_file = base_dir / "config" / "master.json"

# record time spent
log_dir = base_dir / "data" / "logs"

# =====================================================================
# Workflow (Fully Automated)
# =====================================================================

# guard typo
for ls in ls_inputs:
    for fpath in ls:
        if fpath is not None:
            assert fpath.exists()

if log_dir is not None:
    assert isinstance(log_dir, Path)
    assert log_dir.exists()

# execute
for ls in ls_inputs:

    params_file = ls[0]
    hptune_file = ls[1]

    o = EvoContainer(params_file, config_file, hptune_file=hptune_file)
    o.s01_load_raw_data()
    # o.s03_impute_missing()
    o.s05_transform()
    o.s07_init_output_dirs()
    o.s08_save_train_test_data()
    o.s09_save_config_and_params()

    if o.tune_hp:
        o.s10_loop_hparams()
        # Will loop through s11 for all hparams
    else:
        o.s11_loop_n_repeat()
        # _s12 to _s28 are called automatically.

    printl(f"Overall time: {hms(datetime.now().timestamp() - o.timestamp, colons=False)}", "INFO")

    # dump
    if log_dir is not None:
        with open(str(log_dir / f"{o.suffix_ts}.txt"), "w") as f:
            f.writelines([f"Params File: {params_file}\n",
                          f"Timestamp: {o.suffix_ts}\n",
                          f"Time elapsed: {hms(datetime.now().timestamp() - o.timestamp, colons=False)}\n"])
