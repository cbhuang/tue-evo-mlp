"""
Execute EvoAggr to analyze results obtained by EvoContainer.
Can run multiple batches at once.
"""

import os
from pathlib import Path
import sys
from datetime import datetime

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())
# append only if not exist
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

# local libs
from lib.eng_util import print_loglike as printl, seconds_to_hms as hms, timestamp_to_suffix
from src.evo_container import EvoContainer, EvoAggr

t0_ts = datetime.now().timestamp()

# ====================================================
# params
# ====================================================

# list of batches to be analyzed
ls_ls_batch_no = [
    ["20200717_171216"], # Expt. No. 144
]

# error logfile
err_log_file = base_dir / "data" / "logs" / f"run_stats_errlog_{timestamp_to_suffix(t0_ts)}.txt"

for ls_batch_no in ls_ls_batch_no:

    config_file = base_dir / "data" / "out" / ls_batch_no[0] / "config" / "master.json"
    params_file = base_dir / "data" / "out" / ls_batch_no[0] / "params" / "params.json"
    hptune_file = base_dir / "data" / "out" / ls_batch_no[0] / "params" / "hptune.json"
    if not hptune_file.exists():
        hptune_file = None

    # default suffix: suffix_aggr = f"{ls_batch_no[0]}_aggr"
    suffix_aggr = f"{ls_batch_no[0]}_aggr"

    # get_all_docs
    match = {"timestamp_readable": {"$in": ls_batch_no}}  # must be given
    # match = {"timestamp_readable": {"$in": ls_batch_no},
    #          "std_evo_scale": {"$ne": 0.7}}
    projection = None

    # aggr_with_subset
    subset = None
    subset_suffix = None
    # subset = {"batch_normalization": True}
    # subset_suffix = "bn=true"

    # attempt loading saved data for yp_plot
    # load_yp_plot = False
    load_yp_plot = True

    # ====================================================
    # Automated Workflow
    # ====================================================

    try:
        # restore dc first
        dc = EvoContainer(params_file, config_file, hptune_file)
        o = EvoAggr(dc, suffix_aggr=suffix_aggr)

        # preprocess
        o.s32_get_all_docs(match, projection=projection)
        o.s35_to_pandas()
        o.s36_add_group_tags()

        # analyze accuracy
        o.s40_acc_aggr_with_subset(subset=subset, subset_suffix=subset_suffix)

        # analyze prediction
        o.s51_recover_data_and_transformers()
        o.s52_set_x_yt_plot()
        o.s53_recover_yp_plot(load_yp_plot=load_yp_plot)
        o.s54_plot_true_vs_pred_by_hp()

        # plot history (not for hyperparameter tuning)
        if hptune_file is None:
            #o.s60_plot_history(self, subset=subset, load_h5=True)
            o.s61_collect_history(subset=subset)
            o.s62_plot_epoch_history()
            o.s63_plot_epoch_history_by_stage()
            o.s65_plot_stage_history()

    except Exception as e:
        printl(f"{ls_batch_no[0]} aggregation failed! Err msg:", "WARNING")
        print(str(e))
        with open(err_log_file, "a") as f:
            f.write(f"Failed: {ls_batch_no[0]}: {str(e)}\n")

    finally:
        printl(f"{ls_batch_no}: {hms(datetime.now().timestamp() - o.timestamp, colons=False)}", "INFO")

printl(f"Overall time: {hms(datetime.now().timestamp() - t0_ts, colons=False)}", "INFO")
