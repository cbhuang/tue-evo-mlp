db.getCollection('evoCollection').find({
    "data_source": "zigzag-1d",
    "evo_method": "NONE",
    "ls_max_neurons": [32],
    "epochs": 100,
    "init_lr": 0.01,
    "early_stopping": false,
    "reduce_lr_on_plateau": true
}).count()
