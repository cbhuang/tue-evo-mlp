db.getCollection("evoCollection").aggregate([
{
    $match: {
        "timestamp_readable": "20200602_144232",
        "test_acc": {$gt: 0.0}
    }
},
{
    $group: {
        _id: "$init_weight_scale",
        "n": {$sum: 1},
        "avg_test_acc": {$avg: "$test_acc"},
        "max_test_acc": {$max: "$test_acc"},
        "min_test_acc": {$min: "$test_acc"}
    }
},
{
    $sort: {_id: 1}   
}
])