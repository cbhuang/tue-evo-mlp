db.evoCollection.aggregate([
{ 
    $group: {
        _id: "$ls_max_neurons",
        "sum_best_epoch": {$sum: "$best_epoch"},
        "sum_exec_time": {$sum: "$exec_time"}
    }
},
{
    $project: {
        "sum_best_epoch": "$sum_best_epoch",
        "sum_exec_time": "$sum_exec_time",
        "avg_time_per_epoch": {$divide: ["$sum_exec_time", "$sum_best_epoch"]} 
    }
}
])
