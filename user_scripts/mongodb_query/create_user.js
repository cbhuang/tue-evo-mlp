// create user "evo" for db "evoDB"
db.createUser(
    {
        user: "evo",
        pwd: "evo",
        roles: [{
            role: "readWrite",
            db: "evoDB"
        }]
    },
    {
        w: "majority",
        wtimeout: 5000
    }
)
