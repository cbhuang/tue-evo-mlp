"""
Merge two batches after machine failure.
"""

import os
from pathlib import Path
import sys
from datetime import datetime

# add project base directory to path
if '__file__' in globals():
    base_dir = Path(__file__).absolute().parent.parent
else:
    base_dir = Path(os.getcwd())
# append only if not exist
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

# local libs
from src.evo_container import move_partial_batch
from lib.eng_util import print_loglike as printl

# =====================================================================
# Parameters
# =====================================================================

base_batch = "20200715_112613"
add_batch = "20200715_184507"

# =====================================================================
# Workflow (Fully Automated)
# =====================================================================
t0 = datetime.now()

move_partial_batch(base_batch, add_batch)

printl(f"{add_batch} -> {base_batch} in {(datetime.now() - t0).total_seconds():.1f}s")
