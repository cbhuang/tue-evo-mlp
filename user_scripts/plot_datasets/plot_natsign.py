"""
Plot NatSign
"""
import os
from pathlib import Path
import sys
import numpy as np
import matplotlib
from matplotlib import pyplot as plt

from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())
# append only if not exist
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

# local libs
from lib.eng_util import print_loglike as printl, seconds_to_hms as hms
from src.data_synthesis import data_natsign
from lib.vis_template import plot_scatter_by_label

# ====================================================
# Parameters
# ====================================================
# right corner
x0, y0 = 1.0, 0.3
# top right
x1 = 1.3
# slope
r = 1
# gap between classes
gap = 0.05
# total number of points (must be even)
n = 2000
# random sampling
rnd = False
# print
verbose = True

# ====================================================
# Automated Workflow
# ====================================================

# 1. unstandardized data
xy, z = data_natsign(n=n, x0=x0, y0=y0, r=r, gap=gap, rnd=rnd, verbose=verbose)
assert xy.shape == (n, 2)
# get colormap
color = ["navy" if zi == 1 else "green" for zi in z]
u, c = np.unique(color, return_counts=True)
print(f"unique:{u}, counts:{c}")
assert np.all(c == [n/2, n/2])

plot_scatter_by_label(xy[:, 0], xy[:, 1], "$x_1$", "$x_2$", "NatSign", marker='.', color=color, show_fig=True, save_path="/mnt/ramdisk/natsign.png")
