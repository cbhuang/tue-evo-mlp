"""
Plot NatSign
"""
import os
from pathlib import Path
import sys
import numpy as np
import matplotlib
from matplotlib import pyplot as plt

from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())
# append only if not exist
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

# local libs
from lib.eng_util import print_loglike as printl, seconds_to_hms as hms
from src.data_synthesis import data_zigzag_1d
from lib.vis_template import plot_multi_y

# ====================================================
# Parameters
# ====================================================


# ====================================================
# Automated Workflow
# ====================================================

# 1. unstandardized data
x, y = data_zigzag_1d(2000)
idx = np.argsort(x.reshape(-1))
x = x.reshape(-1)[idx]
y = y.reshape(-1)[idx]

plot_multi_y(x, [y], [""], "x", "y", "Zigzag", "-", color="black", legend=False)
