"""
Plot Peak-1D
"""
import os
from pathlib import Path
import sys
import numpy as np
import matplotlib
from matplotlib import pyplot as plt

from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())
# append only if not exist
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

# local libs
from lib.eng_util import print_loglike as printl, seconds_to_hms as hms
from src.data_synthesis import f_peak_1d
from lib.vis_template import plot_scatter_by_label
import matplotlib.ticker as tk

# ====================================================
# Parameters
# ====================================================
# right corner
x0, y0 = 1.0, 0.3
# top right
x1 = 1.3
# slope
r = 1
# gap between classes
gap = 0.05
# total number of points (must be even)
n = 2000
# random sampling
rnd = False
# print
verbose = True

# ====================================================
# Automated Workflow
# ====================================================

# 1. unstandardized data
x = np.linspace(-2, 2, 2000)
y = np.apply_along_axis(f_peak_1d, 0, x.reshape(1, -1))

# get colormap
fig = plt.figure(figsize=(8, 4), dpi=100)
ax = fig.add_subplot(1,1,1)
ax.plot(x, y, linewidth=4, color="black")
ax.tick_params(axis="x", which="both", labelsize=16)
ax.tick_params(axis="y", which="both", labelsize=16)
ax.xaxis.set_major_locator(tk.MultipleLocator(1.0))
ax.yaxis.set_major_locator(tk.MultipleLocator(0.5))
fig.show()
fig.savefig("/mnt/ramdisk/peak-1d.png")
plt.close(fig)
#plot_scatter_by_label(xy[:, 0], xy[:, 1], "$x_1$", "$x_2$", "NatSign", marker='.', color=color, show_fig=True, save_path="/mnt/ramdisk/natsign.png")
