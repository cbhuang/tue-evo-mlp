from tensorflow.keras.datasets import mnist
import matplotlib.pyplot as plt
#import matplotlib.img as mpimg
import os
from pathlib import Path
import numpy as np

(x_train, y_train), (x_test, y_test) = mnist.load_data()

# every digit
for n in range(10):
    arr_i_sample = np.where(y_train == n)[0]
    i_sample = np.random.choice(arr_i_sample)
    sample = x_train[i_sample]
    plt.figure(figsize=(4, 4))
    plt.tick_params(
        axis='both',  # changes apply to the x-axis
        which='both',  # both major and minor ticks are affected
        left=False,
        right=False,
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        labelbottom=False,  # labels along the bottom edge are off
        labelleft=False
    )
    plt.imshow(sample, cmap='Greys')
    plt.savefig(f"/mnt/ramdisk/{n}.png")
    plt.show()
    plt.close()
    