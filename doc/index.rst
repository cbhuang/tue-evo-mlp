.. tue-evo-mlp documentation master file, created by
   sphinx-quickstart on Sun Aug 23 19:32:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Code Documentation of Bill's Master Thesis
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   src
   lib


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
