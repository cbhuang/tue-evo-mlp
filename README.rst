====================================================================
Introduction
====================================================================

:Author: Chuan-Bin "Bill" Huang
:Contact: cbbh0101@gmail.com
:Date: 2020-08-24
:Documentation Update: 2020-09-04

This is the publicized implementation of my master thesis titled "**Novel Evolutionary Algorithms for Robust Training of Very Small Multilayer Perceptron Models**" from `Department of Mathematics and Computer Science <https://www.tue.nl/en/our-university/departments/mathematics-and-computer-science>`_ of `Eindhoven University of Technology <https://www.tue.nl/en>`_, The Netherlands.

- `Code Repository <https://gitlab.com/cbhuang/tue-evo-mlp>`_
- `Online Documentation <https://cbhuang.gitlab.io/tue-evo-mlp>`_
- `Thesis <https://gitlab.com/cbhuang/tue-evo-mlp/-/blob/master/data/presentation/thesis.pdf>`_

.. note::

    1. This project is a minimal working implementation for my thesis.
       Usage other than reproducing the reported experiment results should
       be done on one's own responsibility.
    2. Due to the intrinsic randomness during training process of ANNs, the
       figures may not be perfectly reproducible. One should only expect
       qualitatively similar outcomes.


Snapshots
============================

Algorithms
------------

.. figure:: img_readme/algos.png
    :alt: Algos
    :align: center

    Proposed algorithms and evolution scale


Data
------------

.. figure:: img_readme/syn.png
    :alt: Synthetic
    :align: center
    
    Synthetic datasets


.. figure:: img_readme/mnist.png
    :alt: MNIST
    :align: center
    
    MNIST


Results
-------------

.. figure:: img_readme/hptune.png
    :alt: Hparam Tuning
    :align: center

    Hyperparameter Tuning


.. figure:: img_readme/gof.png
    :alt: Goodness-of-fit
    :align: center

    Goodness-of-fit (ZigZag, 16)


.. figure:: img_readme/history.png
    :alt: History
    :align: center
    
    History


.. figure:: img_readme/benchmark.png
    :alt: Benchmark
    :align: center
    
    Benchmark


System Requirements
=========================================================

- Win10 or Linux (tested on debian 10)
- A python installation with conda environment manager. Required packages were listed in ``conda-env.yml``
  + ``python`` >= 3.7
  + ``tensorflow`` or ``tensorflow-gpu`` >= 2.1.0
  + numpy, scipy, pandas, scikit-learn, h5py, matplotlib, seaborn, pymongo, ...
- MongoDB v4 (tested on v4.2+)


Execute
============================

(1) Setup MongoDB
--------------------

#. Install MongoDB (`official docs <https://docs.mongodb.com/manual/installation>`_).
#. (optional) Install a handy front-end for convenience (e.g. `Robo 3T <https://robomongo.org>`_)
#. `Create a user <https://docs.mongodb.com/manual/reference/method/db.createUser/index.html>`_
   named ``evo`` with password ``evo`` and access to a new database ``evoDB``.
   A sample script is located in ``user_scripts/mongodb_query/create_user.js``.
#. (optional) Previous results can be imported or exported by the following commands.
   The author's results were dumped into ``data/export/evoCollection-20200830.gz``
   for testing. The MongoDB commands should be identical for Windows and Linux.

   .. code-block:: bash

      # (Windows-only): switch to where your local mongodb installation locates
      C:
      cd "C:\Program Files\MongoDB\Server\4.2\bin"

      # import (restore) db from a gzip file
      mongorestore --gzip --archive="D:\GitLab\tue-evo-mlp\evoCollection-20200830.gz" --db evoDB

      # export (dump) db to a gzip file
      mongodump --gzip --archive="D:\GitLab\tue-wir-jobscraper\TEMP\tue_wir.20190310.gz" --db evoDB


(2) Setup Python Environment
-----------------------------------

#. Install python with ``conda`` environment manager. This typically means
   `Anaconda <https://www.anaconda.com/products/individual>`_ or
   `Miniconda <https://docs.conda.io/en/latest/miniconda.html>`_

#. Download this repository

   .. code-block:: bash

     $ git clone https://gitlab.com/cbhuang/tue-evo-mlp
     $ cd tue-evo-mlp

#. Change the environment name and prefix in ``conda-env.yml`` (as ``base``
   is usually not a good choice). We assume ``myenv`` hereafter.

#. Build conda environment in accordance with the conda requirement file,
   ``conda-env.yml``.

   .. code-block:: bash

     $ conda env create -f conda-env.yml


(3) Parameter Settings
-----------------------

See ``data/raw/*`` for working examples.

#. Main parameters

   .. code-block:: bash

      $ vim data/raw/<*params*.json>

#. Hyperparameters to be tuned (optional). Requires ``"hptune": false``
   in ``params.json``.

   .. code-block:: bash

      $ vim data/raw/<*hptune*.json>


(4) Main Experiment
-----------------------

#. Edit the main runfile. Fill in the params file, hptune file and config file.
   Also checkout the steps you would like to perform.

   .. code-block:: bash

       $ cd data/user_scripts
       $ vim run_evo.py

#. Activate the working conda environment. A prefix ``(myenv)`` in the
   console window will appear.

   .. code-block:: bash

      $ conda activate myenv

#. Run. **Note**: On a linux machine with bumblebee enabled,
   experiment a little bit to decide whether ``optirun python <run_xxx.py>``
   is faster or not. It may be slower to use GPU when the network is very
   small in size.

   .. code-block:: bash

      (myenv) $ python run_evo.py

#. Checkout where the batch were stored:

   - ``data/logs/<batchID>.txt``: batch ID and execution time
   - ``data/fig/<batchID>/``: visualization


(5) Analysis
------------------------------

#. Set up the analysis runfile and execute

   .. code-block:: bash

      (myenv) $ vim run_stats.py
      (myenv) $ python run_stats.py

#. Collect aggregation results:

   - ``data/out/<batchID>_aggr/df_choose_hp.csv``: hyperparameter tuning
   - ``data/fig/<batchID>_aggr/``: plots


Project Structure
============================

.. note::

    The complete settings and outputs of experiment no. 115 and 144 were
    included to help the user grasp the idea of how the output looks like.


- ``.gitlab-ci.yml``: GitLab continuous integration (CI) setup
- ``conda-env.yml``: requirement file of conda environment
- ``README.rst``: this document
- ``config/``: environment configs (mainly for MongoDB) and general settings
- ``data/``
    + ``fig/``: graphical outputs
    + ``logs/``: temporary directory for training logs
    + ``out/``: saved models
    + ``presentation/``: thesis, slides and public info
    + ``raw/``: parameter settings (all formal runs included)
- ``doc/``: standard Sphinx documentation
    + ``_build/html/``: output webpages
    + ``_static/``: custom resource files
    + ``conf.py``: build configurations
    + ``*.rst``: linker files
- ``lib/``: personal general-purpose cross-project code base
    + ``base_data_container.py``: data container class with some helper functions
    + ``keras_callback.py``: inherited standard ``keras`` callbacks
    + ``keras_model.py``: utilities for conventional training
    + ``keras_multistage.py``: utilities for multi-stage training
    + ``vis_template.py``: visualization templates
    + ``eng_utils.py``: engineering (non-mathematical) utility functions
    + ``math_utils.py``: mathematical utility functions
- ``src/``: project-specific modules
    + ``data_synthesis.py``: functions for generating synthetic datasets
    + ``evo_container.py``: master data container for evolutionary training
    + ``evo_callback.py``: regular keras callbacks for assisting evolutionary training
    + ``evo_multistage.py``: assisting module of ``evo_container.py`` (currently multistage callbacks)
- ``test/``: tests
    + ``test_*.py``: unit test files
    + ``conftest.py``: ``pytest`` configurations
    + Visualization & other tasks not suitable for unit tests
- ``user_scripts/``: codes for execution
    + ``run_evo.py``: run hyperparameter tuning or fixed-parameter experiments
    + ``run_stats.py``: run hyperparameter tuning or fixed-parameter experiments
    + ``merge_batches.py``: combine two batches (e.g. to resume after a crash)
    + ``mongodb_query/``: sample mongodb queries for debugging
    + ``parallel_coordinates/``: sample code for parallel coordinates plots using ``plotly``


Rebuild Documentation
============================

If somehow the online documentation contains severe error or does not suit
the user's need, then one may consider rebuild the documentation on his own.

#. First, comment out the ``autodoc_mock_imports`` option in ``doc/conf.py``.
#. Make other modifications as desired.
#. Build locally:

   .. code-block:: bash

       $ conda activate myenv
       (myenv) $ cd doc
       (myenv) $ sphinx-build -d _build/doctrees . _build/html

#. View in the `local development server <http://localhost:8000>`_ after firing up
   the following commands.

   .. code-block:: bash

       (myenv) $ cd doc/_build/html
       (myenv) $ python3 -m http.server


Acknowledgements
============================

Thanks to:

- Dr. Decebal C. Mocanu for his guidance in this thesis
- Prof. Dr. M. Pechenizkiy and Dr. O. Papapetrou for their review of this thesis
- Dr. E.C. Huang, Dr. Q.R. Huang, Dr. L.Y. Wang, Dr. S.C. Wu, and Dr. T.W. Xie
  for their valuable advice pertaining to the graphical details
- Dr. Chern Chuang for his prompt advice for latex editing.
- TU/e for providing the resources required to complete this thesis, including
  the Amandus H. Lundqvist Scholarship Program (ALSP), which is funding my studies.
- Dr. L.Y. Hsu and Ms. Mandy Liu who provided invaluable opportunities to polish
  my coding skills and made recommendations for my application to TU/e.
