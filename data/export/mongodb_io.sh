#!/bin/bash
#
# mongodb dump and restore
#

# dump
#mongodump --gzip --archive="evoCollection-20200830.gz" --db evoDB -u evo -p 'evo' 

# restore
#mongorestore --gzip --archive="evoCollection-20200830.gz" --db evoDB -u evo -p 'evo' 
