#!/bin/bash
#"""
# Validate json files before running.
#
# Requirement: ``sudo apt install jsonlint``
#"""

for f in *.json; do
    jsonlint-php "$f" &> /dev/null
    if (( $? > 0 )); then
        echo "Invalid json: $f"
    fi
done
