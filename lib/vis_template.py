"""
Visualization code for quick reuse. Not fine-tuned.

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

"""

from pathlib import Path
import matplotlib.pyplot as plt
from seaborn import heatmap
import numpy as np


def plot_multi_y(x, ls_y, ls_labels, xlabel, ylabel, title, plot_format=" .",
                 color=None, linewidth=2,
                 xlim=None, ylim=None,
                 xticks=None, yticks=None,
                 legend=True, legend_loc="best",
                 fill_between=None, fill_alpha=0.2, fill_color="#939090",
                 tight_layout=True, show_fig=True, save_path=None,
                 figsize=(8, 6), dpi=100,
                 fontsize_title=21, fontsize_axeslabel=16,
                 fontsize_ticklabel=14, fontsize_legend=15):
    """A quick basic plotter.

    Args:
        x (numpy.array): x axis
        ls_y (list|numpy.array): y series to be plotted
        ls_labels (list[str]): labels of y series (required)
        xlabel (str): for ``ax.set_xlabel()``
        ylabel (str): for ``ax.set_ylabel()``
        title (str): latex-compatible string
        plot_format (str|list): line and marker symbol
        color (str|list|ndarray): color of lines
        linewidth (int|float|list): linewidths
        xlim (tuple|list): for ``ax.set_xlim()``
        ylim (tuple|list): for ``ax.set_ylim()``
        xticks (tuple|list|ndarray): for ``ax.xaxis.set_ticks()``
        yticks (tuple|list|ndarray): for ``ax.yaxis.set_ticks()``
        legend (bool): show legend or not
        legend_loc (str): for ``ax.legend(loc=)``
        fill_between (tuple[int, int]): between which series
        fill_alpha (float): transparency for ``fill_between``
        fill_color (str): color for ``fill_between()``
        tight_layout (gool): apply ``tight_layout()`` before output
        show_fig (bool): show figure
        save_path (str): path to save. None=Won't save.
        figsize (tuple): figsize in ``plt.figure()``
        dpi (int): dpi in ``plt.figure()``
        fontsize_title (int): fontsize in ``ax.set_title()``
        fontsize_axeslabel (int): fontsize in ``ax.set_xlabel()``, ``ax.set_ylabel()``
        fontsize_ticklabel (int): fontsize in ``ax.set_xticklabel()``, ``ax.set_yticklabel()``
        fontsize_legend (int): fontsize in ``ax.legend()``
    """

    # check data size
    n_series = len(ls_y)
    assert n_series == len(ls_labels), f"len(ls_y) = {n_series} != {len(ls_labels)} = len(ls_labels)!"
    n_pts = len(x)
    for i in range(n_series):
        assert n_pts == len(ls_y[i]), f"len(x) = {n_pts} != {len(ls_y[i])} = len(ls_y[i])!"

    # plot formats
    if isinstance(plot_format, str):
        ls_plot_format = [plot_format] * n_series
    elif isinstance(plot_format, list):
        assert n_series == len(plot_format), f"len(ls_y) = {n_series} != {len(plot_format)} = len(plot_format)!"
        ls_plot_format = plot_format
    else:
        raise ValueError(f"plot_format not understood: {plot_format}")

    # lines
    if isinstance(color, str):
        ls_colors = [color] * n_series
    elif isinstance(color, (list, np.ndarray)):
        assert n_series == len(color), f"len(ls_y)={n_series} != {len(color)}=len(color)!"
        ls_colors = color
    elif color is None:
        ls_colors = [None] * n_series
    else:
        raise ValueError(f"color not understood: {color}")

    # linewidths
    if isinstance(linewidth, (int, float)):
        ls_linewidths = [linewidth] * n_series
    elif isinstance(linewidth, list):
        assert n_series == len(linewidth), f"len(ls_y)={n_series} != {len(linewidth)}=len(linewidth)!"
        ls_linewidths = linewidth
    else:
        raise ValueError(f"linewidth not understood: {linewidth}")

    # fig, ax
    fig = plt.figure(figsize=figsize, dpi=dpi)
    ax = fig.add_subplot(1, 1, 1)
    if len(title) > 0:
        ax.set_title(title, fontsize=fontsize_title)

    # plot
    for i in range(n_series):
        ax.plot(x, ls_y[i],
                ls_plot_format[i], color=ls_colors[i], label=ls_labels[i],
                linewidth=ls_linewidths[i])

    # plot shaded region
    if fill_between is not None:
        i1, i2 = fill_between[0], fill_between[1]
        ax.fill_between(x, ls_y[i1], ls_y[i2], color=fill_color, alpha=fill_alpha)

    # label
    ax.set_xlabel(xlabel, fontsize=fontsize_axeslabel)
    ax.set_ylabel(ylabel, fontsize=fontsize_axeslabel)
    # range
    if xlim is not None:
        ax.set_xlim(xlim)
    if ylim is not None:
        ax.set_ylim(ylim)
    # xticks, yticks
    if xticks is not None:
        ax.xaxis.set_ticks(xticks)
    if yticks is not None:
        ax.xaxis.set_ticks(yticks)
    # tick_params
    ax.tick_params(axis="both", labelsize=fontsize_ticklabel)
    # legend
    if legend:
        ax.legend(loc=legend_loc, fontsize=fontsize_legend)
    # layout
    if tight_layout:
        fig.tight_layout()

    # output
    if save_path is not None:
        Path(save_path).parent.mkdir(parents=True, exist_ok=True)
        fig.savefig(str(save_path))

    if show_fig:
        fig.show()

    plt.close(fig)


def plot_multi_x_multi_y(ls_x, ls_y, ls_labels, xlabel, ylabel, title, plot_format=" .",
                         color=None, linewidth=2,
                         xlim=None, ylim=None,
                         xticks=None, yticks=None,
                         legend=True, legend_loc="best",
                         fill_between=None, fill_alpha=0.2, fill_color="#939090",
                         tight_layout=True, show_fig=True, save_path=None,
                         figsize=(8, 6), dpi=100,
                         fontsize_title=21, fontsize_axeslabel=16,
                         fontsize_ticklabel=14, fontsize_legend=15):
    """A quick basic plotter.

    Args:
        ls_x (list|numpy.array): x axis
        ls_y (list|numpy.array): y series to be plotted
        ls_labels (list[str]): labels of y series (required)
        xlabel (str): for ``ax.set_xlabel()``
        ylabel (str): for ``ax.set_ylabel()``
        title (str): latex-compatible string
        plot_format (str|list): line and marker symbol
        color (str|list|ndarray): color of lines
        linewidth (int|float|list): linewidths
        xlim (tuple|list): for ``ax.set_xlim()``
        ylim (tuple|list): for ``ax.set_ylim()``
        xticks (tuple|list|ndarray): for ``ax.xaxis.set_ticks()``
        yticks (tuple|list|ndarray): for ``ax.yaxis.set_ticks()``
        legend (bool): show legend or not
        legend_loc (str): for ``ax.legend(loc=)``
        fill_between (tuple[int, int]): between which series
        fill_alpha (float): transparency for ``fill_between``
        fill_color (str): color for ``fill_between()``
        tight_layout (gool): apply ``tight_layout()`` before output
        show_fig (bool): show figure
        save_path (str): path to save. None=Won't save.
        figsize (tuple): figsize in ``plt.figure()``
        dpi (int): dpi in ``plt.figure()``
        fontsize_title (int): fontsize in ``ax.set_title()``
        fontsize_axeslabel (int): fontsize in ``ax.set_xlabel()``, ``ax.set_ylabel()``
        fontsize_ticklabel (int): fontsize in ``ax.set_xticklabel()``, ``ax.set_yticklabel()``
        fontsize_legend (int): fontsize in ``ax.legend()``
    """

    # check n_series
    n_series = len(ls_y)
    assert n_series == len(ls_x)
    assert n_series == len(ls_labels), f"len(ls_y) = {n_series} != {len(ls_labels)} = len(ls_labels)!"

    # check length
    for i in range(n_series):
        assert len(ls_x[i]) == len(ls_y[i]), f"i={i}: len(ls_x[i]) = {len(ls_x[i])} != {len(ls_y[i])} = len(ls_y[i])!"

    # plot formats
    if isinstance(plot_format, str):
        ls_plot_format = [plot_format] * n_series
    elif isinstance(plot_format, list):
        assert n_series == len(plot_format), f"len(ls_y) = {n_series} != {len(plot_format)} = len(plot_format)!"
        ls_plot_format = plot_format
    else:
        raise ValueError(f"plot_format not understood: {plot_format}")

    # lines
    if isinstance(color, str):
        ls_colors = [color] * n_series
    elif isinstance(color, (list, np.ndarray)):
        assert n_series == len(color), f"len(ls_y)={n_series} != {len(color)}=len(color)!"
        ls_colors = color
    elif color is None:
        ls_colors = [None] * n_series
    else:
        raise ValueError(f"color not understood: {color}")

    # linewidths
    if isinstance(linewidth, (int, float)):
        ls_linewidths = [linewidth] * n_series
    elif isinstance(linewidth, list):
        assert n_series == len(linewidth), f"len(ls_y)={n_series} != {len(linewidth)}=len(linewidth)!"
        ls_linewidths = linewidth
    else:
        raise ValueError(f"linewidth not understood: {linewidth}")

    # fig, ax
    fig = plt.figure(figsize=figsize, dpi=dpi)
    ax = fig.add_subplot(1, 1, 1)
    if len(title) > 0:
        ax.set_title(title, fontsize=fontsize_title)

    # plot
    for i in range(n_series):
        ax.plot(ls_x[i], ls_y[i],
                ls_plot_format[i], color=ls_colors[i], label=ls_labels[i],
                linewidth=ls_linewidths[i])

    # plot shaded region
    if fill_between is not None:
        i1, i2 = fill_between[0], fill_between[1]
        assert np.all(ls_x[i1] == ls_x[i2]), "fill_between(): must have the same x domain"
        ax.fill_between(ls_x[i1], ls_y[i1], ls_y[i2], color=fill_color, alpha=fill_alpha)

    # label
    ax.set_xlabel(xlabel, fontsize=fontsize_axeslabel)
    ax.set_ylabel(ylabel, fontsize=fontsize_axeslabel)
    # range
    if xlim is not None:
        ax.set_xlim(xlim)
    if ylim is not None:
        ax.set_ylim(ylim)
    # xticks, yticks
    if xticks is not None:
        ax.xaxis.set_ticks(xticks)
    if yticks is not None:
        ax.xaxis.set_ticks(yticks)
    # tick_params
    ax.tick_params(axis="both", labelsize=fontsize_ticklabel)
    # legend
    if legend:
        ax.legend(loc=legend_loc, fontsize=fontsize_legend)
    # layout
    if tight_layout:
        fig.tight_layout()

    # output
    if save_path is not None:
        Path(save_path).parent.mkdir(parents=True, exist_ok=True)
        fig.savefig(str(save_path))

    if show_fig:
        fig.show()

    plt.close(fig)


def plot_true_vs_pred(
        x, ls_y, xlabel, ylabel, title,  figsize=(8, 6), dpi=100,
        plot_format=" .", color=None, xlim=None, ylim=None, legend=True,
        legend_loc="best", tight_layout=True, show_fig=True, save_path=None,
        fontsize_title=20, fontsize_axeslabel=16, fontsize_ticklabel=14,
        fontsize_legend=15):
    """A special case of :func:`plot_multi_y()`. See its docs."""

    assert len(ls_y) == 2
    assert len(x) == len(ls_y[0]) == len(ls_y[1])

    ls_labels = ["$Y_{true}$", "$Y_{pred}$"]

    plot_multi_y(
        x, ls_y, ls_labels, xlabel, ylabel, title, plot_format=plot_format,
        color=color,
        xlim=xlim, ylim=ylim, legend=legend, legend_loc=legend_loc,
        tight_layout=tight_layout, show_fig=show_fig, save_path=save_path,
        figsize=figsize, dpi=dpi, fontsize_title=fontsize_title,
        fontsize_axeslabel=fontsize_axeslabel, fontsize_ticklabel=fontsize_ticklabel,
        fontsize_legend=fontsize_legend
    )


def plot_confusion_matrix(
        cm, figsize=(9.5, 8), dpi=100, title="Confusion Matrix",
        fontsize_title=20, annot=True, fmt='d', cmap="Greens", xlabel='Predicted Class',
        ylabel='True Class', fontsize_axeslabel=16, fontsize_ticklabel=14,
        cbar_label="Count", cbar_label_format="%d", n_cbar_intervals=0,
        tight_layout=True, show_fig=False, save_path=None):
    """
    Plot a confusion matrix using ``seaborn.heatmap``.

    Args:
        cm (numpy.array): confusion matrix
        figsize (tuple): figure size * dpi
        dpi (int): dpi
        title (str): main title
        fontsize_title (int|float): fontsize of figure title
        annot (bool): annotate the matrix or not
        fmt (str): format of annotation (see seaborn manual)
        cmap (str): colormap
        xlabel (str): x axis label
        ylabel (str): y axis label
        fontsize_axeslabel (int|float): fontsize of xlabel, ylabel
        fontsize_ticklabel (int|float): fontsize of xticklabel, yticklabel
        cbar_label (str): colorbar label
        cbar_label_format (str): colorbar format string (see seaborn manual)
        n_cbar_intervals (int): 0=auto, colorbar label intervals
        tight_layout (bool): apply tight_layout or not
        show_fig (bool): show figure or not
        save_path (PathLike): save to location if assigned
    """
    fig = plt.figure(figsize=figsize, dpi=dpi)
    ax = heatmap(cm, annot=annot, cmap=cmap, fmt=fmt,
                 cbar_kws={"label": cbar_label, "format": cbar_label_format})
    ax.set_title(title, fontsize=fontsize_title)
    ax.set_xlabel(xlabel, fontsize=fontsize_axeslabel)
    ax.set_ylabel(ylabel, fontsize=fontsize_axeslabel)
    ax.tick_params(axis="both", labelsize=fontsize_ticklabel)

    # auto set colorbar ticks
    v_max, v_min = np.max(cm), np.min(cm)
    if n_cbar_intervals == 0:
        den = min(10, v_max - v_min)  # at most 10 ticks
    else:
        den = n_cbar_intervals

    if den == 0:
        ticks = [v_min]
    else:
        ticks = [v_min + int((v_max - v_min) * i / den) for i in range(1+den)]

    cbar = ax.collections[0].colorbar
    cbar.set_ticks(ticks)

    if tight_layout:
        fig.tight_layout()

    if save_path is not None:
        Path(save_path).parent.mkdir(parents=True, exist_ok=True)
        fig.savefig(str(save_path))

    if show_fig:
        fig.show()

    plt.close(fig)


def plot_scatter_by_label(
        x, y, xlabel, ylabel, title, marker=".", color=None,
        xlim=None, ylim=None,
        xticks=None, yticks=None,
        legend=False, legend_loc="best",
        tight_layout=True, show_fig=True, save_path=None,
        figsize=(8, 6), dpi=100,
        fontsize_title=21, fontsize_axeslabel=16,
        fontsize_ticklabel=14, fontsize_legend=15):
    """A quick basic scatterplot.

    Args:
        x (numpy.array): x axis
        y (list|numpy.array): y series to be plotted
        xlabel (str): for ``ax.set_xlabel()``
        ylabel (str): for ``ax.set_ylabel()``
        title (str): latex-compatible string
        marker (str|list): line and marker symbol
        color (str|list|ndarray): color of lines
        xlim (tuple|list): for ``ax.set_xlim()``
        ylim (tuple|list): for ``ax.set_ylim()``
        xticks (tuple|list|ndarray): for ``ax.xaxis.set_ticks()``
        yticks (tuple|list|ndarray): for ``ax.yaxis.set_ticks()``
        legend (bool): show legend or not
        legend_loc (str): for ``ax.legend(loc=)``
        tight_layout (gool): apply ``tight_layout()`` before output
        show_fig (bool): show figure
        save_path (str): path to save. None=Won't save.
        figsize (tuple): figsize in ``plt.figure()``
        dpi (int): dpi in ``plt.figure()``
        fontsize_title (int): fontsize in ``ax.set_title()``
        fontsize_axeslabel (int): fontsize in ``ax.set_xlabel()``, ``ax.set_ylabel()``
        fontsize_ticklabel (int): fontsize in ``ax.set_xticklabel()``, ``ax.set_yticklabel()``
        fontsize_legend (int): fontsize in ``ax.legend()``
    """

    n = len(x)

    # check data size
    assert n == len(y)

    # color
    if color is not None:
        assert len(color) == n

    # fig, ax
    fig = plt.figure(figsize=figsize, dpi=dpi)
    ax = fig.add_subplot(1, 1, 1)
    if len(title) > 0:
        ax.set_title(title, fontsize=fontsize_title)

    # plot
    ax.scatter(x, y, marker=marker, color=color)

    # label
    ax.set_xlabel(xlabel, fontsize=fontsize_axeslabel)
    ax.set_ylabel(ylabel, fontsize=fontsize_axeslabel)
    # range
    if xlim is not None:
        ax.set_xlim(xlim)
    if ylim is not None:
        ax.set_ylim(ylim)
    # xticks, yticks
    if xticks is not None:
        ax.xaxis.set_ticks(xticks)
    if yticks is not None:
        ax.xaxis.set_ticks(yticks)
    # tick_params
    ax.tick_params(axis="both", labelsize=fontsize_ticklabel)
    # legend
    if legend:
        ax.legend(loc=legend_loc, fontsize=fontsize_legend)
    # layout
    if tight_layout:
        fig.tight_layout()

    # output
    if save_path is not None:
        Path(save_path).parent.mkdir(parents=True, exist_ok=True)
        fig.savefig(str(save_path))

    if show_fig:
        fig.show()

    plt.close(fig)
