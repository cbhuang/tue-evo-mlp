"""Miscellaneous general-purpose utility functions for engineering.

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

Contents:
    - Type check functions
    - Print / logging functions
    - Data I/O functions
    - JSON, serialization functions
    - Database-related functions
"""

from datetime import datetime
import numpy as np
import sklearn


def log_prefix(txt='DEBUG', no_date=True):
    """A convenient prefix for ``print()``.

    Args:
        txt (str): info label
        no_date (bool): do not print date

    Returns:
        str

    Usage:
        .. code-block:: python

            from eng_util import log_prefix as lp
            print(f"{lp('INFO')} log_text")
    """

    if no_date:
        return f"{datetime.now().strftime('%T')} [{txt.upper()}]"
    else:
        return f"{datetime.now().strftime('%D %T')} [{txt.upper()}]"


def print_loglike(txt, level='DEBUG', no_date=True):
    """A convenient prefix for ``print()``.

    Args:
        txt (str): info label
        level (str): level tag (DEBUG, INFO, ERROR, ...)
        no_date (bool): do not print date

    Usage:
        .. code-block:: python

            from eng_util import print_loglike as printll
            printll(my_text, "INFO")
    """

    if no_date:
        print(f"{datetime.now().strftime('%T')} [{level.upper()}] {txt}")
    else:
        print(f"{datetime.now().strftime('%D %T')} [{level.upper()}] {txt}")


def is_float(s):
    """Check by experiment if a string can be interpreted as float.

    Args:
        s (str): string

    Returns:
        bool
    """
    try:  # attempt casting
        _ = float(s)
        return True   # if success then fine
    except ValueError:
        return False  # else discard error


def save_str_dict_as_json(dic, fname):
    """
    Save a dict into proper json format by using ``str(dict)``. Handy
    for dealing with floats items which doesn't work with ``json.dump()``.

    Args:
        dic (dict): the object to be saved
        fname (PathLike): destination path

    Note:
        Does NOT work with dict containing single or double quotes as contents!
    """
    # Steps to process the dict object
    #   1. str: stringify
    #   2. [1:-1]: remove leading and trailing double quotes
    #   3. {{ and }}: add with braces
    #   4. replace: substitute single quotes to double quotes
    with open(fname, "w") as f:
        f.write(f"{{{str(dic)[1:-1]}}}".replace("'", '"'))


def seconds_to_hms(seconds, decimals=0, colons=True, alphabets=True):
    """Turn seconds to hh:mm:ss(.ss...s).

    Args:
        seconds (int|float): seconds
        decimals (int): decimal places of seconds
        colons (bool): add colon
        alphabets (bool) print h, m, s or not

    Returns:
        str
    """
    sign = "" if seconds >= 0 else "-"
    abs_sec = abs(seconds)
    h = int(abs_sec / 3600)
    m = int(abs_sec / 60) - 60 * h
    s = abs_sec % 60
    sep = ":" if colons else ""

    if alphabets:
        if decimals == 0:
            return f"{sign}{h:02d}h{sep}{m:02d}m{sep}{int(s):02d}s"
        else:
            return f"{sign}{h:02d}h{sep}{m:02d}m{sep}{s:0{decimals+3}.{decimals}f}s"
    else:  # no alphabets
        if decimals == 0:
            return f"{sign}{h:02d}{sep}{m:02d}{sep}{int(s):02d}"
        else:
            return f"{sign}{h:02d}{sep}{m:02d}{sep}{s:0{decimals+3}.{decimals}f}"


def subset_dicts_by_dict(ls_dicts, dic_match, ret=2):
    """Subset a list of dictionaries by matching the key-value
    pairs given in a query dictionary. Same as mongodb
    ``db.collection.find(dic_subset)``.

    Args:
        ls_dicts (list[dict]): document set
        dic_match (dict): query dict
        ret (int): 1=return indexes, 2=return docs, 3=return a list of (idx, doc)

    Returns:
        list: list[dict] or list[(int, dict)]
    """
    assert ret in (1, 2, 3), f"ret not understood: {ret}"

    ls_docs = []
    ls_idx = []

    for i, doc in enumerate(ls_dicts):
        for k, v in dic_match.items():
            # if a property were not match, proceed to next doc
            if doc[k] != v:
                break
        else:  # append only if not break
            if ret in (1, 3):
                ls_idx.append(i)
            if ret in (2, 3):
                ls_docs.append(doc)

    if ret == 1:
        return ls_idx
    elif ret == 2:
        return ls_docs
    else:
        return list(zip(ls_idx, ls_docs))


def simple_dict_to_suffix(dic, sep="_"):
    """A simple dict to suffix

    Args:
        dic (dict): a simple dict. keys and values are not iterables.
        sep (str): separator

    Returns:
        str
    """
    ls = []
    for k, v in dic.items():
        ls.append(f"{k}={v}")
    return sep.join(ls)


def timestamp_to_suffix(timestamp, fmt="%Y%m%d_%H%M%S", decimal=0):
    """Convert a timestamp into a human-readable suffix.
    Rounds down smaller decimal places.

    Args:
        timestamp (float): timestamp in seconds
        fmt (str): strftime format
        decimal (int): how many decimal places to keep (down to us)

    Returns:
        str
    """
    if decimal == 0:
        return datetime.fromtimestamp(timestamp).strftime(fmt)
    else:
        return datetime.fromtimestamp(timestamp).strftime(fmt)[:16+decimal]


def is_type_int_or_np_int(type_):
    """The type is int or an numpy integer type or not.

    Args:
        type_ (type): data type

    Returns:
        bool
    """
    if type_ in (int, np.int8, np.int16, np.int32, np.int64,
                 np.uint8, np.uint16, np.uint32, np.uint64):
        return True
    else:
        return False


def is_type_float_or_np_float(type_):
    """The type is float or an numpy float type or not.

    Args:
        type_ (type): data type

    Returns:
        bool
    """
    if type_ in (float, np.float32, np.float64, np.float16):
        return True
    else:
        return False


def dump_sklearn_scaler_params(scaler, json_format=True):
    """Retrieve parameters of scalers from ``sklearn.preprocessing``
    for future reuse.

    Args:
        scaler (MinMaxScaler|StandardScaler|LabelEncoder)
        json_format (bool): cast ``array`` or ``tuple`` into ``list`` for JSON output

    Returns:
        dict
    """
    ans = {}
    if isinstance(scaler, (sklearn.preprocessing.MinMaxScaler,
                           sklearn.preprocessing.StandardScaler,
                           sklearn.preprocessing.LabelEncoder)):
        for k, v in scaler.__dict__.items():

            # convert iterables to lists, and np numbers to python numbers
            if json_format:
                if isinstance(v, np.ndarray):
                    ans[k] = v.tolist()  # will auto convert (float|int)(32|64) to (float|int)
                elif isinstance(v, tuple):
                    ans[k] = list(v)
                elif is_type_int_or_np_int(type(v)):
                    ans[k] = int(v)
                elif is_type_float_or_np_float(type(v)):
                    ans[k] = float(v)
                else:
                    ans[k] = v

            else:  # no coversion
                ans[k] = v
    else:
        raise NotImplementedError(f"type(scaler) not understood: {type(scaler)}")

    return ans


def restore_sklearn_scaler(scaler, dic):
    """Counterpart of ``dump_sklearn_scaler_params()``.

    Args:
        scaler (MinMaxScaler|StandardScaler|LabelEncoder): subject
        dic (dict): params

    Returns:
        MinMaxScaler|StandardScaler|LabelEncoder
    """
    if isinstance(scaler, sklearn.preprocessing.MinMaxScaler):
        scaler.__setattr__("feature_range", tuple(dic["feature_range"]))
        scaler.__setattr__("copy", bool(dic["copy"]))
        scaler.__setattr__("min_", np.array(dic["min_"]))
        scaler.__setattr__("scale_", np.array(dic["scale_"]))
        scaler.__setattr__("data_min_", np.array(dic["data_min_"]))
        scaler.__setattr__("data_max_", np.array(dic["data_max_"]))
        scaler.__setattr__("data_range_", np.array(dic["data_range_"]))
        if is_type_int_or_np_int(type(dic["n_samples_seen_"])):
            scaler.__setattr__("n_samples_seen_", dic["n_samples_seen_"])
        else:
            scaler.__setattr__("n_samples_seen_", np.array(dic["n_samples_seen_"]))

    elif isinstance(scaler, sklearn.preprocessing.StandardScaler):
        scaler.__setattr__("copy", bool(dic["copy"]))
        scaler.__setattr__("with_mean", bool(dic["with_mean"]))
        scaler.__setattr__("with_std", bool(dic["with_std"]))
        scaler.__setattr__("scale_", np.array(dic["scale_"]))
        scaler.__setattr__("mean_", np.array(dic["mean_"]))
        scaler.__setattr__("var_", np.array(dic["var_"]))
        if is_type_int_or_np_int(type(dic["n_samples_seen_"])):
            scaler.__setattr__("n_samples_seen_", dic["n_samples_seen_"])
        else:
            scaler.__setattr__("n_samples_seen_", np.array(dic["n_samples_seen_"]))

    elif isinstance(scaler, sklearn.preprocessing.LabelEncoder):
        scaler.__setattr__("classes_", np.array(dic["classes_"]))

    else:
        raise NotImplementedError(f"type(scaler) not understood: {type(scaler)}")
    return scaler


def copy_array_topleft(src, dest):
    """Copy the valous of an array to the topleft corner of another
    array. Supports 1D to 3D arrays.

    Args:
        src (numpy.array): source
        dest (numpy.array): destination
    """

    # empty src -> do nothing
    if len(src) == 0:
        return

    # check dimension
    n = len(src.shape)
    if len(dest.shape) != n:
        raise ValueError(f"Array dimension mismatch: src={src.shape}, dest={dest.shape}")

    if n == 1:
        dest[:src.shape[0]] = src
    elif n == 2:
        dest[:src.shape[0], :src.shape[1]] = src
    elif n == 3:
        dest[:src.shape[0], :src.shape[1], :src.shape[2]] = src
    else:
        raise ValueError(f"Unsupported dimension: n={n}")


def tuple_to_str_compact(tup, fmt=None):
    """Return a compact string expressing a tuple.

    Args:
        tup (tuple|list|ndarray): input tuple
        fmt (str|None): f-string for each element

    Returns:
        str
    """

    ls = ["("]
    for i, elem in enumerate(tup):
        comma = "," if i > 0 else ""
        if fmt is None:
            ls.append(f"{comma}{elem}")
        else:
            ls.append(f"{comma}{elem:{fmt}}")
    ls.append(")")

    return "".join(ls)
