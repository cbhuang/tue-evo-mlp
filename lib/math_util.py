"""General-purpose math functions.

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

"""

import numpy as np
from tensorflow.keras import backend as K


def eq_f32(v1, v2, err_ratio=1e-5, err_abs=1e-5):
    """Check whether 2 float32 numbers are equal considering rounding error.

    Args:
        v1 (float): value 1
        v2 (float): value 2
        err_ratio (float): allowed difference in ratio
        err_abs (float): allowed difference in absolute value

    Returns:
        bool

    TODO:
        Cythonize
    """

    # 1. normal values: check both
    if min(abs(v1), abs(v2)) >= err_abs:
        return True if (abs(v1 - v2) < err_abs) and (abs(abs(1.0 * v1 / v2) - 1.0) < err_ratio) else False

    # 2. if any number is zero: check difference
    elif (v1 == 0) or (v2 == 0):
        return True if abs(v1 - v2) < err_abs else False

    # 3. if values are non-zero but one of them is value: check ratio
    else:
        return True if abs(abs(1.0 * v1 / v2) - 1.0) < err_ratio else False


def randn_abs_at_least(threshold, std=1.0):
    """Get a number from N(0, std) but re-roll until its absolute value is
    greater than the threshold.

    Args:
        threshold (float): threshold for the absolute value.
        std (float): standard deviation

    Returns:
        float

    TODO:
        Cythonize
    """
    while True:
        delta = std * np.random.randn()
        if abs(delta) > threshold:
            return delta


def random_uniform_two_sided(shape, lb, ub, dtype=np.float32):
    """Generate an random array where the absolute value of each element
    lies between ``[lb, ub)``. Both positive and negative
    values are possible. The number is drawn from a uniform distribution.

    Args:
        shape (int|tuple): array shape
        lb (float): lower bound
        ub (float): upper bound
        dtype (type): data type

    Returns:
        numpy.array
    """
    assert lb >= 0 and ub >= 0

    # [ub - 2*(ub-lb) <-> ub - (ub-lb) <-> ub]
    arr = np.random.uniform(2*lb - ub, ub, shape).astype(dtype)

    # shift the minus part
    if lb > 0:
        arr[arr < lb] -= 2 * lb

    return arr


def random_normal_with_threshold(shape, threshold, scale, dtype=np.float32):
    """Generate an random array where the absolute value of each element
    is ``lb + scale * N(0,1)``. Both positive and negative
    values are possible. If the number drawn from N(0,1) is positive, then
    add it by ``threshold``. If the number drawn from N(0,1) is negative, then
    subtract it by ``threshold``. This is to ensure a minimum jump to make
    the jump meaningful.

    Args:
        shape (int|tuple): array shape
        threshold (float): minimum jump threshold
        scale (float): jump scale
        dtype (type): data type

    Returns:
        numpy.array
    """
    assert threshold >= 0 and scale >= 0

    arr = np.random.normal(0, scale, shape).astype(dtype)

    # shift the minus part
    if threshold > 0:
        arr[arr >= 0] += threshold
        arr[arr < 0] -= threshold

    return arr


def r2_metric(y_true, y_pred):
    """Simple implementation of R^2 metric in Tensorflow.
    ``sklearn.metric.r2_score`` won't work.

    Args:
        y_true (array[float]): true values
        y_pred (array[float]): predicted values

    Returns:
        float: R^2 value
    """
    sst = K.sum(K.square(y_true - K.mean(y_true)))  # Total SS (against sample mean)
    sse = K.sum(K.square(y_true - y_pred))  # Sum of square (SS) of error
    return 1.0 - sse / (sst + K.epsilon())


def count_through_list(ls, which_stage, which_step_in_stage,
                       input_one_based=True, count_one_based=True):
    """Sum through a list up to a point.

    Args:
        ls (list|tuple|numpy.array): list of counts
        which_stage (int): which stage (which element of ``ls``)
        which_step_in_stage (int): which step in the stage
        input_one_based (bool): is stage and step using 1-based notation?
        count_one_based (bool): is counting also 1-based?

    Returns:
        int
    """

    # get index
    if input_one_based:
        idx_stage = which_stage - 1
        idx_step = which_step_in_stage - 1
    else:
        idx_stage = which_stage
        idx_step = which_step_in_stage

    # guard out-of-bound error
    assert 0 <= idx_stage <= len(ls) - 1, f"which_stage out of bound: {which_stage}"
    assert 0 <= which_step_in_stage <= ls[idx_stage], f"which_step_in_stage out of bound: {which_step_in_stage}"

    # sum
    if count_one_based:
        # steps before + current step
        return int(np.sum(ls[:idx_stage]) + which_step_in_stage)
    else:
        # 0 is actually 1, 1 is actually 2, etc.
        return int(np.sum(ls[:idx_stage]) + idx_stage + idx_step + 1)


def glorot_uniform(fan_in, fan_out, shape):
    """Return a random array initialized by glorot-uniform formula.

    Reference:
        https://www.tensorflow.org/api_docs/python/tf/keras/initializers/GlorotUniform

    Args:
        fan_in (int): the number of input units in the weight tensor
        fan_out (int): he number of output units in the weight tensor
        shape (tuple): returning shape

    Returns:
        numpy.array
    """

    limit = np.sqrt(6 / (fan_in + fan_out))
    return np.random.uniform(-limit, limit, shape)


def random_mask(arr_shape, k, dtype=bool):
    """Generate an 0/1 mask with size = n and k elements with value 1.

    Args:
        arr_shape (int|tuple[int]): shape of the array
        k (int): how many elements has value 1
        dtype (type): array data type

    Returns:
        numpy.array
    """
    n = np.prod(arr_shape)
    if k == n:
        return np.ones(arr_shape, dtype=dtype)
    elif 0 <= k < n:
        mask = np.zeros(n, dtype=dtype)
        mask[np.random.permutation(n)[:k]] = 1
        return mask.reshape(arr_shape)
    else:
        raise ValueError(f"Bad (n, k): ({n},{k})")


def where_to_mask(where, shape):
    """Convert ``np.where`` to boolean mask.

    Args:
        where (tuple[numpy.array]): tuple of indices from np.where
        shape (tuple): shape

    Returns:
        numpy.array
    """
    ans = np.zeros(shape, dtype=bool)
    ans[where] = True
    return ans


def idx_to_mask(idx, shape):
    """Convert an 1-D index array to a boolean mask.

    Args:
        idx (numpy.array): an 1-D array of indices
        shape (tuple): shape

    Returns:
        numpy.array[bool]
    """

    ans = np.zeros(shape, dtype=bool)

    # guard singlar cases
    if (idx is not None) and (len(idx) > 0):
        ans[np.unravel_index(idx, shape)] = True

    return ans


def where_subset_k(arr, mask, k, mode="min"):
    """Return the indices of the smallest k elements from a subset of
    an ``numpy.array``.

    Args:
        arr (numpy.array): source array
        mask (numpy.array[bool]): a boolean mask to subset the array
        k (int): return k elements
        mode (str): minimum or maximum k

    Returns:
        tuple[numpy.array]: like the return of ``numpy.where``
    """
    # e.g. arr = array([[1,9,3],[2,6,4]]);
    #      mask = (arr < 5) = array([[T,F,T],[T,F,T]])
    #      k = 3
    where_subset = np.where(mask)  # (array([0, 0, 1, 1]), array([0, 2, 0, 2]))
    idx_subset = np.ravel_multi_index(where_subset, arr.shape)  # array([0, 2, 3, 5])
    arr_subset = arr[mask]  # array([1, 3, 2, 4]) !!!!!flattened!!!!!

    if mode == "min":
        idx_k = np.argsort(arr_subset)[:k]  # array([0, 2, 1])
    elif mode == "max":
        idx_k = np.argsort(arr_subset)[-k:]
    else:
        raise ValueError(f"mode not understood: {mode}")

    idx_subset_k = idx_subset[idx_k]  # array([0, 3, 2])
    where_subset_k = np.unravel_index(idx_subset_k, arr.shape)  # (array([0, 1, 0]), array([0, 0, 2]))
    return where_subset_k


def mask_subset_k(arr, mask, k, mode="min"):
    """Return the boolean mask of the smallest k elements from a subset of
    an ``numpy.array``.

    Args:
        arr (numpy.array): source array
        mask (numpy.array[bool]): a boolean mask to subset the array
        k (int): return k elements
        mode (str): minimum or maximum k

    Returns:
        numpy.array[bool]
    """
    # e.g. arr = array([[1,9,3],[2,6,4]]);
    #      mask = (arr < 5) = array([[T,F,T],[T,F,T]])
    #      k = 3
    where_subset = np.where(mask)  # (array([0, 0, 1, 1]), array([0, 2, 0, 2]))
    idx_subset = np.ravel_multi_index(where_subset, arr.shape)  # array([0, 2, 3, 5])
    arr_subset = arr[mask]  # array([1, 3, 2, 4]) !!!!!flattened!!!!!

    if mode == "min":
        idx_k = np.argsort(arr_subset)[:k]  # array([0, 2, 1])
    elif mode == "max":
        idx_k = np.argsort(arr_subset)[-k:]
    else:
        raise ValueError(f"mode not understood: {mode}")

    idx_subset_k = idx_subset[idx_k]  # array([0, 3, 2])
    return idx_to_mask(idx_subset_k, arr.shape)


def median_and_empirical_ci(arr, ci=0.95, axis=0, interpolation_l="linear", interpolation_u="linear"):
    """Return the median and symmetric empirical confidence interval of an 2D array.

    Args:
        arr (numpy.array): an 2D array
        ci (float): set empirical confidence limits (default=95%)
        axis (int): which axis to be summed along
        interpolation_l (str): interpolation method for q05
        interpolation_u (str): interpolation method for q95

    Returns:
        numpy.array: lower confidence limit
        numpy.array: median
        numpy.array: upper confidence limit
    """
    cl_l = (1 - ci) / 2
    cl_u = (1 + ci) / 2
    q_l = np.quantile(arr, cl_l, axis=axis, interpolation=interpolation_l)
    med = np.median(arr, axis=axis)
    q_u = np.quantile(arr, cl_u, axis=axis, interpolation=interpolation_u)
    return q_l, med, q_u


# devel zone
if __name__ == "__main__":
    pass
