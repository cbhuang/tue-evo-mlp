"""
A framework for multi-stage training. This module attempts to mimic
the callback mechanism in standard keras training.

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

Note:
    I do think a multi-stage training framework can benefit researchers
    whos job involves implementing novel algorithms constantly. I guess
    I will disscuss with Keras developers if I have time and can make
    my idea more clear. Of course anybody capable of doing this can
    initiate the discussion.
"""

import os
import sys
from pathlib import Path
from abc import ABC, abstractmethod
import tensorflow as tf

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())
# append only if not exist
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

from .keras_model import sequential_mlp, build_optimizer
from .keras_callback import BestModelCallback
from .eng_util import print_loglike as printl, copy_array_topleft


class MultiStageBestModelContainer:
    """Retain the best model across multiple training stages (in memory).
    A component of ``BaseMultiStageTrainer``.
    If the number of each epoch is needed, ``MultiStageHistoryContainer``
    has to be used additionally.

    Note:
        The criterion of "the best model" is always ``val_loss``!
        It doesn't make sense to use ``train_loss`` (overfitting).
    """

    def __init__(self):
        """Must set which metric to be monitored."""

        #: int: which stage
        self.best_stage = 0

        #: int: which epoch
        self.best_epoch = 0

        #: float: best validation loss value
        self.best_val_loss = float('inf')

        #: list[array]: best weights
        self.best_weights = None

    def update(self, best_val_loss, best_stage, best_epoch, best_weights):
        """Update from the result of the last training stage.
        Worse results won't cause an update.

        Args:
            best_val_loss (float): metric value
            best_stage (int): which stage
            best_epoch (int): which epoch
            best_weights (list[array]): weight value
        """

        if best_val_loss < self.best_val_loss:
            self.best_val_loss = best_val_loss
            self.best_stage = best_stage
            self.best_epoch = best_epoch
            self.best_weights = best_weights

    def reset(self):
        """Reset internal variables"""
        self.best_stage = 0
        self.best_epoch = 0
        self.best_val_loss = float('inf')
        self.best_weights = None


class MultiStageHistoryContainer:
    """History of multi-stage training. A component of :class:`BaseMultiStageTrainer`."""

    def __init__(self, metric=None):
        r"""Required information for the history object.

        Args:
            metric (str): Extra metric to be reported in training history (e.g.,
            ``acc``, ``r2_metric``). Do not add the prefix "val\_".

        TODO:
            Allow multiple metrics.
        """

        #: str: name of the metric to be monitored.
        self.metric = metric

        #: int: stages
        self.stages = 0

        #: list[int]: epochs spent in each stage
        self.ls_epochs = []  # len() = stage

        #: dict: train history
        self.history = self.init_history()

    def init_history(self):
        """Initialize the keys of :attr:`history`.

        Returns:
            dict
        """
        dic = {"loss": [], "val_loss": []}

        if self.metric is not None:  # TODO: support multiple entries
            dic[f"{self.metric}"] = []
            dic[f"val_{self.metric}"] = []

        # learning rate (always made by keras)
        dic["lr"] = []

        return dic

    def update(self, history):
        """Update (concatenate) with history from a new training stage.

        Args:
            history (dict): history obtained from :class:`tf.keras.callbacks.History`
        """

        self.stages += 1

        # record number of epochs spent
        epochs = len(history["loss"])
        self.ls_epochs.append(epochs)

        # update training history
        for k, v in history.items():
            self.history[k] += v  # list concatenation

    def reset(self):
        """Reset internal states."""
        self.stages = 0
        self.ls_epochs = []
        self.history = self.init_history()


class MultiStageCallback(ABC):
    """
    Base abstract class for multistage callbacks which defines what to do
    for each stage. Its instances should be a component of
    :class:`BaseMultiStageTrainer`.

    Note:
        - Suggested name for children classes: ``XxxxYyyyMsCallback``
    """

    def __init__(self, **kwargs):

        super(MultiStageCallback, self).__init__(**kwargs)

        #: BaseMultiStageTrainer: Reference to the parent container which
        #: is set by the containing multistage trainer.
        self.trainer = None

    @property
    def model(self):
        """A shortcut to ``self.trainer.model``."""
        return self.trainer.model

    @model.setter
    def model(self, val):
        self.trainer.model = val

    @model.deleter
    def model(self):
        del self.trainer.model

    def on_multistage_begin(self):
        pass

    def on_multistage_end(self):
        pass

    def on_stage_begin(self):
        pass

    def on_stage_end(self):
        pass


class RebuildModelMsCallback(MultiStageCallback):
    """
    Abstract base class for rebuilding the model at the beginning of a
    training stage.

    TODO:
        Relax the constraint that the same hyperparameters were used in each run.
    """

    def __init__(self,
                 model_builder=tf.keras.Model.__init__,
                 build_params=None,
                 optimizer_params=None,
                 **kwargs):
        """

        Args:
            model_builder (function): a model-building function that returns a keras model
            build_params (dict): Parameters for rebuilding the model.
            optimizer_params (dict): parameters for rebuilding the optimizer
            kwargs (dict): parameters for :class:`MultiStageCallback`
        """
        super(RebuildModelMsCallback, self).__init__(**kwargs)

        #: function: a model-building function that returns a keras model
        self.model_builder = model_builder if model_builder is not None else {}

        #: dict: Parameters for rebuilding the model. ``build_params["ls_neurons"]`` must exist!
        self.build_params = build_params
        assert len(build_params["ls_neurons"]) > 0

        #: dict: parameters for rebuilding the optimizer. SAME FOR EACH STAGE.
        self.optimizer_params = optimizer_params

        #: dict: current parameters for :meth:`model_builder`.
        self.params_now = None

    def on_stage_begin(self):
        """Rebuild :attr:`model` by passing parameters into :attr:`model_builder`."""
        self.params_now = self.get_params()
        self.model = self.model_builder(**self.params_now)

    @abstractmethod
    def get_params(self):
        """The main logic for defining the parameters to be passed into
        :attr:`model_builder` for this stage. Abstract method.

        Note:
            - :attr:`trainer` is responsible for holding the model builder params!
            - :attr:`optimizer_params` is used here in order to construct the optimizer

        Returns:
            dict: :attr:`params_now`
        """
        pass


class RebuildSequentialMlpMsCallback(RebuildModelMsCallback):
    """
    Multistage callback for rebuilding the model by explicit enumeration
    of neurons and parameters for each stage.
    """

    def __init__(self, **kwargs):
        """See :class:`RebuildModelMsCallback`"""

        super(RebuildSequentialMlpMsCallback, self).__init__(model_builder=sequential_mlp, **kwargs)

        #: int: number of stages derived by the length of list
        self.n_stages = len(self.build_params["ls_neurons"])

    def get_params(self):
        """How the parameters will be passed into :attr:`model_builder` for
        each :attr:`stage_now`.

        Returns:
            dict: to be passed into :attr:`params_now`
        """
        p = self.build_params

        # enforce that every parameter was assigned explicitly
        # NOTE: deepcopy can fail because there are tensorflow objects!
        dic = {
            "input_shape": p["input_shape"],
            "out_dim": p["out_dim"],
            "ls_neurons": p["ls_neurons"][self.trainer.stage_now - 1],
            "hidden_activation": p["hidden_activation"],
            "output_activation": p["output_activation"],
            "loss": p["loss"],
            "optimizer": build_optimizer(p["optimizer"], self.optimizer_params),
            "batch_normalization": p["batch_normalization"],
            "dropout_rate": p["dropout_rate"],
            "kernel_initializer": p["kernel_initializer"],
            "bias_initializer": p["bias_initializer"],
            "metrics": p["metrics"],
            "do_compile": p["do_compile"],
        }

        # add non-default kwargs except ``neurons_by_stage``
        for k, v in p.items():
            if (k not in dic) and (k != "ls_neurons"):
                dic[k] = v

        printl(f"RebuildSequentialMlpMsCallback::get_params(): stage={self.trainer.stage_now} done!")

        return dic


class ResetOptimizerMsCallback(MultiStageCallback):
    """
    Multistage callback for resetting the model by explicit enumeration
    of neurons and parameters for each stage.
    """

    def __init__(self, optimizer_params, compile_params, **kwargs):
        """

        Args:
            optimizer_params (dict): parameters for rebuilding the optimizer.
                ``learning_rate`` must be assigned explicitly.
            compile_params (dict): parameters for recompiling the model.
                ``optimizer``, ``loss`` and ``metrics`` must be assigned explicitly.
            kwargs (dict): ABC parameters (of no use for now)
        """

        super(ResetOptimizerMsCallback, self).__init__(**kwargs)

        #: dict: parameters for rebuilding the optimizer
        self.optimizer_params = optimizer_params
        assert "learning_rate" in self.optimizer_params

        #: dict: parameters for recompiling the model
        self.compile_params = compile_params
        assert "optimizer" in self.compile_params
        assert "loss" in self.compile_params
        assert "metrics" in self.compile_params

    def on_stage_begin(self):
        """Rebuild the optimizer and then recompile the model."""

        new_compile_params = {}

        for k, v in self.compile_params.items():

            # replace optimizer with the new one
            if k == "optimizer":
                new_compile_params["optimizer"] = type(self.model.optimizer)(**self.optimizer_params)
            # copy others
            else:
                new_compile_params[k] = v

        self.model.compile(**new_compile_params)

        printl(f"ResetOptimizerMsCallback::on_stage_begin(): optimizer reset!")


class BestWeightsTransferMsCallback(MultiStageCallback):
    """
    Multistage callback for transferring best model weights between stages.

    Requirements:
        - Must be placed AFTER :class:`RebuildModelMsCallback` if it were used.
        - Must be placed BEFORE :class:`EvoMsCallback` if it were used.
    """

    def __init__(self, **kwargs):
        """See :class:`MultiStageCallback`"""
        super(BestWeightsTransferMsCallback, self).__init__(**kwargs)

    def on_stage_begin(self):
        """Transfer model weights (no biases) on stage begin."""
        # skip the first stage
        if self.trainer.stage_now == 1:
            return

        new_weights = self.model.get_weights()
        best_weights = self.trainer.best_model_container.best_weights
        n = len(new_weights)
        assert n == len(best_weights), f"Length mismatch between old and new model!"

        # copy new weights
        for i, src in enumerate(best_weights):
            copy_array_topleft(src, new_weights[i])

        self.model.set_weights(new_weights)

        printl(f"BestWeightsTransferMsCallback::on_stage_begin(): stage={self.trainer.stage_now}: Best weights copied!")


class BaseMultiStageTrainer(ABC):
    """
    An abstract organizer for multi-stage training scheme of keras model.
    """

    def __init__(self, x, y,
                 model=None,
                 metric=None,
                 fit_params=None,
                 ms_callbacks=None,
                 logs=None,
                 **kwargs):
        """Minimal common parameters required for a multi-stage training scheme.

        Args:
            x (numpy.array): training x of shape (n_records, n_input_features)
            y (numpy.array): training y of shape (n_records, n_output_features)
            model (tensorflow.keras.Model): main keras model for the first stage.
            metric (str): extra metric recorded in the history
            fit_params (dict): parameters to pass into :meth:``model.fit()``.
                The existence of a :class:`BestModelCallback` instance at
                ``fit_params["callbacks"][0]`` is enforced for the sake of
                :attr:`best_model_container` to work correctly.
            ms_callbacks (list): :class:`MultiStageCallback` instances.
            logs (dict): (of no use by now)
            **kwargs: (of no use by now)
        """

        #: tensorflow.keras.Model: A compiled model. Callbacks should be preset.
        self.model = model

        #: numpy.array: train x. Shape=(n_rec, m).
        self.x = x

        #: numpy.array: train y. Shape=(n_rec, 1).
        self.y = y

        #: str: name of the additional metric being recorded
        self.metric = metric

        #: dict: parameters for :meth:`model.fit()`
        self.fit_params = fit_params

        #: list[MultiStageCallback]: list of :class:`MultiStageCallback` for this container
        self.ms_callbacks = ms_callbacks if ms_callbacks is not None else []  # avoid None

        #: dict: (of no use currently)
        self.logs = logs

        # other properties
        for k, v in kwargs.items():
            setattr(self, k, v)

        # =====================================
        # Internal States
        # =====================================

        #: MultiStageBestModel: container of best model info
        self.best_model_container = MultiStageBestModelContainer()

        #: MultiStageHistory: container of history info
        self.history_container = MultiStageHistoryContainer(metric=self.metric)

        #: int: current stage
        self.stage_now = 0

        #: BestModelCallback: Enforce a :class:`BestModelCallback` instance on ``self.fit_params['callbacks'][0]``
        self.best_callback = fit_params["callbacks"][0]
        assert isinstance(self.best_callback, BestModelCallback)

    def train(self):
        """Master training function listing the detailed steps of each stage."""

        # setup callbacks (especially for the reference to this object)
        self.set_ms_callbacks()

        self.on_multistage_begin()  # customizable
        self.on_multistage_begin_callbacks()  # plugged in

        while self.continue_training():

            self.stage_now += 1

            # must clear the keras best model callback
            self.best_callback.reset()  # enforced

            self.on_stage_begin()  # customizable
            self.on_stage_begin_callbacks()  # plugged in

            printl(f"Training stage={self.stage_now}...")
            self.train_single_stage()  # enforced

            self.on_stage_end()  # customizable
            self.on_stage_end_update_best()  # enforced
            self.on_stage_end_update_history()  # enforced
            self.on_stage_end_callbacks()  # plugged in

        self.on_multistage_end()  # customizable
        self.on_multistage_end_callbacks()  # plugged in

    @abstractmethod
    def train_single_stage(self):
        """Training a single stage."""
        pass

    @abstractmethod
    def continue_training(self):
        """Condition to continue training

        Returns:
            bool
        """
        pass

    def on_multistage_begin(self, logs=None):
        """Customizable setup before the first stage.

        Typical things to do:
            - Reset model
        """
        pass

    def on_multistage_begin_callbacks(self, logs=None):
        """Execute :meth:`on_multistage_begin()` for each :meth:`multistage_callback`."""
        for callback in self.ms_callbacks:
            callback.on_multistage_begin()

    def on_multistage_end(self, logs=None):
        """Things to postprocess after the last stage.

        Typical things to do:
            - Restore the best model "of all stages"
        """
        pass

    def on_stage_end_update_best(self, logs=None):
        """Update best model from ``self.best_callbacks`` into
        ``self.best_model_container``"""

        self.best_model_container.update(
            self.best_callback.best_val_loss,
            self.stage_now,
            self.best_callback.best_epoch,
            self.best_callback.best_weights
        )

    def on_stage_end_update_history(self, logs=None):
        """Update history from ``self.model.history.history``.

         Note:
             No need to execute on ``on_stage_begin()`` because the entire
             training stages were ended. Hence the history is updated properly.
        """
        self.history_container.update(self.model.history.history)

    def on_multistage_end_callbacks(self, logs=None):
        """Execute ``on_multistage_end()`` for each ``multistage_callback``."""
        for callback in self.ms_callbacks:
            callback.on_multistage_end()

    def on_stage_begin(self, logs=None):
        """Things to setup for every stage, right before the
        ``on_train_begin()`` actions of regular keras callbacks.
        """
        pass

    def on_stage_begin_callbacks(self, logs=None):
        """Execute ``on_stage_begin()`` for each ``multistage_callback``."""
        for callback in self.ms_callbacks:
            callback.on_stage_begin()

    def on_stage_end(self, logs=None):
        """Things to postprocess for every stage, right after the
        ``on_train_end()`` actions of regular keras callbacks."""
        pass

    def on_stage_end_callbacks(self, logs=None):
        """Execute ``on_stage_end()`` for each ``multistage_callback``."""
        for callback in self.ms_callbacks:
            callback.on_stage_end()

    def reset(self):
        """Clear intermediate states. Parameters were kept."""
        self.best_model_container.reset()
        self.history_container.reset()
        self.stage_now = 0

    def set_ms_callbacks(self):
        """Pass trainer reference to multistage callbacks."""
        if self.ms_callbacks is not None:
            for callback in self.ms_callbacks:
                callback.trainer = self


class FixedMsTrainer(BaseMultiStageTrainer):
    """
    Perform multistage training with a fixed number of stages ``n_stages``.
    The best model is automatically restored.

    TODO: (future) multistage trainer with indefinite n_stages.
    """

    def __init__(self, x, y, n_stages, **kwargs):
        """See ``BaseMultiStageTrainer``."""

        super(FixedMsTrainer, self).__init__(x, y, **kwargs)

        #: int: number of train stages
        self.n_stages = n_stages

    def continue_training(self):
        """When ``self.n_stages`` is not reached."""
        if self.stage_now < self.n_stages:
            return True
        else:
            return False

    def train_single_stage(self):
        """Just an ordinary round of training."""
        self.model.fit(
            self.x, self.y,
            validation_split=self.fit_params["validation_split"],
            batch_size=self.fit_params["batch_size"],
            epochs=self.fit_params["epochs"],
            callbacks=self.fit_params["callbacks"]
        )

    def on_multistage_end(self, logs=None):

        self.restore_best_model()
        printl("FixedMsTrainer::on_multistage_end() done!")

    def restore_best_model(self):
        """Restore the best model if applicable."""

        # if ms_callbacks doesn't exist -> do nothing
        if len(self.ms_callbacks) == 0:
            return

        # update best stage
        best_stage = self.best_model_container.best_stage
        best_weights = self.best_model_container.best_weights

        # Must rebuild the model if
        #     (1) the best stage is not the last stage
        #     (2) a RebuildModelMsCallback is required.
        ms_rebuilder = self.find_rebuild_model_ms_callback()
        if (ms_rebuilder is not None) and (best_stage != self.stage_now):
            build_params = ms_rebuilder.build_params.copy()
            build_params["ls_neurons"] = build_params["ls_neurons"][best_stage - 1]
            self.model = ms_rebuilder.model_builder(**build_params)

        # restore weights
        self.model.set_weights(best_weights)

    def find_rebuild_model_ms_callback(self):
        """Get the ``RebuildModelMsCallback`` if it were used"""

        for mscb in self.ms_callbacks:
            if isinstance(mscb, RebuildModelMsCallback):
                return mscb
        else:
            return None
