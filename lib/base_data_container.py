"""
Base containers for data engineering with handy methods.

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

"""
from copy import deepcopy
import pickle
from pathlib import Path
from datetime import datetime
import time
import numpy as np


class BaseDataContainer:
    """Base container for data cleansing with handy I/O and in-memory copying utilities.
    Should be able to deal with special objects such as

    - ``tf.keras.model`` which cannot be pickled directly.
    - ``pathlib.Path`` which produces problem in cross-platform migration.

    Usage:
        .. code-block:: python

            # initialize
            from lib.base_data_container import BaseDataContainer
            o = BaseDataContainer()

            # Workflow
            o.s01_xxx_xx()
            o.sNN_xxx_xx()
            # ...
    """

    # ===========================================
    # Pickle, S/L and Deepcopy Settings
    # ===========================================
    # Add gradually upon implementation!

    #: tuple[str]: name of Path attributes to be converted upon (un)pickling.
    #: Must be dealt with when migrating across machines
    _path_attrs = ()

    #: tuple[str]: name of attributes to not be pickled.
    _no_pickle_attrs = ()

    #: tuple[type]: attribute types to not be pickled.
    _no_pickle_types = ()  # e.g. History, Model, Callables, ...

    def __init__(self, **kwargs):
        """Set commonly used info."""

        # ===========================================
        # Timestamps
        # ===========================================

        #: int: timestamp in nanoseconds
        self.timestamp_ns = time.time_ns()

        #: int: timestamp in seconds
        self.timestamp = int(self.timestamp_ns / 1e9)

        #: str: timestamp suffix of this container (use for subdirectory)
        self.suffix_ts = self.timestamp_to_suffix(self.timestamp)

    # ===========================================
    # I/O
    # ===========================================

    def __getstate__(self):
        """Exclude or process special attributes upon ``save_pickle()``.

        Examples of special attributes:
            - Path: can be pickled but fails in cross-platform reload
            - Model: cannot be pickled
            - History: cannot be pickled
            - database drivers and artifacts, ...
        """

        dic = {}
        for k, v in self.__dict__.items():

            # bypass no-pickle attribute names
            if k in self._no_pickle_attrs:
                pass

            # bypass no-pickle attribute types
            elif isinstance(v, self._no_pickle_types):
                pass

            # Convert Paths to string
            elif isinstance(v, Path):
                dic[k] = str(v)

            # default behavior = simple load
            else:
                dic[k] = v

        return dic

    def __setstate__(self, state):
        """Exclude or process special attributes upon ``load_pickle()``."""

        # convert path strings only if not None
        for k in self._path_attrs:
            if state[k] is not None:
                state[k] = Path(state[k])

        # set debug flag
        state["_dbg_from_setstate"] = True

        # apply changes
        self.__dict__.update(state)

    def save_pickle(self, fname, save_non_picklable=False, **kwargs):
        """Pickle the picklable members of the instance.

        Args:
            fname (PathLike): path to the output file
            save_non_picklable (bool): also save non-picklable objects
            kwargs (dict): save_non_picklable parameters
        """
        with open(fname, 'wb') as f:
            pickle.dump(self, f)

        if save_non_picklable:
            self.save_non_picklable(**kwargs)

    def save_non_picklable(self, **kwargs):
        """Save nonpicklable attributes. For inheritance."""
        pass

    @classmethod
    def load_pickle(cls, fname, load_non_picklable=False, **kwargs):
        """Creating an instance Load pickable attributes.

        Args:
            fname (PathLike): path to the input file
            load_non_picklable (bool): also load non-picklable objects
            kwargs (dict): parameters for load_non_picklable

        Returns:
            BaseDataContainer
        """
        with open(fname, 'rb') as f:
            o = pickle.load(f)

        if load_non_picklable:
            o = cls.load_non_picklable(o, **kwargs)

        return o

    @classmethod
    def load_non_picklable(cls, o, **kwargs):
        """Load nonpicklable attributes into an existing object. For inheritance.

        Args:
            o (BaseDataContainer): existing instance.
            kwargs (dict): loader parameters

        Returns:
            BaseDataContainer
        """
        # do many of these:
        # setattr(o, "attr_name", attr_val)
        return o

    # ==============================
    # In-memory Backup/Restore
    # ==============================

    def copy_from(self, source):
        """(Shallow) copy all attributes from instance b.

        Args:
            source (object): copy source

        Usage:
            Analogous to :meth:`deepcopy_from`
        """
        for k, v in source.__dict__.items():
            setattr(self, k, v)

    def deepcopy_from(self, source):
        """Deepcopy all attributes from another instance based on ``__dict__``.
        Class methods and un-pickleable attributes won't be copied!

        Args:
            source (object): copy source

        Usage:
            This example demonstrates how this method is usually used during
            development in PyCharm.

            .. code-block:: Python

                # o is what we originally have.

                # make a backup
                o2 = BaseDataContainer(...)  # initialize
                o2.deepcopy_from(o)  # deepcopy from o
                # alternatively:
                # o.deepcopy_to(o2)

                # reinstantiate
                o = BaseDataContainer(...)  # new updated settings

                # restore the backup
                o.deepcopy_from(o2)
                # alternatively:
                # o2.deepcopy_to(o)
        """
        for k, v in source.__dict__.items():

            # bypass no-pickle attribute names
            if k in self._no_pickle_attrs:
                pass

            # bypass no-pickle attribute types
            elif isinstance(v, self._no_pickle_types):
                pass

            # bypass nested container (must decide how to process manually)
            elif isinstance(v, BaseDataContainer):
                # TODO: if there is a list of BaseDataContainer....
                pass

            # default beahvior
            else:
                setattr(self, k, deepcopy(v))

    def copy_to(self, target):
        """(Shallow) copy all attributes of this object to object b

        Args:
            target (object): destination of copy

        Usage:
            Analogous to :meth:`deepcopy_from`
        """

        for k, v in self.__dict__.items():
            setattr(target, k, v)

    def deepcopy_to(self, target):
        """Deepcopy all attributes to another instance based on ``__dict__``.
        Class methods and un-pickleable attributes won't be copied!

        Args:
            target (object): destination of copy

        Usage:
            Analogous to :meth:`deepcopy_from`
        """
        for k, v in self.__dict__.items():

            # bypass no-pickle attribute names
            if k in self._no_pickle_attrs:
                pass

            # bypass no-pickle attribute types
            elif isinstance(v, self._no_pickle_types):
                pass

            # bypass nested container (must decide how to process manually)
            elif isinstance(v, BaseDataContainer):
                # TODO: if there is a list of BaseDataContainer....
                pass

            # default behavior
            else:
                setattr(target, k, deepcopy(v))

    # ==============================
    # General Preprocessing Tasks
    # ==============================

    def impute_x(self, x):
        """Impute missing values for x.

        Args:
            x (numpy.array)

        Return:
            numpy.array
        """
        pass

    def impute_y(self, y):
        """Impute missing values for y.

        Args:
            y (numpy.array)

        Return:
            numpy.array
        """
        pass

    def transform_x(self, x):
        """x-transformer logic for the aggregation class to use.

        Args:
            x_raw (numpy.array)

        Return:
            numpy.array: x_tr
        """
        pass

    def transform_y(self, y):
        """y-transformer logic for the aggregation class to use.

        Args:
            y_raw (numpy.array)

        Return:
            numpy.array: y_tr
        """
        pass

    def inverse_transform_x(self, x):
        """x-inverse-transformer logic for the aggregation class to use.

        Args:
            x (numpy.array): x_tr

        Return:
            numpy.array: x_raw
        """
        pass

    def inverse_transform_y(self, y):
        """y-inverse-transformer logic for the aggregation class to use.

        Args:
            y (numpy.array): y_tr

        Return:
            numpy.array: y_raw
        """
        pass

    # ==============================
    # Utility Functions
    # ==============================

    @staticmethod
    def timestamp_to_suffix(timestamp):
        """Convert a timestamp to a human-readable suffix (``yyyymmdd_hhmmss``).

        Args:
            timestamp (float): timestamp in seconds

        Returns:
            str

        Note:
            If sub-second precision is needed (e.g. parallel computation),
            one must use nanosecond timestamps and implement time collision
            detection elsewhere.
        """
        return datetime.fromtimestamp(timestamp).strftime("%Y%m%d_%H%M%S")


class BaseAggrContainer(BaseDataContainer):
    """Base container class for aggregation tasks. The container
    itself inherits all the functionalities from ``BaseDataContainer``
    AND it contains an ``BaseDataContainer`` member instance.
    """

    # ===============================
    # Inherited I/O Artifacts
    # ===============================
    # _path_attrs
    # _no_pickle_attrs
    # _no_pickle_types

    # ===============================
    # Statistics To Be Reported
    # ===============================

    #: tuple[str]: stats to be reported for both numerical and categorical variables
    _common_stats = ("n", "n_miss", "n_miss_rate")
    #: tuple[type]: dtypes for ``_common_stats``
    _common_stats_dtypes = (int, int, float)

    #: tuple[str]: stats to be reported for numerical variables
    _num_stats = ("avg", "std", "min", "q025", "q05", "q10", "q1", "median", "q3", "q90", "q95", "q975", "max")
    #: tuple[type]: dtypes for ``_num_stats``
    _num_stats_dtypes = (float, float, float, float, float, float, float, float, float, float, float, float, float)

    #: tuple[str]: stats to be reported for categorial variables
    _cat_stats = ("n_classes", "modes", "freq")
    #: tuple[type]: dtypes for ``_cat_stats``
    _cat_stats_dtypes = (int, tuple, dict)

    def __init__(self, dc, q_interpolation="lower", **kwargs):

        super(BaseAggrContainer, self).__init__(**kwargs)

        #: BaseDataContainer: the data container object to be analyzed
        #: TODO: multiple data containers!
        self.dc = dc

        #: str: quantile interpolation method
        self.q_interpolation = q_interpolation

    # =======================================
    # Common Utility Functions for Stats
    # =======================================

    def create_stats_columns(self, df, do_num=False, do_cat=False):
        """Create stats columns for a ``DataFrame`` and initialize.

        Args:
            df (DataFrame): Stats DataFrame with indexes initalized
            do_num (bool): create numerical columns
            do_cat (bool): create categorical columns
        """

        n = len(df)

        # 1/3. common columns
        for i, col in enumerate(self._common_stats):
            dtype = self._common_stats_dtypes[i]
            df[col] = self.create_column_by_dtype(n, dtype=dtype)

        # 2/3. stats columns for num vars
        if do_num:
            for i, col in enumerate(self._num_stats):
                dtype = self._num_stats_dtypes[i]
                df[col] = self.create_column_by_dtype(n, dtype=dtype)

        # 3/3. stats columns for cat vars
        if do_cat:
            for i, col in enumerate(self._cat_stats):
                dtype = self._cat_stats_dtypes[i]
                df[col] = self.create_column_by_dtype(n, dtype=dtype)

    def create_column_by_dtype(self, n, dtype=object):
        """Initialize a column according to the given dtype.
        Can be overloaded so not necessarily static.

        Args:
            n (int): column size
            dtype (type): data type

        Returns:
            np.array: an 1-D array
        """

        # floats by nan
        if dtype in (float, np.float64, np.float32, np.float16):
            return np.full(n, np.nan, dtype=dtype)

        # integers by zero
        elif dtype in (int, np.int8, np.int16, np.int32, np.int64, np.uint8,
                       np.uint16, np.uint32, np.uint64):
            return np.full(n, 0, dtype=dtype)

        # bools by False
        elif dtype in (bool, np.bool_):
            return np.full(n, False, dtype=dtype)

        # objects by None
        else:
            return np.empty(n, dtype=object)

    def compute_stats(self, df_out, idx, arr, do_num=False, do_cat=False):
        """Compute statistics of arr into target DataFrame.

        Args:
            df_out (pandas.DataFrame): output df_out with columns prepared
            idx (tuple): which row of the array
            arr (numpy.array): value array
            do_num (bool): compute numerical stats
            do_cat (bool): compute categorical stats
        """

        # common stats of both categorical and numerical data
        self.compute_common_stats(df_out, idx, arr)

        if do_num:
            self.compute_num_stats(df_out, idx, arr)

        if do_cat:
            self.compute_cat_stats(df_out, idx, arr)

    def compute_common_stats(self, df_out, idx, arr):
        """Compute stats of numerical vars into target DataFrame.

        Args:
            df_out (pandas.DataFrame): output df_out with columns prepared
            idx (tuple): which row of the array
            arr (numpy.array): value array
        """

        # NOTE: mind n = 0!
        n = len(arr)
        n_miss = self.count_n_miss(arr) if n > 0 else 0
        n_miss_rate = n_miss / n if n > 0 else np.nan

        # Cite ``_common_stats`` deliberately to detect inconsistency
        df_out.loc[idx, self._common_stats] = [n, n_miss, n_miss_rate]

    def count_n_miss(self, arr):
        """Define what counts as "miss". Can be overloaded so
        not necessarily static.

        Args:
            arr (array|tuple|list): data

        Returns:
            int
        """
        return len(arr) - np.isfinite(arr).sum() if len(arr) > 0 else 0

    def compute_num_stats(self, df_out, idx, arr):
        """Compute stats of numerical vars into target DataFrame.

        Args:
            df_out (pandas.DataFrame): output df_out with columns prepared
            idx (tuple): which row of the array
            arr (numpy.array): value array
        """
        n = len(arr)

        avg = np.mean(arr) if n else np.nan
        std = np.std(arr) if n else np.nan
        min_ = np.min(arr) if n else np.nan
        q025 = np.quantile(arr, 0.025, interpolation=self.q_interpolation) if n else np.nan
        q05 = np.quantile(arr, 0.05, interpolation=self.q_interpolation) if n else np.nan
        q10 = np.quantile(arr, 0.1, interpolation=self.q_interpolation) if n else np.nan
        q1 = np.quantile(arr, 0.25, interpolation=self.q_interpolation) if n else np.nan
        median = np.quantile(arr, 0.5, interpolation=self.q_interpolation) if n else np.nan
        q3 = np.quantile(arr, 0.75, interpolation=self.q_interpolation) if n else np.nan
        q90 = np.quantile(arr, 0.9, interpolation=self.q_interpolation) if n else np.nan
        q95 = np.quantile(arr, 0.95, interpolation=self.q_interpolation) if n else np.nan
        q975 = np.quantile(arr, 0.975, interpolation=self.q_interpolation) if n else np.nan
        max_ = np.max(arr) if n else np.nan

        df_out.loc[idx, self._num_stats] = [
            avg, std, min_, q025, q05, q10, q1, median, q3, q90, q95, q975, max_
        ]

    def compute_cat_stats(self, df_out, idx, arr):
        """Compute stats of categorical vars into target DataFrame.

        Args:
            df_out (pandas.DataFrame): output df_out with columns prepared
            idx (tuple): which row of the array
            arr (numpy.array): value array (may be number labels)
        """
        n = len(arr)

        if n > 0:
            unique_vals, counts = np.unique(arr, return_counts=True)
            n_classes = unique_vals.size
            modes = self.multi_modes(unique_vals, counts)
            freq = list(zip(unique_vals, counts))
        else:
            n_classes = 0
            modes = None
            freq = None

        df_out.loc[idx, self._cat_stats] = [n_classes, modes, freq]

    def multi_modes(self, unique_vals, counts):
        """Return a tuple of ret value(s).

        Args:
            unique_vals (list|tuple|array): unique values
            counts (list|tuple|array): count of unique_vals

        Returns:
            tuple
        """
        m = max(counts)
        return tuple(unique_vals[counts == m])
