"""
Customized keras callback functions.

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

Important:
    For multistage callbacks, plese refer to :mod:`.keras_multistage`.
"""

from tensorflow.keras.callbacks import Callback
import numpy as np
from copy import deepcopy

from .keras_model import get_dense_layer_names


class BestModelCallback(Callback):
    """Retain the best model in memory and restore at the end of
    training. "Best" means minimal ``val_loss``.

    Note:
        - Better put at the FIRST element of ``callbacks=`` because
          the retainment has to be performed on epoch begin.
        - If saving to a file is preferred, please use
          :class:`tensorflow.keras.callbacks.ModelCheckpoint` instead.
        - EarlyStopping has a bug (`Issue #12511 <https://github.com/keras-team/keras/issues/12511>`_) so it is unreliable.
    """

    def __init__(self):

        super(BestModelCallback, self).__init__()

        # ================================
        # Best Result Retained
        # ================================

        #: float: best metric value
        self.best_val_loss = float('inf')

        #: list[array]: best model weights from ``model.get_weights()``
        self.best_weights = None

        #: int: best epoch since training begins
        self.best_epoch = 0

        # ================================
        # internal variables
        # ================================

        #: int: current epoch
        self.epoch_now = 0

        #: float: current metric value
        self.val_loss_now = self.best_val_loss

    def on_epoch_begin(self, epoch, logs=None):

        # print(f"\n_docs[Debug] begin of epoch {self.epoch_now + 1} = end of epoch {self.epoch_now}")

        if self.epoch_now > 0:  # do nothing in the first round
            self.retain_best_model()
        # ============== the real end of the last epoch ==================
        self.epoch_now += 1

    def on_train_begin(self, logs=None):
        pass

    def on_train_end(self, logs=None):

        # print(f"\n_docs[Debug] on_train_end: epoch {self.epoch_now}!")

        # retain the best model for the last epoch
        self.retain_best_model()

        # restore the best model
        # guard against non-updating cases (val_loss == nan)
        if self.best_weights is not None:
            self.model.set_weights(self.best_weights)

    def retain_best_model(self):
        """Retain the best model ON THE BEGINNING of each epoch.

        Important:
            Conceptually it should be applied at the end of each epoch.
            The problem is that the history won't be ready by then, making
            the loss function value unavailable for selection.
        """

        # get current metric value
        self.val_loss_now = self.model.history.history["val_loss"][-1]

        # update if improved
        if self.is_improved():
            self.update_best()

    def is_improved(self):
        """If the metric concerned is improved.

        Returns:
             bool
        """
        return True if self.val_loss_now < self.best_val_loss else False

    def update_best(self):
        """Update the best record."""
        self.best_val_loss = self.val_loss_now
        self.best_weights = self.model.get_weights()
        self.best_epoch = self.epoch_now
        # print(f"[Info] Best model updated: epoch={self.best_epoch}, {self.metric}={self.best_metric_val:.3e}")

    def reset(self):
        """Reset intermediate states. Mode and metric were kept.

        Important:
            This is not automatically called in :meth:`on_train_begin` because
            one may want the option to keep the previous results.
        """

        # retained results
        self.best_val_loss = float('inf')
        self.best_weights = None
        self.best_epoch = 0

        # initialize internal variables
        self.epoch_now = 0
        self.val_loss_now = self.best_val_loss


class PrintCallback(Callback):
    """Just to print the selected information."""

    def __init__(self, dic=None):

        super(PrintCallback, self).__init__()

        #: dict: what to be inspected
        self.dic = dic if dic is not None else {}

        #: str: instance name
        self.name = self.dic["name"] if "name" in self.dic else "PrintCallback"

    def on_epoch_begin(self, epoch, logs=None):
        print(f"{self.name}::on_epoch_begin(), ", end="")
        self.print_dic(epoch)

    def on_train_begin(self, logs=None):
        print(f"{self.name}::on_train_begin()...")
        self.print_dic()

    def on_epoch_end(self, epoch, logs=None):
        print(f"{self.name}::on_epoch_end(), ", end="")
        self.print_dic(epoch)

    def on_train_end(self, logs=None):
        print(f"{self.name}::on_train_end()...")
        self.print_dic()

    def print_dic(self, epoch=None):
        """Master print function."""
        self.print_epoch(epoch)
        self.print_validation_loss()
        # self.print_evaluated_loss()
        self.print_nonzero_neurons()

    @staticmethod
    def print_epoch(epoch=None):
        if epoch is not None:
            print(f"epoch(1-based)={epoch + 1}")

    def print_validation_loss(self):
        if "val_loss" in self.model.history.history:
            val_loss_last = self.model.history.history["val_loss"][-1]
            print(f"  Last val_loss in history={val_loss_last:.4f}")
        else:
            print(f"  Last val_loss was not found in model.history!")

    def print_evaluated_loss(self):
        """Print train_loss and val_loss(es)

        Caution:
            This will clear ``model.history``! WTH.
        """

        if ("x_val" in self.dic) and ("y_val" in self.dic):
            # save
            hist = deepcopy(self.model.history.history)

            ls = self.model.evaluate(self.dic["x_val"], self.dic["y_val"], verbose=0)
            print(f"  Eval result: {ls}")
            # restore
            self.model.history.history = hist  # restore history

    def print_nonzero_neurons(self):
        print(f"  #Nonzero-neurons: Layer ", end="")
        ls_dense_names = get_dense_layer_names(self.model)
        for i in range(len(ls_dense_names)):
            wt = self.model.get_layer(f"dense_{i+1}").get_weights()
            print(f"{i+1}={np.count_nonzero(wt[0])}, ", end="")
        else:
            print()
