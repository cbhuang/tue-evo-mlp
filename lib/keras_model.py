"""
Helper functions related to keras model.

:Author: Chuan-Bin "Bill" Huang
:Date: 2020-07-07

"""
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras import layers, Input, optimizers, Model


def peak_1d_exact():
    """An exact minimal MLP model for the peak-1d synthesized dataset.

    Returns:
        tensorflow.keras.model.Sequential: a compiled model
    """
    # skeleton
    model = Sequential()
    model.add(layers.Dense(3, activation="relu", input_dim=1))
    model.add(layers.Dense(1, name="out"))
    model.compile(loss="mse")

    # assign coefficients
    model.set_weights([
        np.array([[1.0, 1.0, 1.0]], dtype=np.float32),  # hidden weight
        np.array([1.0, 0.0, -1.0], dtype=np.float32),  # hidden bias
        np.array([[1.0], [-2.0], [1.0]], dtype=np.float32),  # output weight
        np.array([0.0], dtype=np.float32)  # output bias
    ])

    return model


def add_activation_by_str(model, activation_name, layer_name=None, **kwargs):
    """Add activation layer to a model by name string.

    Args:
        model (tensorflow.keras.Model)
        activation_name (str): lower-case string
        layer_name (str): name of the layer. None = auto.
        **kwargs: activation-specific parameters (e.g. "name")
    """
    if activation_name in ("linear", "", None):
        pass

    elif activation_name in ("relu", "elu", "selu", "softmax", "softsign",
                             "tanh", "sigmoid", "softplus", "exponential"):
        model.add(layers.Activation(activation_name, name=layer_name))

    elif activation_name == "prelu":
        model.add(layers.PReLU(name=layer_name))

    elif activation_name == "leakyrelu":
        if "leakyrelu_alpha" in kwargs:
            model.add(layers.LeakyReLU(alpha=kwargs["leakyrelu_alpha"], name=layer_name))
        else:  # use default value (0.3, seems large)
            model.add(layers.LeakyReLU(name=layer_name))
    else:
        raise NotImplementedError(f"Activation_name not understood: {activation_name}")


def sequential_mlp(input_shape, out_dim, ls_neurons, hidden_activation,
                   output_activation, loss, optimizer="adam",
                   batch_normalization=True, dropout_rate=0.20,
                   kernel_initializer="glorot_uniform",
                   bias_initializer="zeros",
                   metrics=None, do_compile=True, **kwargs):
    """Automate building a sequential multilayer perceptron (MLP) model.

    Args:
        input_shape (tuple): input dimension
        out_dim (int): output dimension
        ls_neurons (list): number of neurons in the hidden layers
        hidden_activation (str | list): activation function in the hidden
            layers. Accepts list to set individual layers.
        output_activation (str): activation function for the last layer
        loss (str | Loss): loss function
        optimizer (str | Optimizer): optimizer
        batch_normalization (bool): apply BatchNormalization between dense and activation layers,
            EXCEPT for the output layer.
        dropout_rate (float): dropout rate. (0 = no dropout layer)
        kernel_initializer (str | Initializer): kernel_initializer parameter
        bias_initializer (str | Initializer): bias_initializer parameter
        metrics (list): list of metric functions
        do_compile (bool): compile the model or not
        kwargs (dict): activation or initialization parameters

    Returns:
        tensorflow.keras.model.Sequential: a compiled model

    TODO:
        support passing activation instances
    """

    # number of hidden layers
    n_layers = len(ls_neurons)

    # set list of hidden activation functions
    if isinstance(hidden_activation, list):
        ls_activations = hidden_activation
        assert n_layers == len(hidden_activation), f"len(hidden_activation)={len(hidden_activation)} != n_layers={n_layers}."
    else:
        ls_activations = [hidden_activation for _ in range(n_layers)]

    # (1) The input layer (invisible in model.summary())
    model = Sequential()
    model.add(Input(shape=input_shape, name="input"))

    # (2) The hidden layers
    for i in range(n_layers):

        model.add(layers.Dense(
            ls_neurons[i],
            kernel_initializer=kernel_initializer,
            bias_initializer=bias_initializer,
            name=f"dense_{i+1}"
        ))

        # BN between Dense and Activation as suggested in the original paper
        if batch_normalization:
            model.add(layers.BatchNormalization(name=f"bn_{i+1}"))

        add_activation_by_str(model, ls_activations[i], layer_name=f"activation_{i+1}", **kwargs)

        if dropout_rate > 0:
            model.add(layers.Dropout(dropout_rate, name=f"dropout_{i+1}"))

    # (3) The output layer (no batch normalization)
    model.add(layers.Dense(out_dim, name=f"dense_{n_layers+1}"))
    add_activation_by_str(model, output_activation, layer_name=f"activation_{n_layers+1}", **kwargs)

    # If use as a model-building function, must compile before return!
    if do_compile:
        model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    return model


def build_optimizer(opt, opt_params=None):
    """Build optimizer by optimizer instance or name, with its params.

    Args:
        opt (str|Optimizer): optimizer name or instance
        opt_params (dict): optimizer parameters

    Returns:
        Optimizer
    """
    if opt_params is None:
        opt_params = {}

    if (opt == "adam") or isinstance(opt, optimizers.Adam):
        return optimizers.Adam(**opt_params)
    elif (opt == "rmsprop") or isinstance(opt, optimizers.RMSprop):
        return optimizers.RMSprop(**opt_params)
    elif (opt == "sgd") or isinstance(opt, optimizers.SGD):
        return optimizers.SGD(**opt_params)
    elif (opt == "adadelta") or isinstance(opt, optimizers.Adadelta):
        return optimizers.Adadelta(**opt_params)
    elif (opt == "adagrad") or isinstance(opt, optimizers.Adagrad):
        return optimizers.Adagrad(**opt_params)
    else:
        raise ValueError(f"Optimizer not understood: {opt}")


def get_dense_layer_names(model, dense_i=True):
    """Get the dense layers in a multilayer perceptron network.

    Args:
        model (tensorflow.keras.Model): an MLP model
        dense_i (bool): check if the names of the layers are "dense_i".

    Returns:
        list[str]: list of layer names
    """

    # get layer names
    ans = []
    for layer in model.layers:
        if isinstance(layer, layers.Dense):
            ans.append(layer.name)

    # check layer names and ordering
    if dense_i:
        for i in range(1, 1+len(ans)):
            assert ans[i-1] == f"dense_{i}"  # ordering
            assert model.get_layer(f"dense_{i}") is not None

    return ans


def get_layer_n_links(model, layer_name):
    """Get #edges in a dense layer.

    Args:
        model (Model): the model
        layer_name (str): name of the dense layer

    Returns:
        int
    """
    layer = model.get_layer(layer_name)
    return np.prod(layer.input_shape[1:]) * np.prod(layer.output_shape[1:])


def get_layer_shape(model, layer_name):
    """Get the shape of a dense layer

    Args:
        model (Model): the model
        layer_name (str): name of the dense layer

    Returns:
        int, int: input_shape, output_shape
    """
    layer = model.get_layer(layer_name)
    return layer.input_shape[1], layer.output_shape[1]


# quick test block
if __name__ == "__main__":
    from datetime import datetime
    t0 = datetime.now()
    model = sequential_mlp(
        (768,), 10, [128, 32, 32, 32, 32, 32, 32, 32, 32, 32],
        ["relu", "elu", "selu", "softsign", "tanh", "sigmoid", "softplus", "exponential", "prelu", "leakyrelu"],
        "softmax", "sparse_categorical_crossentropy"
    )
    #model.summary()
    print(f"Time elapsed: {(datetime.now() - t0).total_seconds()*1000:.3f}ms")

    ls = get_dense_layer_names(model, dense_i=True)
    assert ls == ['dense_1', 'dense_2', 'dense_3', 'dense_4', 'dense_5', 'dense_6', 'dense_7', 'dense_8', 'dense_9', 'dense_10', 'dense_11']
