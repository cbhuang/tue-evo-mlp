"""
Tests for evolutionary algorithms.
"""
from pathlib import Path
import sys
from pytest import fixture
from tensorflow.keras.datasets import mnist

# locate project base
base_dir = Path(__file__).absolute().parent.parent
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

from src.data_synthesis import data_zigzag_1d


@fixture(scope="session")
def data_zigzag_1000():
    """The Zigzag dataset

    Returns:
        numpy.array: x
        numpy.array: y
    """
    return data_zigzag_1d(1000)


@fixture(scope="session")
def data_mnist_1000():
    """The Zigzag dataset

    Returns:
        numpy.array: x
        numpy.array: y
    """
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x = x_train.reshape((-1, 28*28))[:1000, :]
    y = y_train.reshape((-1, 1))[:1000, :]
    return x, y


@fixture(scope="session")
def data_mnist_10000():
    """The Zigzag dataset

    Returns:
        numpy.array: x
        numpy.array: y
    """
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x = x_train.reshape((-1, 28*28))[:10000, :]
    y = y_train.reshape((-1, 1))[:10000, :]
    return x, y
