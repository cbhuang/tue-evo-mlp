"""
Test ``evo_multistage`` and ``evo_callback``
"""

import os
import sys
from pathlib import Path
from tensorflow.keras import Model
from tensorflow.keras.callbacks import ReduceLROnPlateau
from tensorflow.keras.optimizers import Adam
#from tensorflow.keras.datasets import mnist
import numpy as np

# locate project base
if "__file__" in globals():
    base_dir = Path(__file__).absolute().parent.parent
else:
    base_dir = Path(os.getcwd())
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

from lib.keras_model import sequential_mlp
from lib.keras_callback import BestModelCallback
from lib.keras_multistage import RebuildSequentialMlpMsCallback, FixedMsTrainer, BestWeightsTransferMsCallback
from lib.math_util import r2_metric, count_through_list
from src.evo_callback import NeuronMaskingCallback, LinkMaskingCallback
from src.evo_multistage import NiByMaskingTrainer, EvoMsCallback, RleMsCallback
from src.data_synthesis import data_zigzag_1d


def test_rebuild_sequential_mlp_ms_callback_f(data_zigzag_1000):
    """Test best model storage & masking callback
    on a function-fitting problem."""

    # data
    x, y = data_zigzag_1000
    #x, y = data_zigzag_1d(1000)

    for _ in range(10):

        # keras callbacks
        callbacks = [
            BestModelCallback(),  # enforced!
            ReduceLROnPlateau(patience=1, factor=0.99, min_lr=0.1)  # use a bad one
        ]

        # multistage callbacks
        build_params = {
            "input_shape": (1,),
            "out_dim": 1,
            "ls_neurons": [
                [32, 8],
                [64, 16],
                [128, 32]
            ],
            "hidden_activation": ["relu", "prelu"],
            "output_activation": "linear",
            "loss": "mse",
            "optimizer": Adam(learning_rate=1.0),  # use a bad one
            "batch_normalization": True,
            "dropout_rate": 0,
            "kernel_initializer": "glorot_uniform",
            "bias_initializer": "zeros",
            "metrics": r2_metric,
            "do_compile": True
        }

        # the best model and the history are automatically retained
        ms_callbacks = [
            RebuildSequentialMlpMsCallback(build_params=build_params),
            BestWeightsTransferMsCallback()
        ]

        # fit params
        fit_params = {
            "validation_split": 0.2,
            "batch_size": 32,
            "epochs": 3,
            "callbacks": callbacks
        }

        # Setup the Listed Multistage Trainer

        n_stages = len(build_params["ls_neurons"])  # 3

        o = FixedMsTrainer(
            x, y, n_stages, model=None,
            metric="r2_metric",
            fit_params=fit_params,
            ms_callbacks=ms_callbacks
        )

        # ====== initial status ======

        assert o.model is None

        # best model
        assert o.best_model_container.best_epoch == 0
        assert o.best_model_container.best_stage == 0
        assert o.best_model_container.best_val_loss == float('inf')
        assert o.best_model_container.best_weights is None

        # multistage history
        assert o.history_container.history["loss"] == []
        assert o.history_container.history["val_loss"] == []
        assert o.history_container.history["r2_metric"] == []
        assert o.history_container.history["val_r2_metric"] == []
        assert o.history_container.history["lr"] == []
        assert o.history_container.ls_epochs == []
        assert o.history_container.metric == "r2_metric"
        assert o.history_container.stages == 0

        # the trainer is uninitialized for now
        assert o.ms_callbacks[0].trainer is None  # is o later
        assert o.ms_callbacks[0].n_stages == 3

        # =============== trained ===============
        o.train()
        assert isinstance(o.model, Model)
        # best model container
        if o.best_model_container.best_val_loss < float('inf'):
            assert 1 <= o.best_model_container.best_epoch <= 9
            assert 1 <= o.best_model_container.best_stage <= 3
        else:  # extremely bad case (NaN)
            assert o.best_model_container.best_epoch == 0
            assert o.best_model_container.best_stage == 0

        # history container
        assert len(o.history_container.history["loss"]) == 9
        assert len(o.history_container.history["val_loss"]) == 9
        assert len(o.history_container.history["r2_metric"]) == 9
        assert len(o.history_container.history["val_r2_metric"]) == 9
        assert len(o.history_container.history["lr"]) == 9
        assert o.history_container.ls_epochs == [3, 3, 3]
        assert o.history_container.stages == 3

        # check the best record matches its position in history
        stage = o.best_model_container.best_stage
        epoch = o.best_model_container.best_epoch
        idx = count_through_list(o.history_container.ls_epochs, stage, epoch) - 1
        assert o.best_model_container.best_val_loss == o.history_container.history["val_loss"][idx]

        # check coefficients indeed comes from that stage
        best_weights = o.best_model_container.best_weights
        # make sure the best weights were restored
        for i in range(len(best_weights)):
            assert np.all(o.model.get_weights()[i] == best_weights[i])

        # check model structure
        n_1 = build_params["ls_neurons"][stage - 1][0]
        n_2 = build_params["ls_neurons"][stage - 1][1]
        assert o.model.get_layer("dense_1").input_shape == (None, 1)
        assert o.model.get_layer("dense_1").output_shape == (None, n_1)
        assert o.model.get_layer("bn_1").input_shape == (None, n_1)
        assert o.model.get_layer("bn_1").output_shape == (None, n_1)
        assert o.model.get_layer("dense_3").input_shape == (None, n_2)
        assert o.model.get_layer("dense_3").output_shape == (None, 1)


def test_RleMsCallback_epsilon_1_c(data_mnist_1000):
    """Test: FixedMsTrainer + RleMsCallback on a classification problem.
    LinkMaskingCallback is not required for Epsilon=1.
    """
    # data
    x, y = data_mnist_1000

    # model
    model = sequential_mlp(
        (28 * 28,), 10, [128, 32],
        ["relu", "prelu"],
        "softmax", "sparse_categorical_crossentropy", metrics=["acc"],
        optimizer=Adam(learning_rate=1.0)  # make it bad
    )

    # evolutionary parameters
    epsilon = 1.0
    zeta = 0.2
    min_evo_scale = 0.3
    std_evo_scale = 0.3
    n_evo_rounds = 3

    for _ in range(10):

        # callbacks: keep the best + neuron masking
        best_callback = BestModelCallback()
        lr_callback = ReduceLROnPlateau(patience=1, factor=0.99, min_lr=0.1)  # bad

        # multistage callback
        rle_ms_callback = RleMsCallback(
            epsilon, zeta, min_evo_scale, std_evo_scale
        )

        epochs = 4
        batch_size = 32
        trainer = FixedMsTrainer(
            x, y, n_evo_rounds,
            model=model,
            metric="acc",
            fit_params={
                "validation_split": 0.2,
                "batch_size": batch_size,
                "epochs": epochs,
                "callbacks": [best_callback, lr_callback]
            },
            ms_callbacks=[rle_ms_callback]
        )

        # go!
        trainer.train()

        # ====== the trainer ======

        # state variable
        assert trainer.n_stages == 3
        assert trainer.stage_now == 3
        assert trainer.best_callback is best_callback  # is still the same object passed into
        assert trainer.ms_callbacks[0] is rle_ms_callback  # is still the same object passed into

        # ====== the history container ======

        # check there are 3 stages and 4 stages in each epochs
        assert trainer.history_container.stages == 3
        assert trainer.history_container.ls_epochs == [epochs, epochs, epochs]

        # check loss (mse), acc and learning rate are recorded for every stage
        assert trainer.history_container.metric == "acc"
        assert len(trainer.history_container.history) == 5  # (val_)(loss|r2_metric) + lr
        assert len(trainer.history_container.history["loss"]) == 3 * epochs
        assert len(trainer.history_container.history["val_loss"]) == 3 * epochs
        assert len(trainer.history_container.history["acc"]) == 3 * epochs
        assert len(trainer.history_container.history["val_acc"]) == 3 * epochs
        assert len(trainer.history_container.history["lr"]) == 3 * epochs

        # ====== the best model container ======

        # check the best model over all stages is indeed restored
        # 1. verify the global coeff is indeed restored
        wts = trainer.model.get_weights()
        for i, item in enumerate(trainer.best_model_container.best_weights):
            assert np.all(item == wts[i])

        # 2. verify model evaluation with training history

        # best val_loss in the history
        best_val_loss = np.min(trainer.history_container.history["val_loss"])  # mse
        idx_best_val_loss = np.argmin(trainer.history_container.history["val_loss"])

        # r2_metric "at the best val_loss" (does not necessarily gives the best r2 globally)
        best_val_acc = trainer.history_container.history["val_acc"][idx_best_val_loss]

        assert model.evaluate(x[800:, :], y[800:, :]) == [best_val_loss, best_val_acc]


def test_RleMsCallback_c(data_mnist_1000):
    """Test: FixedMsTrainer + RleMsCallback + LinkMaskingCallback
    on a classification problem. Epsilon < 1.
    """

    # data
    x, y = data_mnist_1000

    # model
    model = sequential_mlp(
        (28 * 28,), 10, [128, 32],
        ["relu", "prelu"],
        "softmax", "sparse_categorical_crossentropy", metrics=["acc"],
        optimizer=Adam(learning_rate=1.0)  # make it bad
    )
    # fit parameters
    epochs = 3
    batch_size = 32

    # evolutionary parameters
    epsilon = 0.7
    zeta = 0.2
    min_evo_scale = 0.3
    std_evo_scale = 0.3
    n_evo_rounds = 3

    for _ in range(10):

        # callbacks: keep the best + neuron masking
        best_callback = BestModelCallback()
        mask_callback = LinkMaskingCallback()
        lr_callback = ReduceLROnPlateau(patience=1, factor=0.99, min_lr=0.1)  # bad

        # multistage callback
        rle_ms_callback = RleMsCallback(
            epsilon, zeta, min_evo_scale, std_evo_scale, mask_callback=mask_callback
        )

        trainer = FixedMsTrainer(
            x, y, n_evo_rounds,
            model=model,
            metric="acc",
            fit_params={
                "validation_split": 0.2,
                "batch_size": batch_size,
                "epochs": epochs,
                "callbacks": [best_callback, mask_callback, lr_callback]
            },
            ms_callbacks=[rle_ms_callback]
        )

        # go!
        trainer.train()

        # ====== the trainer ======

        # state variable
        assert trainer.n_stages == 3
        assert trainer.stage_now == 3
        assert trainer.best_callback is best_callback  # is still the same object passed into
        assert trainer.ms_callbacks[0] is rle_ms_callback  # is still the same object passed into

        # ====== the history container ======

        # check there are 3 stages and 4 stages in each epochs
        assert trainer.history_container.stages == 3
        assert trainer.history_container.ls_epochs == [epochs, epochs, epochs]

        # check loss (mse), acc and learning rate are recorded for every stage
        assert trainer.history_container.metric == "acc"
        assert len(trainer.history_container.history) == 5  # (val_)(loss|r2_metric) + lr
        assert len(trainer.history_container.history["loss"]) == 3 * epochs
        assert len(trainer.history_container.history["val_loss"]) == 3 * epochs
        assert len(trainer.history_container.history["acc"]) == 3 * epochs
        assert len(trainer.history_container.history["val_acc"]) == 3 * epochs
        assert len(trainer.history_container.history["lr"]) == 3 * epochs

        # ====== the model ======

        # dense_1
        wt = trainer.model.get_layer("dense_1").get_weights()
        n_active = int(np.ceil(28 * 28 * 128 * epsilon))
        assert wt[0].shape == (28 * 28, 128)  # weight
        assert np.count_nonzero(wt[0]) <= n_active  # at most n_active nonzero elements
        assert wt[1].shape == (128,)  # bias

        # dense_2
        wt = trainer.model.get_layer("dense_2").get_weights()
        n_active = int(np.ceil(128 * 32 * epsilon))
        assert wt[0].shape == (128, 32)  # weight
        assert np.count_nonzero(wt[0]) <= n_active  # at most n_active nonzero elements
        assert wt[1].shape == (32,)  # bias

        # dense_3 (output)(NOT applied, so not tested)

        # ====== the best model container ======

        # check the best model over all stages is indeed restored
        # 1. verify the global coeff is indeed restored
        wts = trainer.model.get_weights()
        for i, item in enumerate(trainer.best_model_container.best_weights):
            assert np.all(item == wts[i])

        # 2. verify model evaluation with training history

        # best val_loss in the history
        best_val_loss = np.min(trainer.history_container.history["val_loss"])  # mse
        idx_best_val_loss = np.argmin(trainer.history_container.history["val_loss"])

        # r2_metric "at the best val_loss" (does not necessarily gives the best r2 globally)
        best_val_acc = trainer.history_container.history["val_acc"][idx_best_val_loss]

        assert model.evaluate(x[800:, :], y[800:, :]) == [best_val_loss, best_val_acc]

