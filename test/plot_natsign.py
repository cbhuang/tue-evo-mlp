"""
Experiment Zone
"""

import os
from pathlib import Path
import sys
from datetime import datetime
import numpy as np
import matplotlib
from matplotlib import pyplot as plt

# add project base directory to path
if '__file__' in globals():  # run as a file
    base_dir = Path(__file__).absolute().parent.parent
else:  # run interactively
    base_dir = Path(os.getcwd())

if str(base_dir) not in sys.path:  # append only if not exist
    sys.path.append(str(base_dir))

# local libs
from lib.vis_template import plot_scatter_by_label
from src.data_synthesis import data_natsign

# ====================================================
# Parameters
# ====================================================
# right corner
x0, y0 = 1.0, 0.3
# top right
x1 = 1.3
# slope
r = 1
# gap between classes
gap = 0.05
# total number of points (must be even)
n = 2000
# random sampling
rnd = True
# print
verbose = True

# ====================================================
# Automated Workflow
# ====================================================

xy, z = data_natsign(n=n, x0=x0, y0=y0, r=r, gap=gap, rnd=rnd, verbose=verbose)
assert xy.shape == (n, 2)

# scatter plot
fig = plt.figure(figsize=(8, 6), dpi=100)
ax = fig.add_subplot(1,1,1)

# get colormap
color = ["blue" if zi == 1 else "black" for zi in z]
u, c = np.unique(color, return_counts=True)
print(f"unique:{u}, counts:{c}")
assert (len(u) == 2) and np.all(c == [n/2, n/2])

plot_scatter_by_label(xy[:, 0], xy[:, 1], "x", "y", "NatSign", marker='.', color=color, show_fig=True)
