"""
Verify optimizer behavior
"""

import sys
import os
import random
import numpy as np
import json
from pathlib import Path
from datetime import datetime
import tensorflow as tf
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.datasets import mnist
from tensorflow.keras.callbacks import ReduceLROnPlateau

from lib.keras_model import peak_1d_exact, sequential_mlp
from lib.keras_callback import BestModelCallback

# ========== Reproducibility ============

# unused yet
os.environ["PYTHONHASHSEED"] = "0"
os.environ["TF_DETERMINISTIC_OPS"] = "1"
# used
np.random.seed(2)
random.seed(3)
tf.random.set_seed(4)

# =========== Data & Model =============

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x = x_train.reshape((-1, 28*28)) / 255.0
y = y_train.reshape((-1, 1))

opt = Adam(learning_rate=0.01)

model = sequential_mlp(
    (28*28,), 10, [128, 32, 32],
    ["relu", "prelu", "leakyrelu"],
    "softmax", "sparse_categorical_crossentropy", optimizer=opt
)
model.summary()

# check optimizer is passed by reference
# Accessing the optimizer by model.optimizer or the original opt are both OK.
assert id(model.optimizer) == id(opt)

# ========== initial status ==========
opt_addr = hex(id(opt))
opt_weights_begin = opt.get_weights()
print(f"opt_weights_begin: {opt_weights_begin}")

# ========== status after training==========
history = model.fit(
    x[:1024,:], y[:1024,:], validation_split=0.2, epochs=1, batch_size=1024,
    callbacks=[ReduceLROnPlateau(factor=0.3, min_lr=0.001, patience=1)]
)

# check: the optimizer in the model remains the same object
opt_addr_after = hex(id(model.optimizer))
assert opt_addr_after == opt_addr

# new status
assert id(model.optimizer) == id(opt)
opt_weights_new = opt.get_weights()
model.optimizer.learning_rate.assign(0.001)
print(model.optimizer.learning_rate)

# new status2
history = model.fit(
    x, y, validation_split=0.2, epochs=1, batch_size=1024,
    callbacks=[ReduceLROnPlateau(factor=0.3, min_lr=0.001, patience=1)]
)
opt_weights_new2 = opt.get_weights()
for i, item in enumerate(opt_weights_new2):
    if isinstance(item, (int, float, np.float32, np.float64, np.int64, np.int32)):
        print(f"Item {i}: {item}")
    else:
        print(f"Item {i}: {item.shape}")

opt.get_weights()
