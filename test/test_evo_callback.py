"""
Test evolutionary training (ET) callbacks.
"""
import os
import sys
from pathlib import Path
import numpy as np
import random
import tensorflow as tf
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.datasets import mnist

# locate project base
if "__file__" in globals():
    base_dir = Path(__file__).absolute().parent.parent
else:
    base_dir = Path(os.getcwd())
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

from lib.keras_model import sequential_mlp
from lib.keras_callback import BestModelCallback, PrintCallback
# from lib.keras_multistage import RebuildSequentialMlpMsCallback, FixedMsTrainer, BestWeightsTransferMsCallback
from lib.math_util import r2_metric, count_through_list, random_mask, eq_f32
from src.evo_callback import NeuronMaskingCallback, LinkMaskingCallback
# from src.evo_multistage import NiByMaskingTrainer, EvoMsCallback, RleMsCallback
# from src.data_synthesis import data_zigzag_1d


def reseed(seed_np_random, seed_python_random, seed_tf_random):
    os.environ["PYTHONHASHSEED"] = "0"
    os.environ["TF_DETERMINISTIC_OPS"] = "1"
    np.random.seed(seed_np_random)
    random.seed(seed_python_random)
    tf.random.set_seed(seed_tf_random)


def test_LinkMaskingCallback_on_batch_proximity(data_mnist_10000):
    """Verify that LinkMaskingCallback applied ``on_batch_end``
    can obtain val_loss and val_acc both within 1% error compared to the
    values recorded in the history (calculated unmasked)."""

    # data (use a big dataset)
    x, y = data_mnist_10000

    def run():
        print("\n========== run() begins ==========")
        reseed(11111, 22222, 33333)

        init_lr = 0.01
        epochs = 1000  # max epochs
        batch_size = 32
        patience = 10
        validation_split = 0.2

        # model (good settings)
        model = sequential_mlp(
            (28 * 28,), 10, [128, 32],
            ["relu", "prelu"],
            "softmax", "sparse_categorical_crossentropy", metrics=["acc"],
            batch_normalization=True,
            dropout_rate=0,
            optimizer=Adam(learning_rate=init_lr)
        )

        # mask (epsilon = 0.5)
        ls_masks = [random_mask((28 * 28, 128), 28 * 28 * 64),
                    random_mask((128, 32), 128 * 16),
                    random_mask((32, 10), 32 * 5)]

        # callbacks: keep the best + neuron masking
        best_callback = BestModelCallback()
        mask_callback = LinkMaskingCallback(ls_masks, apply_on_batch=True)
        rlr_callback = ReduceLROnPlateau(patience=patience, min_lr=init_lr / 1000)
        es_callback = EarlyStopping(patience=2 * patience)

        ls_callbacks = [best_callback, mask_callback, rlr_callback, es_callback]

        # go!
        hist = model.fit(
            x, y, validation_split=validation_split,
            batch_size=batch_size,
            epochs=epochs,
            callbacks=ls_callbacks
        )

        # ============ verify result ==============

        # will destroy internal history
        n_val_begin = int(10000 * (1 - validation_split))
        loss_acc_eval = model.evaluate(x[n_val_begin:, :],
                                       y[n_val_begin:, :],
                                       verbose=0)  # == [best_val_loss, best_val_acc]

        assert isinstance(hist.history, dict)
        return {
            "history_val_loss": hist.history["val_loss"] if "val_loss" in hist.history else None,
            "history_val_acc": hist.history["val_acc"] if "val_acc" in hist.history else None,
            "eval_val_loss": loss_acc_eval[0],
            "eval_val_acc": loss_acc_eval[1]
        }
    ans = run()

    # =========================================================
    # verify the history is indeed computed after masking
    # =========================================================

    # check val_loss (0.01%)
    val_loss_min_hist = min(ans["history_val_loss"])
    val_loss_after_mask = ans["eval_val_loss"]
    assert eq_f32(val_loss_after_mask, val_loss_min_hist, err_ratio=1e-2, err_abs=1e-2)

    # check val_acc (0.01%)
    val_acc_max_hist = max(ans["history_val_acc"])
    val_acc_after_mask = ans["eval_val_acc"]
    assert eq_f32(val_acc_after_mask, val_acc_max_hist, err_ratio=1e-2, err_abs=1e-2)


def test_LinkMaskingCallback_on_epoch_proximity(data_mnist_10000):
    """Verify that LinkMaskingCallback applied ``on_epoch_end``
    can obtain val_loss within 5% and val_acc within 5% when compared to the
    values recorded in the History callback (calculated unmasked).

    On_epoch_end is somehow faster.
    """

    # data (use a big dataset)
    x, y = data_mnist_10000

    def run():
        print("\n========== run() begins ==========")
        reseed(11, 22, 33)

        init_lr = 0.01
        epochs = 1000  # max epochs
        batch_size = 32
        patience = 10
        validation_split = 0.2

        # model (good settings)
        model = sequential_mlp(
            (28 * 28,), 10, [128, 32],
            ["relu", "prelu"],
            "softmax", "sparse_categorical_crossentropy", metrics=["acc"],
            batch_normalization=True,
            dropout_rate=0,
            optimizer=Adam(learning_rate=init_lr)
        )

        # mask (epsilon = 0.5)
        ls_masks = [random_mask((28 * 28, 128), 28 * 28 * 64),
                    random_mask((128, 32), 128 * 16),
                    random_mask((32, 10), 32 * 5)]

        # callbacks: keep the best + neuron masking
        best_callback = BestModelCallback()
        mask_callback = LinkMaskingCallback(ls_masks, apply_on_batch=False)
        rlr_callback = ReduceLROnPlateau(patience=patience, min_lr=init_lr / 1000)
        es_callback = EarlyStopping(patience=2 * patience)

        ls_callbacks = [best_callback, mask_callback, rlr_callback, es_callback]

        # go!
        hist = model.fit(
            x, y, validation_split=validation_split,
            batch_size=batch_size,
            epochs=epochs,
            callbacks=ls_callbacks
        )

        # ============ verify result ==============

        # will destroy internal history
        n_val_begin = int(10000 * (1 - validation_split))
        loss_acc_eval = model.evaluate(x[n_val_begin:, :],
                                       y[n_val_begin:, :],
                                       verbose=0)  # == [best_val_loss, best_val_acc]

        assert isinstance(hist.history, dict)
        return {
            "history_val_loss": hist.history["val_loss"] if "val_loss" in hist.history else None,
            "history_val_acc": hist.history["val_acc"] if "val_acc" in hist.history else None,
            "eval_val_loss": loss_acc_eval[0],
            "eval_val_acc": loss_acc_eval[1]
        }
    ans = run()

    # =========================================================
    # verify the history is indeed computed after masking
    # =========================================================

    # check val_loss (0.01%)
    val_loss_min_hist = min(ans["history_val_loss"])
    val_loss_after_mask = ans["eval_val_loss"]
    assert eq_f32(val_loss_after_mask, val_loss_min_hist, err_ratio=.05, err_abs=.1)

    # check val_acc (0.01%)
    val_acc_max_hist = max(ans["history_val_acc"])
    val_acc_after_mask = ans["eval_val_acc"]
    assert eq_f32(val_acc_after_mask, val_acc_max_hist, err_ratio=.05, err_abs=.1)


def test_NeuronMaskingCallback_one_stage_f(data_zigzag_1000):
    """Test NeuronMaskingCallback on a single-stage
    function-fitting problem."""

    # data
    x, y = data_zigzag_1000

    # model
    model = sequential_mlp(
        (1,), 1, [128, 32],
        ["relu", "prelu"],
        "linear", "mse",
        optimizer=Adam(learning_rate=1.0)  # a bad one
    )

    # keep the best + neuron masking
    best_obj = BestModelCallback()  # enforced
    mask_obj = NeuronMaskingCallback([32, 8])

    for _ in range(10):  # try more times
        best_obj.reset()
        model.fit(
            x, y, validation_split=0.2, batch_size=32, epochs=3,
            callbacks=[best_obj,
                       mask_obj,
                       ReduceLROnPlateau(patience=1, factor=0.99, min_lr=0.1)]  # a bad one
        )

        best_weights = model.get_weights()

        # ====== the best container ======
        assert model.evaluate(x[800:, :], y[800:, :]) == best_obj.best_val_loss

        # ====== the masking callback ======
        # dense_1
        assert best_weights[0].shape == (1, 128)  # weight
        assert (best_weights[0][0, 32:] == 0).all()  # from this layer only
        assert best_weights[1].shape == (128,)  # bias
        assert (best_weights[1][32:] == 0).all()

        # bn_1
        assert best_weights[2].shape == best_weights[3].shape == best_weights[4].shape == best_weights[5].shape == (128,)
        assert (best_weights[2][32:] == 1).all()
        assert (best_weights[3][32:] == 0).all()
        assert (best_weights[4][32:] == 0).all()
        assert (best_weights[5][32:] == 1).all()

        # dense_2
        assert best_weights[6].shape == (128, 32)  # weight
        assert (best_weights[6][:, 8:] == 0).all()  # from the ls_masks of this layer
        assert (best_weights[6][32:, :] == 0).all()  # from the ls_masks of the previous layer
        assert best_weights[7].shape == (32,)  # bias
        assert (best_weights[7][8:] == 0).all()

        # bn_2
        assert best_weights[8].shape == best_weights[9].shape == best_weights[10].shape == best_weights[11].shape == (32,)
        assert (best_weights[8][8:] == 1).all()
        assert (best_weights[9][8:] == 0).all()
        assert (best_weights[10][8:] == 0).all()
        assert (best_weights[11][8:] == 1).all()

        # PReLU
        assert best_weights[12].shape == (32,)
        assert (best_weights[12][8:] == 0).all()

        # dense_3 (output)
        assert best_weights[13].shape == (32, 1)  # weight
        assert (best_weights[13][8:, :] == 0).all()  # from previous layer only
        assert best_weights[14].shape == (1,)  # bias


def test_NeuronMaskingCallback_one_stage_c(data_mnist_1000):
    """Test NeuronMaskingCallback on a single-stage
    classification problem."""

    # data
    # (x_train, y_train), (x_test, y_test) = mnist.load_data()
    # # take 1000 records only
    # x = x_train.reshape((-1, 28*28))[:1000, :]
    # y = y_train.reshape((-1, 1))[:1000, :]
    x, y = data_mnist_1000

    # model
    model = sequential_mlp(
        (28*28,), 10, [128, 32],
        ["relu", "prelu"],
        "softmax", "sparse_categorical_crossentropy",
        optimizer=Adam(learning_rate=1.0)  # a bad one
    )

    # keep the best + neuron masking
    best_obj = BestModelCallback()
    mask_obj = NeuronMaskingCallback([32, 8])

    for _ in range(10):  # try more times
        best_obj.reset()
        model.fit(
            x, y, validation_split=0.2, batch_size=32, epochs=3,
            callbacks=[best_obj,
                       mask_obj,
                       ReduceLROnPlateau(patience=1, factor=0.99, min_lr=0.1)]  # give a bad one
        )

        best_weights = model.get_weights()

        # ====== the best container ======
        assert model.evaluate(x[800:, :], y[800:, :]) == best_obj.best_val_loss

        # ====== the masking callback ======
        # dense_1
        assert best_weights[0].shape == (28*28, 128)  # weight
        assert (best_weights[0][0, 32:] == 0).all()  # from this layer only
        assert best_weights[1].shape == (128,)  # bias
        assert (best_weights[1][32:] == 0).all()

        # bn_1
        assert best_weights[2].shape == best_weights[3].shape == best_weights[4].shape == best_weights[5].shape == (128,)
        assert (best_weights[2][32:] == 1).all()
        assert (best_weights[3][32:] == 0).all()
        assert (best_weights[4][32:] == 0).all()
        assert (best_weights[5][32:] == 1).all()

        # dense_2
        assert best_weights[6].shape == (128, 32)  # weight
        assert (best_weights[6][:, 8:] == 0).all()  # from the ls_masks of this layer
        assert (best_weights[6][32:, :] == 0).all()  # from the ls_masks of the previous layer
        assert best_weights[7].shape == (32,)  # bias
        assert (best_weights[7][8:] == 0).all()

        # bn_2
        assert best_weights[8].shape == best_weights[9].shape == best_weights[10].shape == best_weights[11].shape == (32,)
        assert (best_weights[8][8:] == 1).all()
        assert (best_weights[9][8:] == 0).all()
        assert (best_weights[10][8:] == 0).all()
        assert (best_weights[11][8:] == 1).all()

        # prelu
        assert best_weights[12].shape == (32,)
        assert (best_weights[12][8:] == 0).all()

        # dense_3 (output)
        assert best_weights[13].shape == (32, 10)  # weight
        assert (best_weights[13][8:, :] == 0).all()  # from previous layer only
        assert best_weights[14].shape == (10,)  # bias
