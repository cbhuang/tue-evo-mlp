import sys
import numpy as np
from pathlib import Path

# locate project base (always up one level)
base_dir = Path(__file__).absolute().parent.parent
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

import src.data_synthesis as ds


def test_f_peak_1d():
    # normal cases
    assert ds.f_peak_1d(-2) == 0.0
    assert ds.f_peak_1d(-1) == 0.0
    assert ds.f_peak_1d(-0.25) == 0.75
    assert ds.f_peak_1d(0) == 1
    assert ds.f_peak_1d(0.25) == 0.75
    assert ds.f_peak_1d(1) == 0.0
    assert ds.f_peak_1d(2) == 0


def test_data_peak_1d():
    # normal case: n_docs
    x, y = ds.data_peak_1d(1000, dtype=np.float64)   # rng=2.0
    # shape
    assert x.shape == (1000, 1)
    assert y.shape == (1000, 1)
    # type
    assert x.dtype == np.float64
    assert y.dtype == np.float64
    # value range
    assert np.min(y) == 0.0
    assert np.max(y) < 1.0


def test_f_pyramid_nd():
    # zero region
    assert ds.f_peak_nd((-2, 0, 0)) == 0.0
    assert ds.f_peak_nd([0, 1, 0]) == 0.0
    assert ds.f_peak_nd([2]) == 0.0  # 1d case
    # normal region
    assert ds.f_peak_nd([0.5, -0.25]) == 0.5
    assert ds.f_peak_nd(np.array([0.5, -0.25, 0.875])) == 0.125
    assert ds.f_peak_nd([0.875]) == 0.125  # 1d
    # origin is 1
    assert ds.f_peak_nd(np.zeros(100)) == 1.0


def test_data_pyramid_nd():
    # normal case: n_docs
    x, y = ds.data_peak_nd(1000, 4, dtype=np.float64)   # rng=2.0
    # shape
    assert x.shape == (1000, 4)
    assert y.shape == (1000, 1)
    # dtype
    # type
    assert x.dtype == np.float64
    assert y.dtype == np.float64
    # value range
    assert np.min(y) == 0.0
    assert np.max(y) < 1.0


def test_zigzag():
    assert abs(ds.f_zigzag(0) - 0) < 1e-10
    assert abs(ds.f_zigzag(0.5) - 1) < 1e-10
    assert abs(ds.f_zigzag(1.0) - 0) < 1e-10
    assert abs(ds.f_zigzag(1.5) - (-1)) < 1e-10
    assert abs(ds.f_zigzag(2) - 0) < 1e-10
    assert abs(ds.f_zigzag(2.25) - 0.5) < 1e-10
    assert abs(ds.f_zigzag(-0.3) - -0.6) < 1e-10
    assert abs(ds.f_zigzag(-1.8) - 0.4) < 1e-10
    assert abs(ds.f_zigzag(-3) - 0) < 1e-10
    assert abs(ds.f_zigzag(-2.5) - (-1)) < 1e-10


def test_data_natsign():

    xy, z = ds.data_natsign(n=200, rnd=True)
    assert xy.shape == (200, 2)
    assert z.shape == (200, 1)
    # check label is [0, 1]
    assert np.all(np.sort(np.unique(z)) == np.array([0.0, 1.0], dtype=np.float32))
