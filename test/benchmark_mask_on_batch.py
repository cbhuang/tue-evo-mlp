
import os
import sys
from pathlib import Path
import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.optimizers import Adam
import numpy as np
import random
from datetime import datetime

# locate project base
if "__file__" in globals():
    base_dir = Path(__file__).absolute().parent.parent
else:
    base_dir = Path(os.getcwd())
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

from lib.keras_model import sequential_mlp
from lib.keras_callback import BestModelCallback
from lib.math_util import random_mask
from src.evo_callback import LinkMaskingCallback
from lib.vis_template import plot_multi_y


ls_batch_intervals = [1,2,3,4,6,8,12,16,32]  # 0=on_epoch_end
ls_t = []  # (time)

# data
n_subset = 6000
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x = x_train.reshape((-1, 28 * 28))[:n_subset, :]
y = y_train.reshape((-1, 1))[:n_subset, :]

# =========== 1. on epoch end ==============
init_lr = 0.01
epochs = 10  # max epochs
batch_size = 32
patience = 10
validation_split = 0.2

# model (good settings)
model = sequential_mlp(
    (28 * 28,), 10, [128, 32],
    ["relu", "relu"],
    "softmax", "sparse_categorical_crossentropy", metrics=["acc"],
    batch_normalization=True,
    dropout_rate=0,
    optimizer=Adam(learning_rate=init_lr)
)

# mask (epsilon = 0.5)
ls_masks = [random_mask((28 * 28, 128), 28 * 28 * 64),
            random_mask((128, 32), 128 * 16),
            random_mask((32, 10), 32 * 5)]

# callbacks: keep the best + neuron masking
best_callback = BestModelCallback()
mask_callback = LinkMaskingCallback(ls_masks, apply_on_batch=False)

ls_callbacks = [best_callback, mask_callback]

# go!
t0 = datetime.now()
model.fit(
    x, y, validation_split=validation_split,
    batch_size=batch_size,
    epochs=epochs,
    callbacks=ls_callbacks
)
t_on_epoch = (datetime.now() - t0).total_seconds()

# =========== 2. on batch end ==============

for batch_interval in ls_batch_intervals:

    # model (good settings)
    model = sequential_mlp(
        (28 * 28,), 10, [128, 32],
        ["relu", "relu"],
        "softmax", "sparse_categorical_crossentropy", metrics=["acc"],
        batch_normalization=True,
        dropout_rate=0,
        optimizer=Adam(learning_rate=init_lr)
    )

    # callbacks: keep the best + neuron masking
    best_callback = BestModelCallback()
    mask_callback = LinkMaskingCallback(ls_masks, apply_on_batch=True, batch_interval=batch_interval)
    ls_callbacks = [best_callback, mask_callback]

    # go!
    t0 = datetime.now()
    model.fit(
        x, y, validation_split=validation_split,
        batch_size=batch_size,
        epochs=epochs,
        callbacks=ls_callbacks
    )
    ls_t.append((datetime.now() - t0).total_seconds())

# plot

print(f"t_on_epoch = {t_on_epoch:.1f}s")

ls_ratio = [t / t_on_epoch for t in ls_t]

plot_multi_y(ls_batch_intervals,
             [ls_ratio], ["on_batch_end", "on_epoch_end"],
             "batch_intervals", "t / t_on_epoch",
             "Batch vs epoch")
